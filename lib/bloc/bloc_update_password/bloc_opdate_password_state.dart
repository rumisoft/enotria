part of 'bloc_opdate_password_bloc.dart';

@immutable
// ignore: must_be_immutable
class BlocUpdatePasswordState {
  final bool status;
  final String oldPasswd;
  final String password1;
  final String password2;
  final bool updateStatus;
  int timerEnd;

  BlocUpdatePasswordState({
    this.status,
    this.oldPasswd,
    this.password1,
    this.password2,
    this.updateStatus,
    this.timerEnd,
  });

  static BlocUpdatePasswordState get initialState => BlocUpdatePasswordState(
        status: false,
        oldPasswd: "",
        password1: "",
        password2: "",
        updateStatus: false,
        timerEnd: 0,
      );
  BlocUpdatePasswordState copyWith({
    bool status,
    String user,
    String password1,
    String password2,
    bool updateStatus,
    int timerEnd,
  }) {
    return BlocUpdatePasswordState(
      status: status ?? this.status,
      oldPasswd: oldPasswd ?? this.oldPasswd,
      password1: password1 ?? this.password1,
      password2: password2 ?? this.password2,
      updateStatus: updateStatus ?? this.updateStatus,
      timerEnd: timerEnd ?? this.timerEnd,
    );
  }
}
