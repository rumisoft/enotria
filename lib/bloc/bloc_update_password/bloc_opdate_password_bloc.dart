import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_newPassword.dart';
import 'package:enotria/pages/page_login.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

part 'bloc_opdate_password_event.dart';
part 'bloc_opdate_password_state.dart';

class BlocUpdatePasswordBloc
    extends Bloc<BlocUpdatePasswordEvent, BlocUpdatePasswordState> {
  BlocUpdatePasswordBloc() : super(BlocUpdatePasswordState.initialState);
  Map<String, dynamic> _map = Map<String, dynamic>();
  ApiNewPassword _api = ApiNewPassword();

  Timer timer;
  timerOuth(BuildContext context) async {
    timer = Timer.periodic(Duration(seconds: 1), (t) async {
      state.timerEnd++;
      print("------------------------- timer ${state.timerEnd}");
      add(CountTimer(timer: state.timerEnd));

      if (state.timerEnd == 6) {
        t.cancel();
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
              builder: (context) => PageLogin(),
            ),
            (Route<dynamic> route) => false);
      }
    });
  }

  @override
  Stream<BlocUpdatePasswordState> mapEventToState(
    BlocUpdatePasswordEvent event,
  ) async* {
    if (event is NewPassword) {
      yield this.state.copyWith(status: true, updateStatus: false);
      _map = await _api.newPassword(
        oldPasswd: event.oldPasswd,
        password1: event.password1,
        password2: event.password2,
      );

      print("----> ${_map.toString()}");
      if (_map["data"]["err"]) {
        yield this.state.copyWith(status: false, updateStatus: false);
        showTopSnackBar(
          event.context,
          CustomSnackBar.error(
            message: "${_map['data']['data']}",
          ),
        );
      } else if (!_map["data"]["err"]) {
        showTopSnackBar(
          event.context,
          CustomSnackBar.info(
            message: "${_map['data']['data']}",
          ),
        );
        print("----> ${state.updateStatus}");
        timerOuth(event.context);
        yield this.state.copyWith(status: false, updateStatus: true);
      }
    } else if (event is CountTimer) {
      yield this.state.copyWith(timerEnd: event.timer);
    }
  }
}
