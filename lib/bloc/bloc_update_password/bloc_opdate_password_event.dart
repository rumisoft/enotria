part of 'bloc_opdate_password_bloc.dart';

@immutable
abstract class BlocUpdatePasswordEvent {}

class NewPassword extends BlocUpdatePasswordEvent {
  final String oldPasswd;
  final String password1;
  final String password2;
  final BuildContext context;

  NewPassword({this.oldPasswd, this.password1, this.password2, this.context});
}

class CountTimer extends BlocUpdatePasswordEvent {
  final int timer;

  CountTimer({this.timer});
}
