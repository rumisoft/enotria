part of 'bloc_pruebahuellero_bloc.dart';

@immutable
abstract class BlocPruebahuelleroEvent {}

class VerificarIdentidadWsqEvent extends BlocPruebahuelleroEvent {
  final String dni;
  final huellaDer;
  final huellaIzq;


  VerificarIdentidadWsqEvent(this.dni, this.huellaDer, this.huellaIzq);
}
