import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_consultaDatosReniec.dart';
import 'package:enotria/model/consulta_reniec/consultaBiometriaReniecModel.dart';
import 'package:meta/meta.dart';

part 'bloc_pruebahuellero_event.dart';
part 'bloc_pruebahuellero_state.dart';

class BlocPruebahuelleroBloc
    extends Bloc<BlocPruebahuelleroEvent, BlocPruebahuelleroState> {
  BlocPruebahuelleroBloc() : super(BlocPruebahuelleroState());

  @override
  Stream<BlocPruebahuelleroState> mapEventToState(
      BlocPruebahuelleroEvent event) async* {
    if (event is VerificarIdentidadWsqEvent) {
      //map en donde recibirá la rpta de la api
      Map<String, dynamic> decodedData = new Map<String, dynamic>();

      decodedData = await ApiConsultaDatosReniec()
          .getBiometriaWsqReniec(event.dni, event.huellaDer, event.huellaIzq);

      var wsqModel = IdentidadWsqModel();

      print(" ResWsq--- $decodedData");

      if (decodedData["Message"] == "Error.") {
        var wsqModel = IdentidadWsqModel();
        wsqModel.descripResultado = decodedData["deResultado"];
      } else {
        //llenar el objeto con la rpta
        wsqModel.codResultado = decodedData["coResultado"];
        wsqModel.descripResultado = decodedData["deResultado"];
        wsqModel.dni = decodedData["nuDni"];
        wsqModel.coRestriccion = decodedData["coRestriccion"];
        wsqModel.tiempoRespuesta = decodedData["tiempoRespuesta"];
      }

      //yield BlocPruebahuelleroState(wsqModel);
      yield this.state.copyWith(
            identidadWsqModel: wsqModel,
          );
    }
  }
}
