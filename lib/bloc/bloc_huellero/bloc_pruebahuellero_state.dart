part of 'bloc_pruebahuellero_bloc.dart';

class BlocPruebahuelleroState {
  final IdentidadWsqModel identidadWsqModel;
  BlocPruebahuelleroState({this.identidadWsqModel});

  BlocPruebahuelleroState copyWith({
    IdentidadWsqModel identidadWsqModel,
  }) {
    return BlocPruebahuelleroState(
      identidadWsqModel: identidadWsqModel ?? this.identidadWsqModel,
    );
  }
}
