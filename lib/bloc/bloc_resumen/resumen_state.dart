part of 'resumen_bloc.dart';

class ResumenState {
  final bool loading;
  final bool gpsEnabled;
  final Position myLocation;
  final List<Map<String, dynamic>> estadosList;
  final List<Map<String, dynamic>> subestadosList;
  //validar Biometria
  final ValidacionBioModel validacionBio;

  ResumenState( {
    this.loading,
    this.gpsEnabled,
    this.myLocation,
    this.estadosList,
    this.subestadosList,
    this.validacionBio,
  });

  static ResumenState get initialState => new ResumenState(
        loading: true,
        gpsEnabled: Platform.isIOS,
        myLocation: null,
        estadosList: [],
        subestadosList: [],
      );
  ResumenState copyWith({
    Position myLocation,
    bool loading,
    bool gpsEnabled,
    List<Map<String, dynamic>> estadosList,
    List<Map<String, dynamic>> subestadosList,
    ValidacionBioModel validacionBio,
  }) {
    return ResumenState(
      myLocation: myLocation ?? this.myLocation,
      loading: loading ?? this.loading,
      gpsEnabled: gpsEnabled ?? this.gpsEnabled,
      estadosList: estadosList ?? this.estadosList,
      subestadosList: subestadosList ?? this.subestadosList,
      validacionBio: validacionBio ?? this.validacionBio,
    );
  }
}
