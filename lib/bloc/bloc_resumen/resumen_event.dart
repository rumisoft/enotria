part of 'resumen_bloc.dart';

@immutable
abstract class ResumenEvent {}

class SendResumenFinalize extends ResumenEvent {
  final BuildContext context;
  final String type;
  final String productName;
  final String enterpriseName;
  final String deliveryCode;
  final String code;
  final Map<String, dynamic> selectAddressMap;
  final Map<String, dynamic> selectPersonMap;
  final String fecha;
  final String estado;
  final String subestado;
  final List<FilesPhoto> cargos;
  final List<FilesPhoto> evidencia;
  final Position position;
  final String codEstado;
  final String codSubEstado;
  SendResumenFinalize({
    this.context,
    this.type,
    this.productName,
    this.enterpriseName,
    this.deliveryCode,
    this.fecha,
    this.code,
    this.selectAddressMap,
    this.selectPersonMap,
    this.estado,
    this.subestado,
    this.cargos,
    this.evidencia,
    this.position,
    this.codEstado,
    this.codSubEstado,
  });
}

//verificar si el GPS esta habilitado
class OnGpsEnabled extends ResumenEvent {
  final bool enabled;

  OnGpsEnabled({this.enabled});
}

//actualizar ubicacion
class OnMyLocationUpdate extends ResumenEvent {
  final LatLng location;
  OnMyLocationUpdate(this.location);
}

class OnrideStart extends ResumenEvent {
  final int idDriver;
  final int idRide;

  OnrideStart(this.idDriver, this.idRide);
}

class OnPrivatePosition extends ResumenEvent {
  final Position newPosition;
  final int newDriverId;
  final String newDriverName;

  OnPrivatePosition({this.newPosition, this.newDriverId, this.newDriverName});
}

//obtener estados y subestados de los productos
class GetEstadoSubestados extends ResumenEvent {
  final String id;
  GetEstadoSubestados({this.id});
}
//obtener estados de los productos
class GetSubestados extends ResumenEvent {
  final List<Map<String, dynamic>> subestados;
  GetSubestados({this.subestados});
}

class SendUpdateImage extends ResumenEvent{
  final String code;
  final List<FilesPhoto> cargos;
  final List<FilesPhoto> evidencias;

  SendUpdateImage( {this.code, this.cargos, this.evidencias});
}
