import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_estadoSubestados.dart';
import 'package:enotria/model/EstadoProductoModel.dart';
import 'package:enotria/pages/page_resumen.dart';
import 'package:enotria/pages/page_resumen_finalize.dart';
import 'package:enotria/util/geolocator_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

part 'resumen_event.dart';
part 'resumen_state.dart';

class ResumenBloc extends Bloc<ResumenEvent, ResumenState> {
  ResumenBloc() : super(ResumenState.initialState) {
    init();
  }
  StreamSubscription _gpsSubscription, _postionSubscription;
  //instancia de configuracion de Gps
  GeolocatorWrapper _geolocator = new GeolocatorWrapper();
  //instancia que llama a la api
  final _api = new ApiEstadosSubestados();

  Map<String, dynamic> _map = Map<String, dynamic>();
  Map<String, dynamic> _mapGeneric = Map<String, dynamic>();
  List<Map<String, dynamic>> estados = [];

  //funcion que verifica si el Gps esta activo o no
  init() async {
    final gpsEnabled = await _geolocator.isLocationServiceEnabled;

    print("---> GPS: 1 ${gpsEnabled.toString()}");

    add(OnGpsEnabled(enabled: gpsEnabled));

    _geolocator.onServiceEnabled.listen(
      (enabled) {
        add(OnGpsEnabled(enabled: enabled));
      },
    );

    await _initLocationUpdates();
  }

  //permisos para el gps
  Future<void> _initLocationUpdates() async {
    LocationPermission permission;

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        print("ACCESO A UBICACION DENEGADO...................");
      } else {
        var position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high);
        add(OnPrivatePosition(
          newPosition: position,
          newDriverId: 1,
          newDriverName: "usuario",
        ));
      }
    } else {
      var position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      add(OnPrivatePosition(
        newPosition: position,
        newDriverId: 1,
        newDriverName: "usuario",
      ));
    }
  }

  onGPS() => Geolocator.openLocationSettings();

  @override
  Future<void> close() {
    _gpsSubscription?.cancel();
    _postionSubscription?.cancel();
    return super.close();
  }

  @override
  Stream<ResumenState> mapEventToState(ResumenEvent event) async* {
    if (event is SendResumenFinalize) {
      int countCargo = 0;
      int countEvidencias = 0;
      List<String> listB64Cargo = [];
      List<String> listB64Evidencias = [];

      event.cargos.forEach((element) {
        {
          listB64Cargo.add(element.b64);
          if (element.b64 != "") {
            countCargo++;
          }
        }
      });

      event.evidencia.forEach((element) {
        {
          listB64Evidencias.add(element.b64);
          if (element.b64 != "") {
            countEvidencias++;
          }
        }
      });

      Navigator.of(event.context).pushAndRemoveUntil(
          MaterialPageRoute(
            builder: (context) => PageResumenFinalize(
              type: event.type,
              productName: event.productName,
              enterpriseName: event.enterpriseName,
              deliveryCode: event.deliveryCode,
              selectAddressMap: event.selectAddressMap,
              selectPersonMap: event.selectPersonMap,
              fecha: event.fecha,
              code: event.code,
              estado: event.estado,
              subestado: event.subestado,
              countCargo: countCargo,
              countEvidencias: countEvidencias,
              listCargo: listB64Cargo,
              listEvidendia: listB64Evidencias,
              position: state.myLocation,
              codEstado: event.codEstado,
              codSubEstado: event.codSubEstado,
            ),
          ),
          (Route<dynamic> route) => false);
    } else if (event is OnGpsEnabled) {
      yield this.state.copyWith(gpsEnabled: event.enabled);
    } else if (event is OnPrivatePosition) {
      yield this.state.copyWith(myLocation: event.newPosition);
    } else if (event is GetEstadoSubestados) {
      
      //llamar a api de estado de producto
      _map = await _api.getEstadoSubestados(event.id);

      print("respuesta biooo-----${_map['data']["bio"]}");

      for (_mapGeneric in _map["data"]["estados"]) {
        estados.add(_mapGeneric);
        //-->TODO recorrer estados

      }
      //yield this.state.copyWith(estadosList: _estados);
      final valBioModel = ValidacionBioModel();
      if (_map["data"]["bio"].length > 0) {
        valBioModel.tipoBiometria = _map["data"]["bio"]["tipo_biometria"];
        valBioModel.numeroIntentos = _map["data"]["bio"]["numero_intentos"];
        valBioModel.saltoBiometria = _map["data"]["bio"]["salto_biometria"];
        //para restar los numeroa de intentos 
        valBioModel.cantRestante = 1;

      }

      yield this.state.copyWith(
          estadosList: estados, validacionBio: valBioModel);
      
    } else if (event is GetSubestados) {
      yield this.state.copyWith(subestadosList: event.subestados);
    } else if (event is SendUpdateImage) {
      // _mapGeneric.clear();
      // _mapGeneric = await _api.updateImageCodigoUnico(
      //   idCodigoUnico: event.code,
      //   jsonCargos: event.cargos,
      //   jsonEvidencia: event.evidencias,
      // );
    }
  }
}
