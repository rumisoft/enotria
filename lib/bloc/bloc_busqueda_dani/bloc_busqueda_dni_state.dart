part of 'bloc_busqueda_dni_bloc.dart';

class BlocBusquedaDniState {
  final bool search;
  final List<Datum> data;
  final String enterpriseName;
  final bool client;
  bool timeGlobal;
  int timer;
  BuildContext context;
  final List<Map<String, dynamic>> personList;
  final List<Map<String, dynamic>> addressList;
  final List<String> cargoList;
  final List<String> evidenciaList;

  BlocBusquedaDniState({
    this.search,
    this.data,
    this.enterpriseName,
    this.client,
    this.timeGlobal,
    this.context,
    this.timer,
    this.personList,
    this.addressList,
    this.cargoList,
    this.evidenciaList,
  });

  static BlocBusquedaDniState get initialState => BlocBusquedaDniState(
        search: false,
        data: [],
        enterpriseName: " ",
        client: false,
        timeGlobal: false,
        timer: 0,
        personList: [],
        addressList: [],
        cargoList: [],
        evidenciaList: [],
      );

  BlocBusquedaDniState copyWith({
    bool search,
    List<Datum> data,
    String enterpriseName,
    bool client,
    bool timeGlobal,
    BuildContext context,
    int timer,
    List<Map<String, dynamic>> personList,
    List<Map<String, dynamic>> addressList,
    List<String> cargoList,
    List<String> evidenciaList,
  }) {
    return BlocBusquedaDniState(
      search: search ?? this.search,
      data: data ?? this.data,
      enterpriseName: enterpriseName ?? this.enterpriseName,
      client: client ?? this.client,
      timeGlobal: timeGlobal ?? this.timeGlobal,
      context: context ?? this.context,
      timer: timer ?? this.timer,
      personList: personList ?? this.personList,
      addressList: addressList ?? this.addressList,
      cargoList: cargoList ?? this.cargoList,
      evidenciaList: evidenciaList ?? this.evidenciaList,
    );
  }
}
