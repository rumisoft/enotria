part of 'bloc_busqueda_dni_bloc.dart';

@immutable
abstract class BlocBusquedaDniEvent {}

class SearchByDocument extends BlocBusquedaDniEvent {
  final String document;
  final BuildContext contex;

  SearchByDocument({this.document, this.contex});
}

class SendConsultDni extends BlocBusquedaDniEvent {
  final Datum datum;
  final BuildContext context;

  SendConsultDni({this.context, this.datum});
}

class TimeReload extends BlocBusquedaDniEvent {
  final int time;

  TimeReload({this.time});
}
