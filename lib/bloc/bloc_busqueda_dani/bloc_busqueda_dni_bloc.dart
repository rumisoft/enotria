import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_dni.dart';
import 'package:enotria/model/modelDni.dart';
import 'package:enotria/pages/page_resumen_homologacion.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_restart/flutter_restart.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

part 'bloc_busqueda_dni_event.dart';
part 'bloc_busqueda_dni_state.dart';

class BlocBusquedaDniBloc
    extends Bloc<BlocBusquedaDniEvent, BlocBusquedaDniState> {
  BlocBusquedaDniBloc() : super(BlocBusquedaDniState.initialState);

  ApiDni _api = new ApiDni();
  Map<String, dynamic> _mapApi = new Map<String, dynamic>();
  ModelDni _modelDni = new ModelDni();
  Timer timer;

  @override
  Stream<BlocBusquedaDniState> mapEventToState(
    BlocBusquedaDniEvent event,
  ) async* {
    if (event is SearchByDocument) {
      BuildContext _contex = event.contex;
      yield this.state.copyWith(
            search: true,
            data: [],
            enterpriseName: " ",
          );

      if (event.document.length == 8) {
        /*_DNI */
        print("DNI");
        _mapApi = await _api.getByDocument(event.document);
      } else if (event.document.length == 15) {
        /*_UNICO */
        _mapApi = await _api.getPedido(event.document);
      } else if (event.document.substring(0, 3) == "PED") {
        /*_PEDIDO*/
        print("PED");
        _mapApi = await _api.getPedido(event.document);
      }

      yield this.state.copyWith(
            search: false,
          );

      print("rpta consulta----${_mapApi}");
      if (_mapApi["data"]["error"] ?? false) {
        showTopSnackBar(
          _contex,
          CustomSnackBar.error(
            message: _mapApi["data"]["message"].toString(),
          ),
        );
        yield this.state.copyWith(
              search: false,
            );
      }
      if (_mapApi["data"]["err"] ?? false) {
        showTopSnackBar(
          _contex,
          CustomSnackBar.error(
            message: _mapApi["data"]["msg"].toString(),
          ),
        );
        yield this.state.copyWith(
              search: false,
            );
      }
      if (_mapApi["data"]["error"]?.toString() == "¡Sesión inválida!") {
        var f = await Get.dialog(
          AlertDialog(
            title: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 50,
                    width: 50,
                    child: SvgPicture.asset(
                      "assets/svg/tiempo.svg", color: UtilsColors.cris1,
                      // color: ViteColors.primary,
                    ),
                  ),
                  SizedBox(height: 25),
                  Text(
                    _mapApi["data"]["error"]?.toString(),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: UtilsColors.cris1,
                        fontSize: 12),
                  ),
                  RoundedButton(
                    color: UtilsColors.secundary,
                    text: "Aceptar",
                    press: () async {
                      // ignore: unused_local_variable
                      final result = await FlutterRestart.restartApp();
                    },
                    loading: false,
                  ),
                ],
              ),
            ),
            backgroundColor: UtilsColors.white,
          ),
          barrierDismissible: false,
        );
      }
      //if (!_mapApi["data"]["status"])
      if (!_mapApi["data"]["status"]) {
        showTopSnackBar(
          _contex,
          CustomSnackBar.error(
            message: _mapApi["data"]["data"]["error"].toString(),
          ),
        );
        yield this.state.copyWith(
              search: false,
            );
      }
     
      
       else {
        List<Map<String, dynamic>> _personList = [];
        List<Map<String, dynamic>> _addressList = [];
        Map<String, dynamic> _mapGeneric = Map<String, dynamic>();
        _modelDni = ModelDni.fromJson(_mapApi["data"]);

        if (_modelDni.data.length == 0) {
          showTopSnackBar(
            _contex,
            CustomSnackBar.error(
              message: "No se encontraron pedidos activos",
            ),
          );
          yield this.state.copyWith(search: false);
        } else {
          for (_mapGeneric in _mapApi["data"]["data"][0]["deliveryPerson"]) {
            _personList.add(_mapGeneric);
          }

          for (_mapGeneric in _mapApi["data"]["data"][0]["deliveryAddress"]) {
            _addressList.add(_mapGeneric);
          }

          List<String> _cargos = ["x", "x", "x", "x"];
          List<String> _evidencia = ["x", "x", "x"];

          for (int _mapGeneric in _mapApi["data"]["data"][0]["imagesCargo"]) {
            for (int i = 0; i <= _cargos.length - 1; i++) {
              if (i == _mapGeneric) {
                _cargos[i] = "yes";
              }
            }
          }

          for (int _mapGeneric2 in _mapApi["data"]["data"][0]
              ["imagesSupport"]) {
            for (int i = 0; i <= _evidencia.length - 1; i++) {
              if (i == _mapGeneric2) {
                _evidencia[i] = "yes";
              }
            }
          }

          yield this.state.copyWith(
                search: false,
                data: _modelDni.data,
                enterpriseName: _modelDni.data[0].deliveryPerson[0].fullName,
                personList: _personList,
                addressList: _addressList,
                cargoList: _cargos,
                evidenciaList: _evidencia,
              );
        }
      }
    } else if (event is SendConsultDni) {
      print("EL DATO SELECCIONADO ES: ${event.datum.product.productName}");
      state.timeGlobal = false;
      final route = MaterialPageRoute(
          builder: (BuildContext context) => PageDniConsult(
                data: event.datum,
              ));

      Navigator.push(event.context, route);
      BlocBusquedaDniBloc().close();
      BlocBusquedaDniBloc().close();
    } else if (event is TimeReload) {
      yield this.state.copyWith(timer: event.time);
    }
  }
}

class LoginX extends GetxController {
  RxBool result = false.obs;
}
