// import 'package:enotria/api/apiEstados.dart';
// import 'package:enotria/model/EstadoProductoModel.dart';
// import 'package:rxdart/rxdart.dart';

// class EstadoProductoBloc {
//   final estadosApi = EstadosApi();

//   final _estadosProductoController = BehaviorSubject<List<ValidacionBioModel>>();

//   Stream<List<ValidacionBioModel>> get validacionesEstadoStream =>
//       _estadosProductoController.stream;

//   dispose() {
//     _estadosProductoController?.close();
//   }

//   void obtenerValidaciones(String id) async {
//     _estadosProductoController.sink.add(await estadosApi.validacionesApi(id));
//   }
// }
