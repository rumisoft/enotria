part of 'bloc_resumen_finalize_bloc.dart';

@immutable
class BlocResumenFinalizeState {

  final bool proccessin;
  final bool status;

  BlocResumenFinalizeState({this.proccessin, this.status});

  static BlocResumenFinalizeState get initialState => new BlocResumenFinalizeState(
        proccessin: false,
        status: false,
      );

  BlocResumenFinalizeState copyWith({bool proccessin, bool status}) {
    return BlocResumenFinalizeState(
      proccessin: proccessin ?? this.proccessin,
      status: status ?? this.status,
    );
  }
}

