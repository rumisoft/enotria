part of 'bloc_resumen_finalize_bloc.dart';

abstract class BlocResumenFinalizeEvent {}

class SendResumenEnd extends BlocResumenFinalizeEvent {
  BuildContext contex;
  final String idpedido;
  final String latitud;
  final String longitud;
  final String statusType;
  final String subStatusType;
  final String date;
  final String fullName;
  final String documentType;
  final String documentId;
//status biometria
  final String statusTypeCodeBio;
  final String statusTypeBio;
  final String subStatusTypeCodeBio;
  final String subStatusTypeBio;
  final String deliveryDateBio;
  final String cargoPathReniec;

  final List<String> cargos;
  final List<String> evidencia;
  final Position position;
  final String codEstado;
  final String codSubEstado;

  SendResumenEnd({
    this.contex,
    this.idpedido,
    this.latitud,
    this.longitud,
    this.statusType,
    this.subStatusType,
    this.date,
    this.statusTypeCodeBio,
    this.statusTypeBio,
    this.subStatusTypeCodeBio,
    this.subStatusTypeBio,
    this.deliveryDateBio,
    this.cargoPathReniec,
    this.fullName,
    this.documentType,
    this.documentId,
    this.cargos,
    this.evidencia,
    this.position,
    this.codEstado,
    this.codSubEstado,
  });
}

class SendResumenUpdateImage extends BlocResumenFinalizeEvent {
  BuildContext contex;
  final String idpedido;
  final String deliveryCode;
  final List<String> cargos;
  final List<String> evidencia;

  SendResumenUpdateImage(
      {this.contex,
      this.deliveryCode,
      this.idpedido,
      this.cargos,
      this.evidencia});
}
