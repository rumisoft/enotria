import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_resumen.dart';
import 'package:enotria/database/data_base_provider.dart';
import 'package:enotria/database/log_data_base.dart';
import 'package:enotria/widget/all/dialogGetx.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
part 'bloc_resumen_finalize_event.dart';
part 'bloc_resumen_finalize_state.dart';

class DataUrl {
  int id;
  String url;

  DataUrl(this.id, this.url);
}

class BlocResumenFinalizeBloc
    extends Bloc<BlocResumenFinalizeEvent, BlocResumenFinalizeState> {
  BlocResumenFinalizeBloc() : super(BlocResumenFinalizeState.initialState);
  ApiResumen _api = new ApiResumen();
  final LogDb = LogDataBase();
  Map<String, dynamic> _map = new Map<String, dynamic>();
  BuildContext bycontex;
  List<dynamic> lisCargoUrl = ["", "", "", ""];
  List<dynamic> lisEvidenciaUrl = ["", "", ""];
  final df = new DateFormat('dd/MM/yyyy hh:mm:ss a');
  String _datetime;
  int id, timer = 0;
  @override
  Stream<BlocResumenFinalizeState> mapEventToState(
    BlocResumenFinalizeEvent event,
  ) async* {
    if (event is SendResumenEnd) {
      yield this.state.copyWith(proccessin: false);

      //instancia de la bd
      final dbprovider = DatabaseProvider.db;
      final db = await dbprovider.database;

      this._datetime = df.format(DateTime.now());

      await db.transaction((txn) async {
        id = await txn.rawInsert(
            'INSERT INTO DataLog(usuario ,pedido ,cliente ,direccion , horaI , cargo1 , cargo2 , cargo3 , cargo4 , evide1 ,evide2 ,evide3 ,detalle ,horaF , total ) VALUES(  "usuario" ,"${event.idpedido}" , "${event.fullName}" , "${event.latitud} - ${event.longitud}", "${this._datetime}" , "cargo1" , "cargo2" , "cargo3" , "cargo4" , "evide1" ,"evide2" ,"evide3","detalle" ,"horaF" ,"total")');
      });
      
      DateTime _entrada = DateTime.parse(formatISOTime(DateTime.now()));

      String dateC1 = df.format(DateTime.now());
      String _mapCargo1 = (event.cargos[0].toString() == "")
          ? ""
          : await _api.sendFile("${event.idpedido}_cargo", event.cargos[0]);
      lisCargoUrl[0] = (_mapCargo1);
      String dateC2 = df.format(DateTime.now());
      String mode = (event.cargos[0].toString() == "")
          ? "no file"
          : "${dateC1.toString()} ${dateC2.toString()}";

      //actualizar
      await LogDb.updateData(mode, id);

      String dateC3 = df.format(DateTime.now());
      String _mapCargo2 = (event.cargos[1].toString() == "")
          ? ""
          : await _api.sendFile("${event.idpedido}_cargo", event.cargos[1]);
      lisCargoUrl[1] = (_mapCargo2);
      String dateC4 = df.format(DateTime.now());
      String mode2 = (event.cargos[1].toString() == "")
          ? "no file"
          : "${dateC3.toString()} ${dateC4.toString()}";

      await LogDb.updateDataCargo2(mode2, id);

      String dateC5 = df.format(DateTime.now());
      String _mapCargo3 = (event.cargos[2].toString() == "")
          ? ""
          : await _api.sendFile("${event.idpedido}_cargo", event.cargos[2]);
      lisCargoUrl[2] = (_mapCargo3);
      String dateC6 = df.format(DateTime.now());
      String mode3 = (event.cargos[2].toString() == "")
          ? "no file"
          : "${dateC5.toString()} ${dateC6.toString()}";

      await LogDb.updateDataCargo3(mode3, id);

      String dateC7 = df.format(DateTime.now());
      String _mapCargo4 = (event.cargos[3].toString() == "")
          ? ""
          : await _api.sendFile("${event.idpedido}_cargo", event.cargos[3]);
      lisCargoUrl[3] = (_mapCargo4);
      String dateC8 = df.format(DateTime.now());
      String mode4 = (event.cargos[3].toString() == "")
          ? "no file"
          : "${dateC7.toString()} ${dateC8.toString()}";

      await LogDb.updateDataCargo4(mode4, id);

      String dateE9 = df.format(DateTime.now());
      String _evidencia1 = (event.evidencia[0].toString() == "")
          ? ""
          : await _api.sendFile(
              "${event.idpedido}_evidencia", event.evidencia[0]);
      lisEvidenciaUrl[0] = (_evidencia1);
      String dateE10 = df.format(DateTime.now());
      String mode5 = (event.evidencia[0].toString() == "")
          ? "no file"
          : "${dateE9.toString()} ${dateE10.toString()}";

      //actualizar bd
      int count5 = await LogDb.updateDataEvidencia(mode5, id);
      print('updated: $count5');

      String dateE11 = df.format(DateTime.now());
      String _evidencia2 = (event.evidencia[1].toString() == "")
          ? ""
          : await _api.sendFile(
              "${event.idpedido}_evidencia", event.evidencia[1]);
      lisEvidenciaUrl[1] = (_evidencia2);
      String dateE12 = df.format(DateTime.now());
      String mode6 = (event.evidencia[1].toString() == "")
          ? "no file"
          : "${dateE11.toString()} ${dateE12.toString()}";

      //actualizar
      int count6 = await LogDb.updateDataEvidencia2(mode6, id);
      print('updated: $count6');

      String dateE13 = df.format(DateTime.now());
      String _evidencia3 = (event.evidencia[2].toString() == "")
          ? ""
          : await _api.sendFile(
              "${event.idpedido}_evidencia", event.evidencia[2]);
      lisEvidenciaUrl[2] = (_evidencia3);
      String dateE14 = df.format(DateTime.now());
      String mode7 = (event.evidencia[2].toString() == "")
          ? "no file"
          : "${dateE13.toString()} ${dateE14.toString()}";

      //actualizar
      int count7 = await LogDb.updateDataEvidencia3(mode7, id);
      print('updated: $count7');

      print("--->LIS CARGOS: ${jsonEncode(lisCargoUrl)}");
      print("--->LIS EVIDENCIAS: ${jsonEncode(lisEvidenciaUrl)}");

      bycontex = event.contex;

      try {
        if (_mapCargo1 == "err" ||
            _mapCargo2 == "err" ||
            _mapCargo3 == "err" ||
            _mapCargo4 == "err" ||
            _evidencia1 == "err" ||
            _evidencia2 == "err" ||
            _evidencia3 == "err") {
          showTopSnackBar(
            bycontex,
            CustomSnackBar.error(
              message: "Error al enviar Cargo y/o Evidencia.",
            ),
          );
          yield this.state.copyWith(proccessin: true, status: false);
        } else {
          String dateC16 = df.format(DateTime.now());
          _map = await _api.resumenFinalize(
            event.idpedido,
            event.latitud,
            event.longitud,
            event.statusType,
            event.subStatusType,
            event.date,
            event.statusTypeCodeBio,
            event.statusTypeBio,
            event.subStatusTypeCodeBio,
            event.subStatusTypeBio,
            event.deliveryDateBio,
            event.cargoPathReniec,
            event.fullName,
            event.documentType,
            event.documentId,
            lisCargoUrl,
            lisEvidenciaUrl,
            event.position,
            event.codEstado,
            event.codSubEstado,
          );

          this._datetime = df.format(DateTime.now());

          Duration _diastotales = DateTime.now().difference(_entrada);

          await LogDb.updateDireccion(
              event.position.latitude,
              event.position.longitude,
              dateC16.toString(),
              this._datetime,
              _datetime,
              _diastotales.inHours,
              _diastotales.inMinutes,
              _diastotales.inSeconds,
              id);
          // await database.rawUpdate(
          //     'UPDATE DataLog SET direccion = ?,detalle = ?,horaF = ?, total=? WHERE id = ?',
          //     [
          //       "${event.position.latitude} ${event.position.longitude}",
          //       "${dateC16.toString()} ${this._datetime}",
          //       _datetime,
          //       "${_diastotales.inHours}:${_diastotales.inMinutes}:${_diastotales.inSeconds}",
          //       id
          //     ]);

          bool date = _map["data"]["err"] ?? true;
          String sesion = _map["data"]["error"] ?? "false";
          if (sesion != "false") {
            showTopSnackBar(
              bycontex,
              CustomSnackBar.error(
                message: _map["data"]["error"],
              ),
            );
            yield this.state.copyWith(proccessin: true, status: false);
          } else if (!date) {
            print("---------------------->1");
            showTopSnackBar(
              bycontex,
              CustomSnackBar.info(
                message: _map["data"]["msg"],
              ),
            );
            yield this.state.copyWith(proccessin: true, status: true);
          } else if (date) {
            print("---------------------->2");
            showTopSnackBar(
              bycontex,
              CustomSnackBar.error(
                message: _map["data"]["msg"],
              ),
            );
            yield this.state.copyWith(proccessin: true, status: false);
          }
        }
      } on Exception catch (_) {
        print("---------------------->3");
        showTopSnackBar(
          bycontex,
          CustomSnackBar.info(
            message: _map["data"],
          ),
        );
        yield this.state.copyWith(proccessin: true, status: false);
      }
    } else if (event is SendResumenUpdateImage) {
      print(
          "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
      print("----> ${event.deliveryCode}");
      print("----> ${event.idpedido}");
      print("----> ${event.cargos}");
      print("----> ${event.evidencia}");
      print(
          "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");

      yield this.state.copyWith(proccessin: false);

      String _mapCargo1 = (event.cargos[0].toString() == "")
          ? ""
          : await _api.sendFile("${event.idpedido}_cargo", event.cargos[0]);
      lisCargoUrl[0] = (_mapCargo1);

      String _mapCargo2 = (event.cargos[1].toString() == "")
          ? ""
          : await _api.sendFile("${event.idpedido}_cargo", event.cargos[1]);
      lisCargoUrl[1] = (_mapCargo2);

      String _mapCargo3 = (event.cargos[2].toString() == "")
          ? ""
          : await _api.sendFile("${event.idpedido}_cargo", event.cargos[2]);
      lisCargoUrl[2] = (_mapCargo3);

      String _mapCargo4 = (event.cargos[3].toString() == "")
          ? ""
          : await _api.sendFile("${event.idpedido}_cargo", event.cargos[3]);
      lisCargoUrl[3] = (_mapCargo4);

      print("--->ENVIANDO CRGOS--------------");

      String _evidencia1 = (event.evidencia[0].toString() == "")
          ? ""
          : await _api.sendFile(
              "${event.idpedido}_evidencia", event.evidencia[0]);
      lisEvidenciaUrl[0] = (_evidencia1);

      String _evidencia2 = (event.evidencia[1].toString() == "")
          ? ""
          : await _api.sendFile(
              "${event.idpedido}_evidencia", event.evidencia[1]);
      lisEvidenciaUrl[1] = (_evidencia2);

      String _evidencia3 = (event.evidencia[2].toString() == "")
          ? ""
          : await _api.sendFile(
              "${event.idpedido}_evidencia", event.evidencia[2]);
      lisEvidenciaUrl[2] = (_evidencia3);

      print("--->LIS CARGOS: ${jsonEncode(lisCargoUrl)}");
      print("--->LIS EVIDENCIAS: ${jsonEncode(lisEvidenciaUrl)}");

      bycontex = event.contex;

      var type = event.idpedido.substring(0, 3);
      if (type == "PED") {
        _map = await _api.updateImagePedido(
          idCodigoUnico: event.idpedido,
          jsonCargos: lisCargoUrl,
          jsonEvidencia: lisEvidenciaUrl,
        );
      } else {
        _map = await _api.updateImageCodigoUnico(
          idCodigoUnico: event.idpedido,
          jsonCargos: lisCargoUrl,
          jsonEvidencia: lisEvidenciaUrl,
        );
      }

      try {
        bool date = _map["data"]["err"] ?? true;
        String sesion = _map["data"]["error"] ?? "false";
        if (sesion != "false") {
          showTopSnackBar(
            bycontex,
            CustomSnackBar.error(
              message: _map["data"]["error"],
            ),
          );
          yield this.state.copyWith(proccessin: true, status: false);
        } else if (!date) {
          print("---------------------->1");
          if (_map["data"]["data"] ==
              "Codigo unico no le pertenece a este courier") {
            showTopSnackBar(
              bycontex,
              CustomSnackBar.error(
                message: _map["data"]["data"],
              ),
            );
            yield this.state.copyWith(proccessin: true, status: false);
          } else {
            showTopSnackBar(
              bycontex,
              CustomSnackBar.info(
                message: _map["data"]["data"],
              ),
            );
            yield this.state.copyWith(proccessin: true, status: true);
          }
        } else if (date) {
          print("---------------------->2");
          showTopSnackBar(
            bycontex,
            CustomSnackBar.error(
              message: _map["data"]["msg"],
            ),
          );
          yield this.state.copyWith(proccessin: true, status: false);
        }
      } on Exception catch (_) {
        print("---------------------->3");
        showTopSnackBar(
          bycontex,
          CustomSnackBar.info(
            message: _map["data"],
          ),
        );
        yield this.state.copyWith(proccessin: true, status: false);
      }
    }
  }
}
