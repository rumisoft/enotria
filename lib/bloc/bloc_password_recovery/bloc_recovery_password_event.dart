part of 'bloc_recovery_password_bloc.dart';

@immutable
abstract class BlocRecoveryPasswordEvent {}

class ValidateUser extends BlocRecoveryPasswordEvent {
  final String user;
  final BuildContext context;
  final String type;

  ValidateUser({this.user, this.context,this.type="no"});
}

class ValidateCodeSms extends BlocRecoveryPasswordEvent {
  final String user;
  final String code;
  final BuildContext contextSms;

  ValidateCodeSms({this.user, this.code, this.contextSms});
}

class Process extends BlocRecoveryPasswordEvent {}

class NewPassword extends BlocRecoveryPasswordEvent {
  final BuildContext contextNew;
  final String pass1;
  final String pass2;
  final String hash;

  NewPassword({this.contextNew, this.pass1, this.pass2, this.hash});
}

class TimerSend extends BlocRecoveryPasswordEvent {
  final BuildContext context;

  TimerSend({this.context});
}

class CountTimer extends BlocRecoveryPasswordEvent {
  final int timer;

  CountTimer({this.timer});
}
