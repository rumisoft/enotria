part of 'bloc_recovery_password_bloc.dart';

class BlocRecoveryPasswordState {
  final String user;
  final String code;
  final String newpassword1;
  final String newpassword2;
  final bool process;
  final bool newPasswordStatus;
  int timeSend;

  BlocRecoveryPasswordState({
    this.user,
    this.code,
    this.newpassword1,
    this.newpassword2,
    this.process,
    this.newPasswordStatus,
    this.timeSend,
  });

  static BlocRecoveryPasswordState get initState =>
      new BlocRecoveryPasswordState(
          user: "",
          code: "",
          newpassword1: "",
          newpassword2: "",
          process: false,
          newPasswordStatus: false,
          timeSend: 0);
  BlocRecoveryPasswordState copyWith(
      {String user,
      String code,
      String newpassword1,
      String newpassword2,
      bool process,
      bool newPasswordStatus,
      int timeSend}) {
    return BlocRecoveryPasswordState(
        user: user ?? this.user,
        code: code ?? this.code,
        newpassword1: newpassword1 ?? this.newpassword1,
        newpassword2: newpassword2 ?? this.newpassword2,
        process: process ?? this.process,
        newPasswordStatus: newPasswordStatus ?? this.newPasswordStatus,
        timeSend: timeSend ?? this.timeSend);
  }
}
