import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_recoveryPassword.dart';
import 'package:enotria/pages/page_login.dart';
import 'package:enotria/pages/recuperar_contrasenia/page_end.dart';
import 'package:enotria/pages/recuperar_contrasenia/page_new_password.dart';
import 'package:enotria/pages/recuperar_contrasenia/page_validate_code.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

part 'bloc_recovery_password_event.dart';
part 'bloc_recovery_password_state.dart';

class BlocRecoveryPasswordBloc
    extends Bloc<BlocRecoveryPasswordEvent, BlocRecoveryPasswordState> {
  BlocRecoveryPasswordBloc() : super(BlocRecoveryPasswordState.initState);
  Map<String, dynamic> _map = Map<String, dynamic>();
  RecoveryPassword _api = new RecoveryPassword();
  Timer timer;
  timerOuth(BuildContext context) async {
    timer = Timer.periodic(Duration(seconds: 1), (t) async {
      state.timeSend++;
      print("------------------------- timer ${state.timeSend}");
      add(CountTimer(timer: state.timeSend));
      if (state.timeSend == 6) {
        t.cancel();
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
              builder: (context) => PageLogin(),
            ),
            (Route<dynamic> route) => false);
      }
    });
  }

  @override
  Stream<BlocRecoveryPasswordState> mapEventToState(
      BlocRecoveryPasswordEvent event) async* {
    if (event is ValidateUser) {
      try {
        yield this.state.copyWith(process: true);
        this._map = await _api.validatePassword(user: event.user);
        print("------------MAP------------------ ${_map['data']}");
        if (_map["data"]["err"] == true) {
          showTopSnackBar(
            event.context,
            CustomSnackBar.error(
              message: _map["data"]["message"] ?? _map["data"]["data"],
            ),
          );
        }
        if (_map["data"]["err"] == false) {
          print("--->aqui-- ${_map["data"]["data"]["telephone"]}");
          if (event.type != "yes") {
            showTopSnackBar(
              event.context,
              CustomSnackBar.info(
                message: _map["data"]["data"]["message"],
              ),
            );
          }

          Navigator.of(event.context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (context) => ValidateCode(user: event.user,phone:_map["data"]["data"]["telephone"] ,),
              ),
              (Route<dynamic> route) => false);

          //Navigator.pushAndRemoveUntil(context,  MaterialPageRoute(builder: (context) => NewPasswordPage(hash : "Hello World")), (route) => false);
        }
        yield this.state.copyWith(process: false);
      } catch (ex) {
        showTopSnackBar(
          event.context,
          CustomSnackBar.error(
            message: "Error de conexión",
          ),
        );
        yield this.state.copyWith(process: false);
      }
    } else if (event is ValidateCodeSms) {
      yield this.state.copyWith(process: true);
      _map = await _api.validateCode(
        user: event.user,
        code: event.code,
      );
      print("::::> JSON MAP $_map");

      if (!_map["data"]["data"]["ok"]) {
        yield this.state.copyWith(process: false);
        showTopSnackBar(
          event.contextSms,
          CustomSnackBar.error(
            message: "Código Incorrecto",
          ),
        );
      } else if (_map["data"]["data"]["ok"]) {
        yield this.state.copyWith(process: false);
        String token = _map["data"]["data"]["data"]["token"];
        print("TOKEN ::::::::::: ${token.toString()}");

        Navigator.of(event.contextSms).pushAndRemoveUntil(
            MaterialPageRoute(
              builder: (context) => NewPasswordPage(hash: token),
            ),
            (Route<dynamic> route) => false);
      }
      yield this.state.copyWith(process: false);
    } else if (event is Process) {
      yield this.state.copyWith(process: false);
    } else if (event is NewPassword) {
      yield this.state.copyWith(process: true);
      _map = await _api.newPassword(
        hash: event.hash,
        pass1: event.pass1,
        pass2: event.pass2,
      );

      if (_map['data']['err']) {
        yield this.state.copyWith(process: false);
        print("ingreso...................");
        showTopSnackBar(
          event.contextNew,
          CustomSnackBar.error(
            message: "${_map['data']['data']}",
          ),
        );
      } else if (!_map['data']['err']) {
        yield this.state.copyWith(process: false);
        print("ingreso......no.............");
        showTopSnackBar(
          event.contextNew,
          CustomSnackBar.info(
            message: "${_map['data']['data']}",
          ),
        );
        Navigator.of(event.contextNew).pushAndRemoveUntil(
            MaterialPageRoute(
              builder: (context) => RecoveryPassEnd(
                status: true,
                response: "${_map['data']['data']}",
              ),
            ),
            (Route<dynamic> route) => false);
      }
      yield this.state.copyWith(process: false);
    } else if (event is TimerSend) {
      timerOuth(event.context);
    } else if (event is CountTimer) {
      yield this.state.copyWith(timeSend: event.timer);
    }
  }
}
