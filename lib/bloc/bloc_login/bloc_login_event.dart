part of 'bloc_login_bloc.dart';

@immutable
abstract class BlocLoginEvent {}
class LoginUser extends BlocLoginEvent{
  final BuildContext context;
  final String user;
  final String password;

  LoginUser({ this.context, this.user, this.password});
}
