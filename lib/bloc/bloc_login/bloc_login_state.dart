part of 'bloc_login_bloc.dart';

@immutable
// ignore: must_be_immutable
class BlocLoginState {
  final bool proccessin;
  final int attempts;
  int timeGlobal;
  bool isDialog;

  BlocLoginState(
      {this.proccessin, this.attempts, this.timeGlobal, this.isDialog});

  static BlocLoginState get initialState => new BlocLoginState(
        proccessin: false,
        attempts: 0,
        timeGlobal: 0,
        isDialog: false,
      );

  BlocLoginState copyWith({
    bool proccessin,
    int attempts,
    int timeGlobal,
    bool isDialog,
  }) {
    return BlocLoginState(
      proccessin: proccessin ?? this.proccessin,
      attempts: attempts ?? this.attempts,
      timeGlobal: timeGlobal ?? this.timeGlobal,
      isDialog: isDialog ?? this.isDialog,
    );
  }
}
