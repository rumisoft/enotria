import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_login.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/userPreferences.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:flutter_restart/flutter_restart.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

part 'bloc_login_event.dart';
part 'bloc_login_state.dart';

class BlocLoginBloc extends Bloc<BlocLoginEvent, BlocLoginState> {
  BlocLoginBloc() : super(BlocLoginState.initialState);
  ApiLogin _api = new ApiLogin();
  Map<String, dynamic> _map = new Map<String, dynamic>();

  UserPreferences _preferences = UserPreferences();
  Timer timer;

  timerOuth() async {
    timer = Timer.periodic(Duration(minutes: 1), (Timer t) async {
      state.timeGlobal++;

      if (state.isDialog == false &&
          state.timeGlobal >= int.parse(_preferences.consultationTime)) {
        state.isDialog = true;
        // ignore: unused_local_variable
        var f = await Get.dialog(
          AlertDialog(
            title: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 50,
                    width: 50,
                    child: SvgPicture.asset(
                      "assets/svg/tiempo.svg", color: UtilsColors.cris1,
                      // color: ViteColors.primary,
                    ),
                  ),
                  SizedBox(height: 25),
                  Text(
                    "SESIÓN EXPIRADA",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: UtilsColors.cris1,
                        fontSize: 12),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text(
                      "El tiempo de consulta caduco.",
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: UtilsColors.secundary,
                          fontSize: 11),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  RoundedButton(
                    color: UtilsColors.secundary,
                    text: "Aceptar",
                    press: () async {
                      // ignore: unused_local_variable
                      final result = await FlutterRestart.restartApp();
                    },
                    loading: false,
                  ),
                ],
              ),
            ),
            backgroundColor: UtilsColors.white,
          ),
          barrierDismissible: false,
        );
      }
    });
  }

  @override
  Stream<BlocLoginState> mapEventToState(
    BlocLoginEvent event,
  ) async* {
    if (event is LoginUser) {
      //Navigator.pushReplacementNamed(event.context, 'Resumen');
      yield this.state.copyWith(proccessin: true);

      //llamada a la api de login
      _map = await _api.userAuth(event.user ?? "", event.password ?? "");

      print("$_map");
      if (_map["code"] == 0) {
        showTopSnackBar(
          event.context,
          CustomSnackBar.error(
            message: "Conexión fallida",
          ),
        );
        yield this.state.copyWith(proccessin: false);
      } else if (_map["data"]["err"] ?? false) {
        showTopSnackBar(
          event.context,
          CustomSnackBar.error(
            message: _map["data"]["message"] ??
                "No se puede establecer una conexión ",
          ),
        );
        yield this.state.copyWith(proccessin: false);
      } else {
        timerOuth();

        try {
          _preferences.user = _map["data"]["user"];
          _preferences.token = _map["data"]["token"];
          _preferences.lastName = _map["data"]["lastName"];
          _preferences.firstName = _map["data"]["firstName"];
          _preferences.userType = _map["data"]["userType"] ?? "-";
          _preferences.courierCode = _map["data"]["courierCode"] ?? "-";
          _preferences.needsPasswdChange = _map["data"]["needsPasswdChange"];
          _preferences.downtime = _map["data"]["downtime"];
          _preferences.consultationTime = _map["data"]["consultationTime"];
          yield this.state.copyWith(proccessin: false);
          Navigator.of(event.context).pushNamedAndRemoveUntil(
              'PermissionLocation', (Route<dynamic> route) => false);
        } catch (_) {
          showTopSnackBar(
            event.context,
            CustomSnackBar.error(
              message: _.toString(),
            ),
          );
        }
      }
    }
  }
}

class LoginX extends GetxController {
  RxBool result = false.obs;
}
