part of 'bloc_consult_bloc.dart';

class BlocConsultState {
  final List<Map<String, dynamic>> personList;
  final List<Map<String, dynamic>> addressList;
  final String productName;
  final String productCode;
  final bool search;
  BuildContext context;
  int timer;
  bool timeGlobal;
  int codeleng;
  String enterpriseName;
  String deliveryCode;
  String selectAddress;
  String selectPerson;
  Map<String, dynamic> selectAddressMap;
  Map<String, dynamic> selectPersonMap;
  String document;
  BlocConsultState({
    this.personList,
    this.addressList,
    this.selectAddress,
    this.selectPerson,
    this.productName,
    this.productCode,
    this.enterpriseName,
    this.deliveryCode,
    this.selectAddressMap,
    this.selectPersonMap,
    this.document,
    this.search,
    this.codeleng,
    this.timeGlobal,
    this.timer,
    this.context,
  });

  static BlocConsultState get initialState => new BlocConsultState(
      personList: [],
      selectPerson: "Seleccionar",
      selectAddress: "Seleccionar",
      deliveryCode: "",
      document: "",
      productName: "",
      productCode: "",
      enterpriseName: "",
      addressList: [],
      search: false,
      codeleng: 0,
      timeGlobal: false,
      timer: 0);

  BlocConsultState copyWith({
    List<Map<String, dynamic>> personList,
    List<Map<String, dynamic>> addressList,
    String selectAddress,
    String selectPerson,
    Map<String, dynamic> selectAddressMap,
    Map<String, dynamic> selectPersonMap,
    String productName,
    String productCode,
    String enterpriseName,
    String document,
    String deliveryCode,
    bool search,
    int codeleng,
    bool timeGlobal,
    int timer,
    BuildContext context,
  }) {
    return BlocConsultState(
        personList: personList ?? this.personList,
        addressList: addressList ?? this.addressList,
        selectAddress: selectAddress ?? this.selectAddress,
        document: document ?? this.document,
        selectPerson: selectPerson ?? this.selectPerson,
        productName: productName ?? this.productName,
        productCode: productCode ?? this.productCode,
        enterpriseName: enterpriseName ?? this.enterpriseName,
        deliveryCode: deliveryCode ?? this.deliveryCode,
        selectAddressMap: selectAddressMap ?? this.selectAddressMap,
        selectPersonMap: selectPersonMap ?? this.selectPersonMap,
        search: search ?? this.search,
        codeleng: codeleng ?? this.codeleng,
        timeGlobal: timeGlobal ?? this.timeGlobal,
        timer: timer ?? this.timer,
        context: context ?? this.context);
  }

  BlocConsultState reset() {
    Map<String, dynamic> selectAddressMap2 = new Map<String, dynamic>();
    Map<String, dynamic> selectPersonMap2 = new Map<String, dynamic>();
    return BlocConsultState(
        personList: [],
        addressList: [],
        document: "",
        selectAddressMap: selectAddressMap2,
        selectPersonMap: selectPersonMap2,
        productName: "",
        enterpriseName: "",
        deliveryCode: "");
  }
}
