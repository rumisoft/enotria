import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_resumen.dart';
import 'package:enotria/pages/page_resumen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

part 'bloc_consult_event.dart';
part 'bloc_consult_state.dart';

class BlocConsultBloc extends Bloc<BlocConsultEvent, BlocConsultState> {
  BlocConsultBloc() : super(BlocConsultState.initialState);

  ApiResumen _api = new ApiResumen();
  Timer timer;

  @override
  Stream<BlocConsultState> mapEventToState(BlocConsultEvent event) async* {
    if (event is GetOrder) {
      Map<String, dynamic> _map = Map<String, dynamic>();
      List<Map<String, dynamic>> _personList = [];
      List<Map<String, dynamic>> _addressList = [];
      Map<String, dynamic> _mapGeneric = Map<String, dynamic>();
      int countPerson = 0, countAddress = 0;

      yield this.state.copyWith(
            search: true,
            productName: "",
            enterpriseName: "",
          );
      try {
        state.reset();
        _map = await _api.getPedido(event.code);
        print("-------------- $_map");

        if (_map["data"]["err"] ?? false) {
          showTopSnackBar(
            event.context,
            CustomSnackBar.error(
              message: _map["data"]["msg"],
            ),
          );
          yield this.state.copyWith(search: false);
        } else if (_map["data"]["error"] ?? false) {
          showTopSnackBar(
            event.context,
            CustomSnackBar.error(
              message: _map["data"]["message"],
            ),
          );
          yield this.state.copyWith(search: false);
        } else {
          print(_map["data"]);
          print("-----------------------------------------------------");
          print(_map["data"]["data"]);

          for (_mapGeneric in _map['data']['data']) {
            for (_mapGeneric in _mapGeneric['deliveryPerson']) {
              print(
                  "-------------------------------INGRESE AJAJA--22--------------------");
              countPerson++;
              _personList.add(_mapGeneric);
            }
          }
          for (_mapGeneric in _map['data']['data']) {
            for (_mapGeneric in _mapGeneric['deliveryAddress']) {
              print(
                  "-------------------------------INGRESE AJAJA--23--------------------");
              countAddress++;
              _addressList.add(_mapGeneric);
            }
          }

          yield this.state.copyWith(
              personList: _personList,
              addressList: _addressList,
              document:
                  (countPerson >= 2) ? null : _personList[0]["documentId"],
              selectPerson: (countPerson >= 2)
                  ? "Seleccionar"
                  : _personList[0]["fullName"],
              selectPersonMap: (countPerson >= 2) ? null : _personList[0],
              selectAddress: (countAddress >= 2)
                  ? "Seleccionar"
                  : _addressList[0]["address"],
              selectAddressMap: (countAddress >= 2) ? null : _addressList[0],
              productName: _map['data']['data'][0]['product']['productName'],
              productCode: _map['data']['data'][0]['product']['productCode'],
              enterpriseName: _map['data']['data'][0]['enterprise']
                  ['enterpriseName'],
              deliveryCode: _map['data']['data'][0]["deliveryCode"],
              search: false);

          showTopSnackBar(
            event.context,
            CustomSnackBar.success(
              message: "${state.productName} encontrado.",
            ),
          );
        }
      } catch (e) {
        showTopSnackBar(
          event.context,
          CustomSnackBar.error(
            message: _map["data"],
          ),
        );
        yield this.state.copyWith(search: false);
      }
    } else if (event is SendResumenFinalize) {
      print(event.selectPersonMap.toString());
      print(
          ":::::::::::::::::ENVIANDO ::::::::::::::::::> ${event.productCode}");
      final route = MaterialPageRoute(
        builder: (BuildContext _) => PageResumen(
          type: "REGISTRO",
          productName: event.productName,
          productCode: event.productCode,
          enterpriseName: event.enterpriseName,
          deliveryCode: event.deliveryCode,
          code: event.deliveryCode,
          selectAddressMap: event.selectAddressMap,
          selectPersonMap: event.selectPersonMap,
          cargo: ["x", "x", "x", "x"],
          evidencia: ["x", "x", "x"],
        ),
      );

      Navigator.push(event.context, route);
    } else if (event is TimeReload) {
      print("ACTUALIZANDO TIEMPO A CONSULT ${event.time}");
      yield this.state.copyWith(timer: event.time);
    }
  }
}
