part of 'bloc_consult_bloc.dart';

@immutable
abstract class BlocConsultEvent {}

class GetOrder extends BlocConsultEvent {
  final String code;
  final BuildContext context;

  GetOrder({this.context, this.code});
}


class SendResumenFinalize extends BlocConsultEvent {
  final BuildContext context;
  final String productName;
  final String productCode;
  final String enterpriseName;
  final String deliveryCode;
  final String code;
  final Map<String, dynamic> selectAddressMap;
  final Map<String, dynamic> selectPersonMap;

  SendResumenFinalize({
    this.context,
    this.productName,
    this.productCode,
    this.code,
    this.enterpriseName,
    this.deliveryCode,
    this.selectAddressMap,
    this.selectPersonMap,
  });
}

class TimeReload extends BlocConsultEvent {
  final int time;

  TimeReload({this.time});
}
