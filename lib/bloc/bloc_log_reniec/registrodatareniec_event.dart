part of 'registrodatareniec_bloc.dart';

@immutable
abstract class RegistrodatareniecEvent {}

class EnviarDatosLogReniec extends RegistrodatareniecEvent {
  final LogBioModel logReniec;
   EnviarDatosLogReniec(this.logReniec);
}
