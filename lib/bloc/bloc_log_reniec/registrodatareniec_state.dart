part of 'registrodatareniec_bloc.dart';

@immutable
abstract class RegistrodatareniecState {
  //agregar estado inicial
  // final LogReniecModel logData;
  // const RegistrodatareniecState({this.logData});
}

class RegistrodatareniecInitial extends RegistrodatareniecState {
  RegistrodatareniecInitial({this.logData});
  final LogBioModel logData;

  // RegistrodatareniecInitial copyWith({LogReniecModel logData}) =>
  //     RegistrodatareniecInitial(logData:RegistrodatareniecBloc?? this.logData);
}
