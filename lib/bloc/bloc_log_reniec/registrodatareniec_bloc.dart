import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_logDataReniec.dart';
import 'package:enotria/model/log_data_reniec/logBiometriaModel.dart';
import 'package:meta/meta.dart';

part 'registrodatareniec_event.dart';
part 'registrodatareniec_state.dart';

class RegistrodatareniecBloc
    extends Bloc<RegistrodatareniecEvent, RegistrodatareniecState> {
  RegistrodatareniecBloc() : super(RegistrodatareniecInitial());

  @override
  Stream<RegistrodatareniecState> mapEventToState(
      RegistrodatareniecEvent event) async* {
    if (event is EnviarDatosLogReniec) {
      final apiDataLog = ApiLogDataReniec();
      print("entrando en bloc");
      yield RegistrodatareniecInitial(logData: event.logReniec);

      try {
        //llamar a la api de log
        apiDataLog.enviarDataLogReniec(event.logReniec);
        
      } catch (e) {
        print("error $e");
      }
    }
  }
}
