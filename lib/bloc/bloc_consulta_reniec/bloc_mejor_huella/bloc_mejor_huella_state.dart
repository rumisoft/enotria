part of 'bloc_mejor_huella_bloc.dart';


 class BlocMejorHuellaState {
  final ConsultaMejorHuellaReniecModel consultamejorHuellaModel;
  BlocMejorHuellaState({this.consultamejorHuellaModel});

    BlocMejorHuellaState copyWith({
    ConsultaMejorHuellaReniecModel consultamejorHuellaModel,
  }) {
    return BlocMejorHuellaState(
      consultamejorHuellaModel:
          consultamejorHuellaModel ?? this.consultamejorHuellaModel,
    );
  }
}


// class BlocPruebahuelleroState {
//   final IdentidadWsqModel identidadWsqModel;
//   BlocPruebahuelleroState({this.identidadWsqModel});

//   BlocPruebahuelleroState copyWith({
//     IdentidadWsqModel identidadWsqModel,
//   }) {
//     return BlocPruebahuelleroState(
//       identidadWsqModel: identidadWsqModel ?? this.identidadWsqModel,
//     );
//   }
// }
