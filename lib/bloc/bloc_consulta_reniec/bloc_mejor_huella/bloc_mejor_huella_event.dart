part of 'bloc_mejor_huella_bloc.dart';

@immutable
abstract class BlocMejorHuellaEvent {}

class ObtenerMejorHuellaEvent extends BlocMejorHuellaEvent {
  final Map<String, dynamic> persona;
  final String codPedido;

  ObtenerMejorHuellaEvent(this.persona, this.codPedido);
}
