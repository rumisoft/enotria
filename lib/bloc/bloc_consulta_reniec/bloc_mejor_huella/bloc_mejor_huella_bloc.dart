import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_consultaDatosReniec.dart';
import 'package:enotria/api/api_logDataReniec.dart';
import 'package:enotria/model/consulta_reniec/consultaMejorHuellaReniecModel.dart';
import 'package:enotria/model/log_data_reniec/logMejorHuellaModel.dart';
import 'package:enotria/util/userPreferences.dart';
import 'package:meta/meta.dart';

part 'bloc_mejor_huella_event.dart';
part 'bloc_mejor_huella_state.dart';

class BlocMejorHuellaBloc
    extends Bloc<BlocMejorHuellaEvent, BlocMejorHuellaState> {
  BlocMejorHuellaBloc() : super(BlocMejorHuellaState());

  @override
  Stream<BlocMejorHuellaState> mapEventToState(
      BlocMejorHuellaEvent event) async* {
    if (event is ObtenerMejorHuellaEvent) {
      final apiconsultaDatos = ApiConsultaDatosReniec();
      final logDataModel = LogMejorHuellaModel();
      UserPreferences _pre = UserPreferences();

      try {
        //obtener la fecha actual
        DateTime horaConsulta = DateTime.now();
        // //llamar a la api de log
        final res = await apiconsultaDatos
            .getMejorHuellaReniec(
            //"72676295",
              event.persona["documentId"]
              );

        //instanciar el objeto de model
        final consultaHuellaModel = ConsultaMejorHuellaReniecModel();
        // //llenar el objeto con la data del api
        consultaHuellaModel.coResponse = res["coResponse"];
        consultaHuellaModel.msgResponse = res["msgResponse"];
        consultaHuellaModel.coHuellaDer = res["coHuellaDer"];
        consultaHuellaModel.descHuellaDer = res["descHuellaDer"];
        consultaHuellaModel.coHuellaIzq = res["coHuellaIzq"];
        consultaHuellaModel.descHuellaIzq = res["descHuellaIzq"];
        consultaHuellaModel.error = res["error"];

        //llenar en el objeto todo el modelo con la rpta de la api de biometria
        logDataModel.type = "***Verificar Mejor Huella";
        logDataModel.firstName = event.persona["fullName"];
        logDataModel.lastName = event.persona["fullName"];
        logDataModel.mobilePhoneNumber = event.persona["phoneNumber"];
        logDataModel.reniecRequestHour = horaConsulta.toString();
        logDataModel.logDescription = consultaHuellaModel.msgResponse == 'OK'
            ? "Mejor Huella ${consultaHuellaModel.descHuellaDer}"
            : consultaHuellaModel.error == null
                ? consultaHuellaModel.msgResponse
                : "Error al realizar la solicitud a https://devsvb.reniec.gob.pe";
        logDataModel.logDate = horaConsulta.toString();
        logDataModel.documentType = event.persona["documentType"];
        logDataModel.transactionStartDate = horaConsulta.toString();
        logDataModel.requestMessage = consultaHuellaModel.msgResponse;
        logDataModel.reniecRequestId = '';
        logDataModel.generatedChargeDate = horaConsulta.toString();
        logDataModel.movilMacId = _pre.mac;
        logDataModel.reniecFullName = event.persona["fullName"];
        logDataModel.methodType = "***Verificar Mejor Huella";
        logDataModel.transactionEndDate = horaConsulta.toString();
        //consultaBiometriaModel.tiempoRespuesta
        logDataModel.reniecResponse = consultaHuellaModel.msgResponse;
        //consultaBiometriaModel.descripResultado
        logDataModel.reniecRequestDate = horaConsulta.toString();
        //consultaBiometriaModel.tiempoRespuesta
        logDataModel.requestedOrderId = event.codPedido;
        logDataModel.documentId = event.persona["documentId"];
        logDataModel.userType = _pre.userType;
        logDataModel.referenceCode = "";
        logDataModel.responseMessage = '';
        logDataModel.user = _pre.user;
        logDataModel.courierCode = _pre.courierCode;

        //enviar al log
        final apiDataLog = ApiLogDataReniec();
        //llamar a la api de log
        apiDataLog.enviarDataLogReniec(logDataModel);

        //retornar al state
        yield this.state.copyWith(
              consultamejorHuellaModel: consultaHuellaModel,
            );
      } catch (e) {
        print("error $e");
      }
    }
  }
}
