part of 'bloc_consulta_datos_reniec_bloc.dart';

@immutable
abstract class BlocConsultaDatosReniecEvent {}

class ConsultarDatosPersonales extends BlocConsultaDatosReniecEvent {
  final String dni;

  ConsultarDatosPersonales(this.dni);
}
