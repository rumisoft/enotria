import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_consultaDatosReniec.dart';
import 'package:enotria/model/consulta_reniec/consultaDatosPersonalesReniecModel.dart';
import 'package:meta/meta.dart';
part 'bloc_consulta_datos_reniec_event.dart';
part 'bloc_consulta_datos_reniec_state.dart';

class BlocConsultaDatosReniecBloc
    extends Bloc<BlocConsultaDatosReniecEvent, BlocConsultaDatosReniecState> {
  BlocConsultaDatosReniecBloc() : super(BlocConsultaDatosReniecInitial());

  @override
  Stream<BlocConsultaDatosReniecState> mapEventToState(
      BlocConsultaDatosReniecEvent event) async* {
    if (event is ConsultarDatosPersonales) {
      final apiconsultaDatos = ApiConsultaDatosReniec();

      try {
        //llamar a la api de log
        final res =
            await apiconsultaDatos.getConsultaDatosPersonalesReniec(
              event.dni
              //"72676295",
        );

        //instanciar el objeto de model
        final consultaDatosPersonalesModel =
            ConsultaDatosPersonalesReniecModel();
        //llenar el objeto con la data del api
        consultaDatosPersonalesModel.coResultado = res["coResultado"];
        consultaDatosPersonalesModel.deResultado = res["deResultado"];
        consultaDatosPersonalesModel.dni = res["dni"];
        consultaDatosPersonalesModel.prenombres = res["prenombres"];
        consultaDatosPersonalesModel.primerApellido = res["primerApellido"];
        consultaDatosPersonalesModel.segundoApellido = res["segundoApellido"];
        consultaDatosPersonalesModel.apellidoCasada = res["apellidoCasada"];
        consultaDatosPersonalesModel.restriccion = res["restriccion"];
        consultaDatosPersonalesModel.fechaNacimiento = res["fechaNacimiento"];
        consultaDatosPersonalesModel.genero = res["genero"];
        consultaDatosPersonalesModel.estadoCivil = res["estadoCivil"];

        //generar el pdf con los datos recibidos


        //----llamar a api de firma de pdf


        //enviar a s3

        //retornar al state
        yield BlocConsultaDatosReniecInitial(
            consultaDatosReniecModel: consultaDatosPersonalesModel);

      
      } catch (e) {
        print("error $e");
      }
    }
  }
}
