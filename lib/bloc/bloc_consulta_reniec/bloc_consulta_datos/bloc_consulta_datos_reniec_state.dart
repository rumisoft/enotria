part of 'bloc_consulta_datos_reniec_bloc.dart';

@immutable
abstract class BlocConsultaDatosReniecState {
  final  ConsultaDatosPersonalesReniecModel consultaDatosReniecModel;
  

  BlocConsultaDatosReniecState({this.consultaDatosReniecModel});
}

class BlocConsultaDatosReniecInitial extends BlocConsultaDatosReniecState {
  BlocConsultaDatosReniecInitial({ConsultaDatosPersonalesReniecModel consultaDatosReniecModel}) : super(consultaDatosReniecModel: consultaDatosReniecModel);


}
