part of 'bloc_biometria_reniec_bloc.dart';

@immutable
abstract class BlocBiometriaReniecEvent {}

class VerificarIdentidadWsq extends BlocBiometriaReniecEvent {
  final Map<String, dynamic> selectPersonMap;
  final String codPedido;
  final huella;
  // final String dni;
  // final nombre;
  // final apellido;
  // final codPed;

  VerificarIdentidadWsq(this.selectPersonMap,this.codPedido,this.huella);
}
