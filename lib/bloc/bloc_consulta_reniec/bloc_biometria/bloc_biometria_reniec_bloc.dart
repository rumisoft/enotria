import 'package:bloc/bloc.dart';
import 'package:enotria/api/api_consultaDatosReniec.dart';
import 'package:enotria/api/api_logDataReniec.dart';
import 'package:enotria/model/consulta_reniec/consultaBiometriaReniecModel.dart';
import 'package:enotria/model/consulta_reniec/consultaDatosPersonalesReniecModel.dart';
import 'package:enotria/model/log_data_reniec/logBiometriaModel.dart';
import 'package:enotria/model/log_data_reniec/logConsultaDatosModel.dart';
import 'package:enotria/util/crearPdf.dart';
import 'package:enotria/util/formato_fecha.dart' as utils;
import 'package:enotria/util/userPreferences.dart';
import 'package:meta/meta.dart';

part 'bloc_biometria_reniec_event.dart';
part 'bloc_biometria_reniec_state.dart';

class BlocBiometriaReniecBloc
    extends Bloc<BlocBiometriaReniecEvent, BlocBiometriaReniecState> {
  BlocBiometriaReniecBloc() : super(BlocBiometriaReniecState());

  @override
  Stream<BlocBiometriaReniecState> mapEventToState(
      BlocBiometriaReniecEvent event) async* {
    final apiconsultaDatos = ApiConsultaDatosReniec();
    final logDataModel = LogBioModel();

    UserPreferences _pre = UserPreferences();

    if (event is VerificarIdentidadWsq) {
      try {
        final apiDataLog = ApiLogDataReniec();
        List<ConsultaDatosPersonalesReniecModel> listDatosPersonales = [];
        //lista
        List<ConsultaBiometriaReniecModel> listBioWsqModel = [];
        String namePdf = "";

        //obtener la fecha actual
        DateTime horaConsulta = DateTime.now();
        //llamar a la api de vereific biometria WSQ
        final res = await apiconsultaDatos.getBiometriaWsqReniec(
            event.selectPersonMap["documentId"],
            //"72498553",
           //"72676295",
             event.huella, event.huella);

        //agregar a la lista el response de la api
        listBioWsqModel.add(ConsultaBiometriaReniecModel.fromJson(res));

        yield this.state.copyWith(consultaBiometriaModel: listBioWsqModel[0]);

        //-------------crear pdf si el resulado es HIT-----------------
        if (listBioWsqModel[0].coResultado == "70006" /*HIT*/) {
          //llamar al api de consulta de datos de RENIEC
          final res = await apiconsultaDatos.getConsultaDatosPersonalesReniec(
             //"72676295"
              event.selectPersonMap["documentId"]
              );
          listDatosPersonales
              .add(ConsultaDatosPersonalesReniecModel.fromJson(res));

          namePdf = await _crearCargo(
              "HIT", event, horaConsulta, listDatosPersonales, apiDataLog);

          //enviar Log de consulta
          await _logConsultaDatos(logDataModel, event, horaConsulta,
              listDatosPersonales, _pre, apiDataLog);
        } else {
          final listDatosP = ConsultaDatosPersonalesReniecModel();

          print("lista $listDatosPersonales");
          listDatosP.coResultado = "70007";
          //listDatosP.deResultado = "NO HIT";
          listDatosP.dni = event.selectPersonMap["documentId"];
          listDatosP.prenombres = event.selectPersonMap["fullName"];
          listDatosP.primerApellido = "-";
          listDatosP.segundoApellido = "-";
          listDatosP.apellidoCasada = "-";
          listDatosP.restriccion = "-";
          listDatosP.fechaNacimiento = "-";
          listDatosP.genero = "-";
          listDatosP.estadoCivil = "-";
          listDatosPersonales.add(listDatosP);

          namePdf = await _crearCargo(
              "NO HIT", event, horaConsulta, listDatosPersonales, apiDataLog);
        }

        //enviar al log de biometria
        _logDataBiometria(logDataModel, event, horaConsulta, listBioWsqModel[0],
            _pre, apiDataLog, namePdf);
      } catch (e) {
        print("error $e");
      }
    }
  }

  Future<String> _crearCargo(
      String resultado,
      VerificarIdentidadWsq event,
      DateTime horaConsulta,
      List<ConsultaDatosPersonalesReniecModel> listDatosPersonales,
      ApiLogDataReniec apiDataLog) async {
    var hora = await utils.obtenerFechaHora(horaConsulta);

    //generar pdf
    final pdf64 =
        await CreatePDf().generarPdf1(resultado, listDatosPersonales[0], hora);

    // ----llamar a api de firma de pdf y envio a s3
    var firma = await apiDataLog.firmarPdfReniec(
        event.codPedido, event.selectPersonMap["documentId"], pdf64);
    print("firma--$firma");
    String nameCargo = firma["data"]["fileName"];
    return nameCargo;
  }

  Future<void> _logConsultaDatos(
      LogBioModel logDataModel,
      VerificarIdentidadWsq event,
      DateTime horaConsulta,
      List<ConsultaDatosPersonalesReniecModel> listDatosPersonales,
      UserPreferences _pre,
      ApiLogDataReniec apiDataLog) async {
    final logDatosPersonalesModel = ConsultaDatosReniecModel();

    logDataModel.reniecFullName = event.selectPersonMap["fullName"];
    logDatosPersonalesModel.type = "***Consulta de Datos RUIPN";
    logDatosPersonalesModel.firstName = event.selectPersonMap["fullName"];
    logDatosPersonalesModel.lastName = event.selectPersonMap["fullName"];
    logDatosPersonalesModel.mobilePhoneNumber =
        event.selectPersonMap["phoneNumber"];
    logDatosPersonalesModel.reniecRequestHour = horaConsulta.toString();
    logDatosPersonalesModel.logDescription = listDatosPersonales[0].deResultado;
    logDatosPersonalesModel.logDate = horaConsulta.toString();
    logDatosPersonalesModel.documentType =
        event.selectPersonMap["documentType"];
    logDatosPersonalesModel.transactionStartDate = horaConsulta.toString();
    logDatosPersonalesModel.requestMessage = "";
    // consultaBiometriaModel.descripResultado
    logDatosPersonalesModel.reniecRequestId =
        listDatosPersonales[0].coResultado;
    logDatosPersonalesModel.generatedChargeDate = horaConsulta.toString();
    logDatosPersonalesModel.movilMacId = _pre.mac;
    logDatosPersonalesModel.reniecFullName = event.selectPersonMap["fullName"];
    logDatosPersonalesModel.methodType = "***Consulta de Datos RUIPN";
    logDatosPersonalesModel.transactionEndDate = horaConsulta.toString();
    logDatosPersonalesModel.reniecResponse = listDatosPersonales[0].deResultado;
    logDatosPersonalesModel.reniecRequestDate = horaConsulta.toString();
    logDatosPersonalesModel.requestedOrderId = event.codPedido;
    logDatosPersonalesModel.documentId = event.selectPersonMap["documentId"];
    logDatosPersonalesModel.userType = _pre.userType;
    logDatosPersonalesModel.referenceCode = "";
    logDatosPersonalesModel.responseMessage =
        listDatosPersonales[0].deResultado;
    logDatosPersonalesModel.user = _pre.user;
    logDatosPersonalesModel.courierCode = _pre.courierCode;

    //enviar la consulta de datos al log
    await apiDataLog.enviarDataLogReniec(logDatosPersonalesModel);
  }

  void _logDataBiometria(
      LogBioModel logDataModel,
      VerificarIdentidadWsq event,
      DateTime horaConsulta,
      ConsultaBiometriaReniecModel listBioWsqModel,
      UserPreferences _pre,
      ApiLogDataReniec apiDataLog,
      String namePdf) {
    logDataModel.type = "***Verificar Identidad Based wsq";
    logDataModel.firstName = event.selectPersonMap["fullName"];
    logDataModel.lastName = event.selectPersonMap["fullName"];
    logDataModel.mobilePhoneNumber = event.selectPersonMap["phoneNumber"];
    logDataModel.reniecRequestHour = horaConsulta.toString();
    //consultaBioModel.tiempoRespuesta;
    logDataModel.logDescription =
        listBioWsqModel.error == null && listBioWsqModel.nuDni == null
            ? listBioWsqModel.deResultado
            : listBioWsqModel.error != null
                ? "Error al realizar la solicitud a reniec.gob.pe"
                : listBioWsqModel.deResultado;
    logDataModel.logDate = horaConsulta.toString();
    logDataModel.documentType = event.selectPersonMap["documentType"];
    logDataModel.transactionStartDate = horaConsulta.toString();
    logDataModel.requestMessage = listBioWsqModel.deResultado;
    logDataModel.reniecRequestId = listBioWsqModel.coResultado;
    logDataModel.generatedChargeDate = horaConsulta.toString();
    logDataModel.movilMacId = _pre.mac;
    logDataModel.reniecFullName = event.selectPersonMap["fullName"];
    logDataModel.methodType = "***Verificar Identidad Based wsq";
    logDataModel.transactionEndDate =
        //   consultaBioModel.tiempoRespuesta;
        horaConsulta.toString();
    logDataModel.reniecResponse = listBioWsqModel.deResultado;
    logDataModel.reniecRequestDate = "";
    //consultaBiometriaModel.tiempoRespuesta
    logDataModel.requestedOrderId = event.codPedido;
    logDataModel.documentId = event.selectPersonMap["documentId"];
    logDataModel.userType = _pre.userType;
    logDataModel.referenceCode = "";
    logDataModel.responseMessage = listBioWsqModel.deResultado;
    logDataModel.user = _pre.user;
    logDataModel.courierCode = _pre.courierCode;
    logDataModel.cargoPathReniec = namePdf;

    //llamar a la api de log de biometria
    apiDataLog.enviarDataLogReniec(logDataModel);
  }
}
