part of 'bloc_biometria_reniec_bloc.dart';

class BlocBiometriaReniecState {
  final ConsultaBiometriaReniecModel consultaBiometriaModel;
   final bool proccessin;
  
  BlocBiometriaReniecState( {this.consultaBiometriaModel,this.proccessin});

   BlocBiometriaReniecState copyWith({
    ConsultaBiometriaReniecModel consultaBiometriaModel,
    bool proccessin
  }) {
    return BlocBiometriaReniecState(
      consultaBiometriaModel:
          consultaBiometriaModel ?? this.consultaBiometriaModel,
          proccessin: proccessin?? this.proccessin
    );
  }
}


// class BlocBiometriaReniecInitial extends BlocBiometriaReniecState {
//   BlocBiometriaReniecInitial(
//       {ConsultaBiometriaReniecModel consultaBiometriaModel})
//       : super(consultaBiometriaModel);
// }
