part of 'bloc_dni_consult_bloc.dart';

class BlocDniConsultState {
  final Datum data;
  final bool load;
  final List<Map<String, dynamic>> deliveryPerson;
  final List<Map<String, dynamic>> deliveryAddress;
  String selectAddress;
  String selectPerson;
  Map<String, dynamic> selectAddressMap;
  Map<String, dynamic> selectPersonMap;
  final List<String> cargo;
  final List<String> evidencia;
  String document;
  int timeGlobal;

  BlocDniConsultState({
    this.data,
    this.load,
    this.deliveryPerson,
    this.deliveryAddress,
    this.selectAddress,
    this.selectPerson,
    this.selectAddressMap,
    this.selectPersonMap,
    this.document,
    this.timeGlobal,
    this.cargo,
    this.evidencia,
  });

  static BlocDniConsultState get initialState => BlocDniConsultState(
        load: false,
        deliveryAddress: [],
        deliveryPerson: [],
        selectPerson: "Seleccionar",
        selectAddress: "Seleccionar",
        document: " ",
        timeGlobal: 0,
        cargo: [],
        evidencia: [],
      );

  BlocDniConsultState copyWith({
    Datum data,
    bool load,
    List<Map<String, dynamic>> deliveryPerson,
    List<Map<String, dynamic>> deliveryAddress,
    String selectAddress,
    String selectPerson,
    Map<String, dynamic> selectAddressMap,
    Map<String, dynamic> selectPersonMap,
    List<String> cargo,
    List<String> evidencia,
    String document,
    int timeGlobal,
  }) {
    return BlocDniConsultState(
      data: data ?? this.data,
      load: load ?? this.load,
      deliveryAddress: deliveryAddress ?? this.deliveryAddress,
      deliveryPerson: deliveryPerson ?? this.deliveryPerson,
      selectAddress: selectAddress ?? this.selectAddress,
      selectPerson: selectPerson ?? this.selectPerson,
      selectAddressMap: selectAddressMap ?? this.selectAddressMap,
      selectPersonMap: selectPersonMap ?? this.selectPersonMap,
      document: document ?? this.document,
      timeGlobal: timeGlobal ?? this.timeGlobal,
      cargo: cargo ?? this.cargo,
      evidencia: evidencia ?? this.evidencia,
    );
  }
}
