import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:enotria/model/modelDni.dart';
import 'package:meta/meta.dart';

part 'bloc_dni_consult_event.dart';
part 'bloc_dni_consult_state.dart';

class BlocDniConsultBloc
    extends Bloc<BlocDniConsultEvent, BlocDniConsultState> {
  BlocDniConsultBloc() : super(BlocDniConsultState.initialState);




  @override
  Stream<BlocDniConsultState> mapEventToState(
    BlocDniConsultEvent event,
  ) async* {
    if (event is InitData) {
      List<Map<String, dynamic>> _deliveryPerson = [];
      List<Map<String, dynamic>> _deliveryAddress = [];
      int _personCount = 0, _addressCount = 0;

      event.data.deliveryPerson.forEach((element) {
        Map<String, dynamic> _mapGeneric = {
          "phoneNumber": element.phoneNumber,
          "annexed": element.annexed,
          "personId": element.personId,
          "username": element.username,
          "phoneNumber2": element.phoneNumber2,
          "fullName": element.fullName,
          "documentType": element.documentType,
          "documentId": element.documentId,
          "email": element.email
        };
        _deliveryPerson.add(_mapGeneric);
        _personCount++;
      });

      event.data.deliveryAddress.forEach((element) {
        Map<String, dynamic> _mapGeneric = {
          "departmentName": element.departmentName,
          "provinceName": element.provinceName,
          "districtName": element.districtName,
          "address": element.address
        };
        _deliveryAddress.add(_mapGeneric);
        _addressCount++;
      });



      yield this.state.copyWith(
            data: event.data,
            deliveryPerson: _deliveryPerson,
            deliveryAddress: _deliveryAddress,
            document: (_personCount >= 2) ? null : _deliveryPerson[0]["documentId"],
            selectPerson:(_personCount >= 2) ? "Seleccionar" : _deliveryPerson[0]["fullName"],
            selectPersonMap: (_personCount >= 2) ? null : _deliveryPerson[0],
            selectAddress: (_addressCount >= 2)? "Seleccionar": _deliveryAddress[0]["address"],
            selectAddressMap: (_addressCount >= 2) ? null : _deliveryAddress[0],
            cargo: ["x","x","x","x"],
            evidencia: ["x","x","x"],
            load: true,
          );
    }
  }
}
