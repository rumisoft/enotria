import 'package:flutter/services.dart';

class TestChannel {
  TestChannel._() {
    _platform.setMethodCallHandler(_fromNative);
  }

  static const _platform = const MethodChannel("com.enotriasa/biometria_platform_channel");

  static TestChannel _instance;

  static get instance => _instance ??= TestChannel._();

  Future<bool> callTest() async {
    bool res = false;

    try {
      res = await _platform.invokeMethod('myMethod') == 1;
    } on PlatformException catch (e) {
      print('Error = $e');
    }

    print('Is call successful? $res');
    return res;
  }

  Future<void> _fromNative(MethodCall call) async {
    if (call.method == 'abrirScaner') {
      print('callTest result = ${call.arguments}');
    }
  }
}
