import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:enotria/util/formato_fecha.dart' as utils;

class HuelleroBiometrico {
  final _bioMethodChannel =
      MethodChannel("com.enotriasa/biometria_platform_channel");

  HuelleroBiometrico() {
    _bioMethodChannel.setMethodCallHandler(_fromNative);
  }

  Future<dynamic> checkPermission() async {
    final result = await _bioMethodChannel.invokeMethod<String>("check");
    return result;
  }

  Future<void> _fromNative(MethodCall call) async {
    if (call.method == 'allowDeviceOne') {
      print('callTest result = ${call.arguments}');
      await Future.delayed(Duration(seconds: 5), () {});

      var bytehuella = await stopScan();
      print(bytehuella);
    }
  }

  //funcion que abre el scanner
  Future<dynamic> openScan() async {
    final data = await _bioMethodChannel.invokeMethod("abrirScaner");

    // print("resultado---$data");

    return data;
  }

  //funcion que devuelve la version de android
  Future<String> version() async {
    final result = await _bioMethodChannel.invokeMethod("version");

    if (result == null) {
      print("no se implementó---");
      return null;
    } else {
      print("version $version-----");
      return result;
    }
    //  print(result);
  }

  //funcion que cierra el scanner
  Future<dynamic> stopScan() async {
    final data = await _bioMethodChannel.invokeMethod("cerrarScaner");

    print("resultado cerrando scaner---$data");

    return data;
  }
}

class UtilsBiometria {
  Future<int> verificarHuella(huellader) async {
    var compareList;
    List noImage = [
      255,
      160,
      255,
      168,
      0,
      121,
      78,
      73,
      83,
      84,
      95,
      67,
      79,
      77,
      32,
      57,
      10,
      80,
      73,
      88,
      95,
      87,
      73,
      68,
      84,
      72,
      32,
      53,
      49,
      50,
      10,
      80,
      73,
      88,
      95,
      72,
      69,
      73,
      71,
      72,
      84,
      32,
      53,
      49,
      50,
      10,
      80,
      73,
      88,
      95,
      68,
      69,
      80,
      84,
      72,
      32,
      56,
      10,
      80,
      80,
      73,
      32,
      45,
      49,
      10,
      76,
      79,
      83,
      83,
      89,
      32,
      49,
      10,
      67,
      79,
      76,
      79,
      82,
      83,
      80,
      65,
      67,
      69,
      32,
      71,
      82,
      65,
      89,
      10,
      67,
      79,
      77,
      80,
      82,
      69,
      83,
      83,
      73,
      79,
      78,
      32,
      87,
      83,
      81,
      10,
      87,
      83,
      81,
      95,
      66,
      73,
      84,
      82,
      65,
      84,
      69,
      32,
      48,
      46,
      55,
      53,
      48,
      48,
      48,
      48,
      255,
      164,
      0,
      58,
      9,
      7,
      0,
      9,
      50,
      211,
      37,
      205,
      0,
      10,
      224,
      243,
      25,
      154,
      1,
      10,
      65,
      239,
      241,
      154,
      1,
      11,
      142,
      39,
      100,
      205,
      0,
      11,
      225,
      121,
      163,
      51,
      0,
      9,
      46,
      255,
      86,
      0,
      1,
      10,
      249,
      51,
      211,
      51,
      1,
      11,
      242,
      135,
      33,
      154,
      0,
      10,
      38,
      119,
      218,
      51,
      255,
      165,
      1,
      133,
      2,
      0,
      44,
      0
    ];

    int enable = 0;

    try {
      if (huellader.length > 64) {
        //tomar los primeros 64 elementos del array
        var topList = huellader.take(193);

        //convertir topList a lista con 192 elementos
        var lista1 = topList.toList();
        //comparar ambas listas: true o false
        compareList = _listEquals(lista1, noImage);

        if (compareList == true) {
          // print("No hay imagen de huella");
          return 0;
        } else {
          //imagen de la huella
          //print("Biometría: $huellader");
          enable = 1;
          return enable;

          //await _saveHuellaFile(huellaDer);

        }
      } else {
        //print("Huella está null o nay un dispositivo conectado");
        return 2;
      }
    } catch (e) {
      print("error--- $e");
      return 3;
    }
  }

  bool _listEquals(a, b) {
    if (a == null) return b == null;
    if (b == null || a.length != b.length) return false;
    if (identical(a, b)) return true;
    for (int index = 0; index < a.length; index += 1) {
      if (a[index] != b[index]) return false;
    }
    return true;
  }

  Future<void> saveHuellaFile(huellaDer) async {
    // UserPreferences prefs = new UserPreferences();
    Uint8List archivoHuella = huellaDer;
    //directorio en donde guardará
    final directorio = await getTemporaryDirectory();
    print("directorio---- $directorio");

    //hora actual
    DateTime horaActual = DateTime.now();
    //formato de hora
    final date = utils.obtenerFechaHoraDate(horaActual);
    //nombre del archivo
    String nameFile = "72676295" + date + '.wsq';

    File newFile = await File('${directorio.path}/$nameFile').create();

    //final resizeHuella= await  resizeImage(archivoHuella);

    newFile.writeAsBytesSync(archivoHuella);
    print("peso: ${newFile.lengthSync()}");

//Tu archivo Uint8List ya es un File
  }
}

class MyFirstPlatformChannel {
  final _methodChannel2 =
      MethodChannel("com.enotriasa/my_fisrt_platform_channel");

//funcion que devuelve la version de android
  Future<String> version() async {
    final result = await _methodChannel2.invokeMethod("version");
    print(result);
    return result;
  }
}
