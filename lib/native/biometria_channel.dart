import 'package:flutter/services.dart';

enum PermissionStatus {
  unknown,
  granted,
  denied,
  restricted,
}

class BiometriaChannel {
  BiometriaChannel._internal();
  static BiometriaChannel _instance = BiometriaChannel._internal();
  static BiometriaChannel get instance => _instance;
  PermissionStatus _status = PermissionStatus.unknown;

  final _channel = MethodChannel("com.enotriasa/biometria_platform_channel");
  

  Future<PermissionStatus> checkPermission() async {
    final String result = await this._channel.invokeMethod<String>('check');
    return this._getStatus(result);
  }

  Future<PermissionStatus> requestPermission() async {
    final String result = await _channel.invokeMethod<String>("request");
    return this._getStatus(result);
  }

  Future<void> start() async {
    await _channel.invokeMethod("start");
  }

  Future<void> stop() async {
    await _channel.invokeMethod("stop");
  }

  PermissionStatus _getStatus(String result) {
    switch (result) {
      case "granted":
        this._status = PermissionStatus.granted;
        break;
      case "denied":
        this._status = PermissionStatus.denied;
        break;
      case "restricted":
        this._status = PermissionStatus.restricted;
        break;
      default:
        this._status = PermissionStatus.unknown;
    }
    return this._status;
  }
}
