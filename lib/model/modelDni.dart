// To parse this JSON data, do
//
//     final modelDni = modelDniFromJson(jsonString);

import 'dart:convert';

ModelDni modelDniFromJson(String str) => ModelDni.fromJson(json.decode(str));

String modelDniToJson(ModelDni data) => json.encode(data.toJson());

class ModelDni {
    ModelDni({
        this.status,
        this.data,
    });

    bool status;
    List<Datum> data;

    factory ModelDni.fromJson(Map<String, dynamic> json) => ModelDni(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        this.deliveryCode,
        this.enterprise,
        this.product,
        this.deliveryPerson,
        this.deliveryAddress,
        this.deliveryVisits,
        this.state,
        this.substate,
        this.updateImg,
        this.imagesCargo,
        this.imagesSupport,
    });

    String deliveryCode;
    Enterprise enterprise;
    Product product;
    List<DeliveryPerson> deliveryPerson;
    List<DeliveryAddress> deliveryAddress;
    DeliveryVisits deliveryVisits;
    StateP state;
    StateP substate;
    int updateImg;
    List<dynamic> imagesCargo;
    List<dynamic> imagesSupport;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        deliveryCode: json["deliveryCode"],
        enterprise: Enterprise.fromJson(json["enterprise"]),
        product: Product.fromJson(json["product"]),
        deliveryPerson: List<DeliveryPerson>.from(json["deliveryPerson"].map((x) => DeliveryPerson.fromJson(x))),
        deliveryAddress: List<DeliveryAddress>.from(json["deliveryAddress"].map((x) => DeliveryAddress.fromJson(x))),
        deliveryVisits: DeliveryVisits.fromJson(json["deliveryVisits"]),
        state: StateP.fromJson(json["state"]),
        substate: StateP.fromJson(json["substate"]),
        updateImg: json["updateImg"],
        imagesCargo: List<dynamic>.from(json["imagesCargo"].map((x) => x)),
        imagesSupport: List<dynamic>.from(json["imagesSupport"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "deliveryCode": deliveryCode,
        "enterprise": enterprise.toJson(),
        "product": product.toJson(),
        "deliveryPerson": List<dynamic>.from(deliveryPerson.map((x) => x.toJson())),
        "deliveryAddress": List<dynamic>.from(deliveryAddress.map((x) => x.toJson())),
        "deliveryVisits": deliveryVisits.toJson(),
        "state": state.toJson(),
        "substate": substate.toJson(),
        "updateImg": updateImg,
        "imagesCargo": List<dynamic>.from(imagesCargo.map((x) => x)),
        "imagesSupport": List<dynamic>.from(imagesSupport.map((x) => x)),
    };
}

class DeliveryAddress {
    DeliveryAddress({
        this.departmentName,
        this.provinceName,
        this.districtName,
        this.address,
    });

    String departmentName;
    String provinceName;
    String districtName;
    String address;

    factory DeliveryAddress.fromJson(Map<String, dynamic> json) => DeliveryAddress(
        departmentName: json["departmentName"],
        provinceName: json["provinceName"],
        districtName: json["districtName"],
        address: json["address"],
    );

    Map<String, dynamic> toJson() => {
        "departmentName": departmentName,
        "provinceName": provinceName,
        "districtName": districtName,
        "address": address,
    };
}

class DeliveryPerson {
    DeliveryPerson({
        this.phoneNumber,
        this.annexed,
        this.personId,
        this.username,
        this.phoneNumber2,
        this.fullName,
        this.documentType,
        this.documentId,
        this.email,
    });

    String phoneNumber;
    String annexed;
    String personId;
    String username;
    String phoneNumber2;
    String fullName;
    String documentType;
    String documentId;
    String email;

    factory DeliveryPerson.fromJson(Map<String, dynamic> json) => DeliveryPerson(
        phoneNumber: json["phoneNumber"],
        annexed: json["annexed"],
        personId: json["personId"],
        username: json["username"],
        phoneNumber2: json["phoneNumber2"],
        fullName: json["fullName"],
        documentType: json["documentType"],
        documentId: json["documentId"],
        email: json["email"],
    );

    Map<String, dynamic> toJson() => {
        "phoneNumber": phoneNumber,
        "annexed": annexed,
        "personId": personId,
        "username": username,
        "phoneNumber2": phoneNumber2,
        "fullName": fullName,
        "documentType": documentType,
        "documentId": documentId,
        "email": email,
    };
}

class DeliveryVisits {
    DeliveryVisits();

    factory DeliveryVisits.fromJson(Map<String, dynamic> json) => DeliveryVisits(
    );

    Map<String, dynamic> toJson() => {
    };
}

class Enterprise {
    Enterprise({
        this.enterpriseCode,
        this.enterpriseName,
    });

    String enterpriseCode;
    String enterpriseName;

    factory Enterprise.fromJson(Map<String, dynamic> json) => Enterprise(
        enterpriseCode: json["enterpriseCode"],
        enterpriseName: json["enterpriseName"],
    );

    Map<String, dynamic> toJson() => {
        "enterpriseCode": enterpriseCode,
        "enterpriseName": enterpriseName,
    };
}

class Product {
    Product({
        this.productCode,
        this.productName,
    });

    String productCode;
    String productName;

    factory Product.fromJson(Map<String, dynamic> json) => Product(
        productCode: json["productCode"],
        productName: json["productName"],
    );

    Map<String, dynamic> toJson() => {
        "productCode": productCode,
        "productName": productName,
    };
}

class StateP {
    StateP({
        this.code,
        this.description,
    });

    String code;
    String description;

    factory StateP.fromJson(Map<String, dynamic> json) => StateP(
        code: json["code"],
        description: json["description"],
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "description": description,
    };
}






































// // To parse this JSON data, do
// //
// //     final modelDni = modelDniFromJson(jsonString);

// import 'dart:convert';

// ModelDni modelDniFromJson(String str) => ModelDni.fromJson(json.decode(str));

// String modelDniToJson(ModelDni data) => json.encode(data.toJson());

// class ModelDni {
//     ModelDni({
//         this.status,
//         this.data,
//     });

//     bool status;
//     List<Datum> data;

//     factory ModelDni.fromJson(Map<String, dynamic> json) => ModelDni(
//         status: json["status"],
//         data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
//     );

//     Map<String, dynamic> toJson() => {
//         "status": status,
//         "data": List<dynamic>.from(data.map((x) => x.toJson())),
//     };
// }

// class Datum {
//     Datum({
//         this.deliveryCode,
//         this.enterprise,
//         this.product,
//         this.deliveryPerson,
//         this.deliveryAddress,
//         this.deliveryVisits,
//     });

//     String deliveryCode;
//     Enterprise enterprise;
//     Product product;
//     List<DeliveryPerson> deliveryPerson;
//     List<DeliveryAddress> deliveryAddress;
//     DeliveryVisits deliveryVisits;

//     factory Datum.fromJson(Map<String, dynamic> json) => Datum(
//         deliveryCode: json["deliveryCode"],
//         enterprise: Enterprise.fromJson(json["enterprise"]),
//         product: Product.fromJson(json["product"]),
//         deliveryPerson: List<DeliveryPerson>.from(json["deliveryPerson"].map((x) => DeliveryPerson.fromJson(x))),
//         deliveryAddress: List<DeliveryAddress>.from(json["deliveryAddress"].map((x) => DeliveryAddress.fromJson(x))),
//         deliveryVisits: DeliveryVisits.fromJson(json["deliveryVisits"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "deliveryCode": deliveryCode,
//         "enterprise": enterprise.toJson(),
//         "product": product.toJson(),
//         "deliveryPerson": List<dynamic>.from(deliveryPerson.map((x) => x.toJson())),
//         "deliveryAddress": List<dynamic>.from(deliveryAddress.map((x) => x.toJson())),
//         "deliveryVisits": deliveryVisits.toJson(),
//     };
// }

// class DeliveryAddress {
//     DeliveryAddress({
//         this.departmentName,
//         this.provinceName,
//         this.districtName,
//         this.address,
//     });

//     String departmentName;
//     String provinceName;
//     String districtName;
//     String address;

//     factory DeliveryAddress.fromJson(Map<String, dynamic> json) => DeliveryAddress(
//         departmentName: json["departmentName"],
//         provinceName: json["provinceName"],
//         districtName: json["districtName"],
//         address: json["address"],
//     );

//     Map<String, dynamic> toJson() => {
//         "departmentName": departmentName,
//         "provinceName": provinceName,
//         "districtName": districtName,
//         "address": address,
//     };
// }

// class DeliveryPerson {
//     DeliveryPerson({
//         this.phoneNumber,
//         this.annexed,
//         this.personId,
//         this.username,
//         this.phoneNumber2,
//         this.fullName,
//         this.documentType,
//         this.documentId,
//         this.email,
//     });

//     String phoneNumber;
//     String annexed;
//     String personId;
//     String username;
//     String phoneNumber2;
//     String fullName;
//     String documentType;
//     String documentId;
//     String email;

//     factory DeliveryPerson.fromJson(Map<String, dynamic> json) => DeliveryPerson(
//         phoneNumber: json["phoneNumber"],
//         annexed: json["annexed"],
//         personId: json["personId"],
//         username: json["username"],
//         phoneNumber2: json["phoneNumber2"],
//         fullName: json["fullName"],
//         documentType: json["documentType"],
//         documentId: json["documentId"],
//         email: json["email"],
//     );

//     Map<String, dynamic> toJson() => {
//         "phoneNumber": phoneNumber,
//         "annexed": annexed,
//         "personId": personId,
//         "username": username,
//         "phoneNumber2": phoneNumber2,
//         "fullName": fullName,
//         "documentType": documentType,
//         "documentId": documentId,
//         "email": email,
//     };
// }

// class DeliveryVisits {
//     DeliveryVisits();

//     factory DeliveryVisits.fromJson(Map<String, dynamic> json) => DeliveryVisits(
//     );

//     Map<String, dynamic> toJson() => {
//     };
// }

// class Enterprise {
//     Enterprise({
//         this.enterpriseCode,
//         this.enterpriseName,
//     });

//     String enterpriseCode;
//     String enterpriseName;

//     factory Enterprise.fromJson(Map<String, dynamic> json) => Enterprise(
//         enterpriseCode: json["enterpriseCode"],
//         enterpriseName: json["enterpriseName"],
//     );

//     Map<String, dynamic> toJson() => {
//         "enterpriseCode": enterpriseCode,
//         "enterpriseName": enterpriseName,
//     };
// }

// class Product {
//     Product({
//         this.productCode,
//         this.productName,
//     });

//     String productCode;
//     String productName;

//     factory Product.fromJson(Map<String, dynamic> json) => Product(
//         productCode: json["productCode"],
//         productName: json["productName"],
//     );

//     Map<String, dynamic> toJson() => {
//         "productCode": productCode,
//         "productName": productName,
//     };
// }
