class ConsultaDatosReniecModel {
  ConsultaDatosReniecModel({
    this.type,
    this.datetime,
    this.lastName,
    this.mobilePhoneNumber,
    this.reniecRequestHour,
    this.logDescription,
    this.logDate,
    this.documentType,
    this.transactionStartDate,
    this.requestMessage,
    this.firstName,
    this.reniecRequestId,
    this.generatedChargeDate,
    this.movilMacId,
    this.reniecFullName,
    this.methodType,
    this.transactionEndDate,
    this.reniecResponse,
    this.reniecRequestDate,
    this.requestedOrderId,
    this.documentId,
    this.userType,
    this.referenceCode,
    this.responseMessage,
    this.user,
    this.courierCode,
  });

  String type;
  String datetime;
  String lastName;
  String mobilePhoneNumber;
  String reniecRequestHour;
  String logDescription;
  String logDate;
  String documentType;
  String transactionStartDate;
  String requestMessage;
  String firstName;
  String reniecRequestId;
  String generatedChargeDate;
  String movilMacId;
  String reniecFullName;
  String methodType;
  String transactionEndDate;
  String reniecResponse;
  String reniecRequestDate;
  String requestedOrderId;
  String documentId;
  int userType;
  String referenceCode;
  String responseMessage;
  String user;
  String courierCode;

  factory ConsultaDatosReniecModel.fromJson(Map<String, dynamic> json) =>
      ConsultaDatosReniecModel(
        type: json["type"],
        datetime: json["datetime"],
        lastName: json["lastName"],
        mobilePhoneNumber: json["mobilePhoneNumber"],
        reniecRequestHour: json["reniecRequestHour"],
        logDescription: json["logDescription"],
        logDate: json["logDate"],
        documentType: json["documentType"],
        transactionStartDate: json["transactionStartDate"],
        requestMessage: json["requestMessage"],
        firstName: json["firstName"],
        reniecRequestId: json["reniecRequestId"],
        generatedChargeDate: json["generatedChargeDate"],
        movilMacId: json["movilMacId"],
        reniecFullName: json["reniecFullName"],
        methodType: json["methodType"],
        transactionEndDate: json["transactionEndDate"],
        reniecResponse: json["reniecResponse"],
        reniecRequestDate: json["reniecRequestDate"],
        requestedOrderId: json["requestedOrderId"],
        documentId: json["documentId"],
        userType: json["userType"],
        referenceCode: json["referenceCode"],
        responseMessage: json["responseMessage"],
        user: json["user"],
        courierCode: json["courierCode"],
      );

  Map<String, dynamic> toJson() => {
        "lastName": lastName,
        "mobilePhoneNumber": mobilePhoneNumber,
        "reniecRequestHour": reniecRequestHour,
        "logDescription": logDescription,
        "logDate": logDate,
        "documentType": documentType,
        "transactionStartDate": transactionStartDate,
        "requestMessage": requestMessage,
        "firstName": firstName,
        "reniecRequestId": reniecRequestId,
        "generatedChargeDate": generatedChargeDate,
        "movilMacId": movilMacId,
        "reniecFullName": reniecFullName,
        "methodType": methodType,
        "transactionEndDate": transactionEndDate,
        "reniecResponse": reniecResponse,
        "reniecRequestDate": reniecRequestDate,
        "requestedOrderId": requestedOrderId,
        "documentId": documentId,
        "userType": userType,
        "referenceCode": referenceCode,
        "responseMessage": responseMessage,
        "user": user,
        "courierCode": courierCode,
      };
}
