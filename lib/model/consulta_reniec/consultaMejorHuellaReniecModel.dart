class ConsultaMejorHuellaReniecModel {
  ConsultaMejorHuellaReniecModel(
      {this.coResponse,
      this.msgResponse,
      this.coHuellaDer,
      this.descHuellaDer,
      this.coHuellaIzq,
      this.descHuellaIzq,
      this.error
      });

  String coResponse;
  String msgResponse;
  String coHuellaDer;
  String descHuellaDer;
  String coHuellaIzq;
  String descHuellaIzq;
  String error;
}
