class ConsultaBiometriaReniecModel{
  ConsultaBiometriaReniecModel({
    this.coResultado,
    this.deResultado,
    this.nuDni,
    this.coRestriccion,
    this.tiempoRespuesta,
    this.error,
    this.stackTrace,
  });

  String coResultado;
  String deResultado;
  dynamic nuDni;
  dynamic coRestriccion;
  dynamic tiempoRespuesta;
  dynamic error;
  dynamic stackTrace;

  factory ConsultaBiometriaReniecModel.fromJson(Map<String, dynamic> json) => ConsultaBiometriaReniecModel(
        coResultado: json["coResultado"],
        deResultado: json["deResultado"],
        nuDni: json["nuDni"],
        coRestriccion: json["coRestriccion"],
        tiempoRespuesta: json["tiempoRespuesta"],
        error: json["error"],
        stackTrace: json["stackTrace"],
      );
}

//modelo de prueba huellero
class IdentidadWsqModel {
  IdentidadWsqModel(
      {this.codResultado ,
      this.descripResultado,
      this.dni,
      this.coRestriccion,
      this.tiempoRespuesta});

  String codResultado ;
  String descripResultado ;
  String dni;
  String coRestriccion;
  String tiempoRespuesta;
}
