class ConsultaDatosPersonalesReniecModel {
  ConsultaDatosPersonalesReniecModel({
    this.coResultado,
    this.deResultado,
    this.dni,
    this.prenombres,
    this.primerApellido,
    this.segundoApellido,
    this.apellidoCasada,
    this.restriccion,
    this.fechaNacimiento,
    this.genero,
    this.estadoCivil,
    this.error,
    this.stackTrace,
  });

  String coResultado;
  String deResultado;
  String dni;
  String prenombres;
  String primerApellido;
  String segundoApellido;
  String apellidoCasada;
  String restriccion;
  String fechaNacimiento;
  String genero;
  String estadoCivil;
  dynamic error;
  dynamic stackTrace;

  factory ConsultaDatosPersonalesReniecModel.fromJson(
          Map<String, dynamic> json) =>
      ConsultaDatosPersonalesReniecModel(
        coResultado: json["coResultado"],
        deResultado: json["deResultado"],
        dni: json["dni"],
        prenombres: json["prenombres"],
        primerApellido: json["primerApellido"],
        segundoApellido: json["segundoApellido"],
        apellidoCasada: json["apellidoCasada"],
        restriccion: json["restriccion"],
        fechaNacimiento: json["fechaNacimiento"],
        genero: json["genero"],
        estadoCivil: json["estadoCivil"],
        error: json["error"],
        stackTrace: json["stackTrace"],
      );
}
