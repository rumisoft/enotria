// To parse this JSON data, do
//
//     final modelUser = modelUserFromJson(jsonString);

import 'dart:convert';

ModelUser modelUserFromJson(String str) => ModelUser.fromJson(json.decode(str));

String modelUserToJson(ModelUser data) => json.encode(data.toJson());

class ModelUser {
    ModelUser({
        this.user,
        this.token,
        this.lastName,
        this.firstName,
        this.userType,
        this.courierCode,
    });

    dynamic user;
    dynamic token;
    dynamic lastName;
    dynamic firstName;
    dynamic userType;
    dynamic courierCode;

    factory ModelUser.fromJson(Map<String, dynamic> json) => ModelUser(
        user: json["user"],
        token: json["token"],
        lastName: json["lastName"],
        firstName: json["firstName"],
        userType: json["userType"],
        courierCode: json["courierCode"],
    );

    Map<String, dynamic> toJson() => {
        "user": user,
        "token": token,
        "lastName": lastName,
        "firstName": firstName,
        "userType": userType,
        "courierCode": courierCode,
    };
}
