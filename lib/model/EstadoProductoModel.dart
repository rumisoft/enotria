class EstadoPedidoModel {
  EstadoPedidoModel({
    this.estados,
    this.bio,
  });

  List<Estado> estados;
  ValidacionBioModel bio;

  factory EstadoPedidoModel.fromJson(Map<String, dynamic> json) =>
      EstadoPedidoModel(
        estados:
            List<Estado>.from(json["estados"].map((x) => Estado.fromJson(x))),
        bio: ValidacionBioModel.fromJson(json["bio"]),
      );

  Map<String, dynamic> toJson() => {
        "estados": List<dynamic>.from(estados.map((x) => x.toJson())),
        "bio": bio.toJson(),
      };
}

class ValidacionBioModel {
  ValidacionBioModel({
    this.tipoBiometria,
    this.numeroIntentos,
    this.saltoBiometria,
    this.cantRestante,
  });

  String tipoBiometria;
  int numeroIntentos;
  bool saltoBiometria;
  int cantRestante;

  factory ValidacionBioModel.fromJson(Map<String, dynamic> json) =>
      ValidacionBioModel(
        tipoBiometria: json["tipo_biometria"],
        numeroIntentos: json["numero_intentos"],
        saltoBiometria: json["salto_biometria"],
      );

  Map<String, dynamic> toJson() => {
        "tipo_biometria": tipoBiometria,
        "numero_intentos": numeroIntentos,
        "salto_biometria": saltoBiometria,
      };
}

class Estado {
  Estado({
    this.valores,
    this.id,
    this.descripcion,
    this.abreviatura,
  });

  List<Valore> valores;
  String id;
  String descripcion;
  String abreviatura;

  factory Estado.fromJson(Map<String, dynamic> json) => Estado(
        valores:
            List<Valore>.from(json["valores"].map((x) => Valore.fromJson(x))),
        id: json["_id"],
        descripcion: json["descripcion"],
        abreviatura: json["abreviatura"],
      );

  Map<String, dynamic> toJson() => {
        "valores": List<dynamic>.from(valores.map((x) => x.toJson())),
        "_id": id,
        "descripcion": descripcion,
        "abreviatura": abreviatura,
      };
}

class Valore {
  Valore({
    this.id,
    this.abreviatura,
    this.descripcion,
  });

  String id;
  String abreviatura;
  String descripcion;

  factory Valore.fromJson(Map<String, dynamic> json) => Valore(
        id: json["id"],
        abreviatura: json["abreviatura"],
        descripcion: json["descripcion"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "abreviatura": abreviatura,
        "descripcion": descripcion,
      };
}
