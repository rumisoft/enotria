import 'package:enotria/database/data_base_provider.dart';
import 'package:enotria/model/log_data_reniec/logBiometriaModel.dart';



class DataReniecDataBase {
  final dbprovider = DatabaseProvider.db;
  Future<void> insertarPendientes(LogBioModel dataReniecModel) async {
    // Obtiene una referencia de la base de datos

    try {
      final db = await dbprovider.database;
      await db.insert(
        'DataReniecLog',
        dataReniecModel.toJson(),
        //conflictAlgorithm: ConflictAlgorithm.replace,
      );
    } catch (exception) {
      print(exception);
    }
  }

  Future<List<LogBioModel>> obtenerData() async {
    final db = await dbprovider.database;

    // Consulta la tabla por toda la DataReniecLog.
    final List<Map<String, dynamic>> res = await db.query('DataReniecLog');

    // Convierte List<Map<String, dynamic> en List<LogReniecModel>.
    List<LogBioModel> list = res.isNotEmpty
        ? res.map((c) => LogBioModel.fromJson(c)).toList()
        : [];
    return list;
  }

  Future<int> updateData(LogBioModel dataReniecModel) async {
    final db = await dbprovider.database;

    // Actualiza
    final res = await db.update(
      'DataReniecLog',
      dataReniecModel.toJson(),
      // Aseguúrate de que solo actualizarás el Dog con el id coincidente
      where: "movilMacId = ?",
      // Pasa el id Dog a través de whereArg para prevenir SQL injection
      whereArgs: [dataReniecModel.movilMacId],
    );
    return res;
  }

  Future<void> deleteData(int id) async {
    final db = await dbprovider.database;

    // Elimina el Dog de la base de datos
    await db.delete(
      'DataReniecLog',
      // Utiliza la cláusula `where` para eliminar un dog específico
      where: "id = ?",
      // Pasa el id Dog a través de whereArg para prevenir SQL injection
      whereArgs: [id],
    );
  }
}
