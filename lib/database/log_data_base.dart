import 'package:enotria/database/data_base_provider.dart';

class LogDataBase {
  final dbprovider = DatabaseProvider.db;
  Future<void> insertarDataLog(String idpedido, String fullName, String latitud,
      String longitud, String _datetime) async {
    // Obtiene una referencia de la base de datos

    try {
      final db = await dbprovider.database;
      await db.transaction((txn) async {
        await txn.rawInsert(
            "INSERT INTO DataLog(usuario,pedido,cliente ,direccion,"
            "horaI,cargo1,cargo2,cargo3,cargo4,evide1,evide2,"
            "evide3,detalle,horaF,total) "
            "VALUES('usuario','$idpedido', "
            "'$fullName','$latitud - $longitud','$_datetime', "
            "'cargo1','cargo2','cargo3','cargo4','evide1','evide2','evide3', "
            "'detalle','horaF','total'"
            ")");
      });

     
    } catch (exception) {
      print(exception);
    }
  }

  Future<int> updateData(String mode, int id) async {
    final db = await dbprovider.database;

    // Actualizar
    final res = await db
        .rawUpdate('UPDATE DataLog SET cargo1 = ? WHERE id = ?', [mode, id]);
    return res;
  }

  Future<int> updateDataCargo2(String mode2, int id) async {
    final db = await dbprovider.database;

    // Actualizar
    final res = await db
        .rawUpdate('UPDATE DataLog SET cargo2 = ? WHERE id = ?', [mode2, id]);
    return res;
  }

  Future<int> updateDataCargo3(String mode3, int id) async {
    final db = await dbprovider.database;

    // Actualizar
    final res = await db
        .rawUpdate('UPDATE DataLog SET cargo3 = ? WHERE id = ?', [mode3, id]);
    return res;
  }

  Future<int> updateDataCargo4(String mode4, int id) async {
    final db = await dbprovider.database;

    // Actualizar
    final res = await db
        .rawUpdate('UPDATE DataLog SET cargo4 = ? WHERE id = ?', [mode4, id]);
    return res;
  }

  Future<int> updateDataEvidencia(String mode5, int id) async {
    final db = await dbprovider.database;

    // Actualizar
    final res = await db
        .rawUpdate('UPDATE DataLog SET evide1 = ? WHERE id = ?', [mode5, id]);
    return res;
  }

  Future<int> updateDataEvidencia2(String mode6, int id) async {
    final db = await dbprovider.database;

    // Actualizar
    final res = await db
        .rawUpdate('UPDATE DataLog SET evide2 = ? WHERE id = ?', [mode6, id]);
    return res;
  }

  Future<int> updateDataEvidencia3(String mode7, int id) async {
    final db = await dbprovider.database;

    // Actualizar
    final res = await db
        .rawUpdate('UPDATE DataLog SET evide3 = ? WHERE id = ?', [mode7, id]);
    return res;
  }

//-------------direccion-------------

  Future<int> updateDireccion(
      double latitude,
      double longitude,
      String dateC16,
      String _datetime,
      String datetime,
      int hours,
      int minutes,
      int seconds,
      int id) async {
    final db = await dbprovider.database;

    // Actualizar
    final res = await db.rawUpdate(
        'UPDATE DataLog SET direccion = ?,detalle = ?,horaF = ?, total=? WHERE id = ?',
        [
          "$latitude $longitude",
          "$dateC16 $_datetime",
          datetime,
          "$hours:$minutes:$seconds",
          id
        ]);
    return res;
  }

  Future<void> deleteData(int id) async {
    final db = await dbprovider.database;

    // Elimina el Dog de la base de datos
    await db.delete(
      'DataReniecLog',
      // Utiliza la cláusula `where` para eliminar un dog específico
      where: "id = ?",
      // Pasa el id Dog a través de whereArg para prevenir SQL injection
      whereArgs: [id],
    );
  }
}
