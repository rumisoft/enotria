import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseProvider {
  static Database _database;
  static final DatabaseProvider db = DatabaseProvider._();

  DatabaseProvider._();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();

    return _database;
  }

  initDB() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'EnotriaLOG.db');
    return await openDatabase(
      path, version: 1,
      // Cuando la base de datos se crea por primera vez, crea una tabla para almacenar dogs
      onCreate: (db, version) async {
        await db.execute(
            'CREATE TABLE DataLog (id INTEGER PRIMARY KEY,usuario TEXT, pedido TEXT,cliente TEXT,direccion TEXT, horaI TEXT, cargo1 TEXT, cargo2 TEXT, cargo3 TEXT, cargo4 TEXT, evide1 TEXT,evide2 TEXT,evide3 TEXT, detalle TEXT,horaF TEXT, total TEXT)');
        await db.execute(
          'CREATE TABLE DataReniecLog (id INTEGER PRIMARY KEY AUTOINCREMENT,'
            'type TEXT,datetime TEXT,firstName TEXT,lastName TEXT,'
            'mobilePhoneNumber TEXT, reniecRequestHour TEXT,'
            'logDescription TEXT, logDate TEXT, documentType TEXT,'
            'transactionStartDate TEXT,requestMessage TEXT,'
            'reniecRequestId TEXT,generatedChargeDate TEXT,'
            'movilMacId TEXT,reniecFullName TEXT, methodType TEXT,'
            'transactionEndDate TEXT,reniecResponse TEXT,'
            'reniecRequestDate TEXT, requestedOrderId TEXT,'
            'documentId TEXT,userType TEXT,referenceCode TEXT,'
            'responseMessage TEXT,user TEXT, courierCode TEXT )');
      },
    );
  }
}
