import 'package:enotria/bloc/bloc_update_password/bloc_opdate_password_bloc.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/constast.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/RoundedPassword.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yaml/yaml.dart';

class UpdatePassword extends StatefulWidget {
  UpdatePassword({Key key}) : super(key: key);

  @override
  _UpdatePasswordState createState() => _UpdatePasswordState();
}

class _UpdatePasswordState extends State<UpdatePassword> {
  final BlocUpdatePasswordBloc _bloc = BlocUpdatePasswordBloc();
  final GlobalKey<FormState> _formKey = GlobalKey();
  String _passwordCurrent;
  String _passwordNew1;
  String _passwordNew2;
  String _version;
  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: UtilsColors.primary,
          centerTitle: true,
          title: Text(
            "SEGURIDAD ",
            style: TextStyle(color: UtilsColors.white),
          ),
        ),
        //drawer: DrawerUser(),
        backgroundColor: UtilsColors.white,
        body: BlocBuilder<BlocUpdatePasswordBloc, BlocUpdatePasswordState>(
            builder: (_, state) {
          return Stack(
            children: <Widget>[
              SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Container(
                          child: Text(""),
                        ),
                      ),
                      (state.updateStatus)
                          ? Container(
                              //color: Colors.red,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(height: 20),
                                  Text(
                                    "Cambio de Contraseña ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15,
                                        fontFamily: "Cocon",
                                        letterSpacing: 1),
                                    textAlign: TextAlign.center,
                                  ),
                                  Container(
                                    child: Column(
                                      children: [
                                        SizedBox(height: 100),
                                        Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 20),
                                          child: SvgPicture.asset(
                                            "assets/svg/check.svg",
                                            height: 60,
                                            color: UtilsColors.success,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 20),
                                          child: Text(
                                              "¡Se realizo el cambio de contraseña!",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14,
                                                  color: UtilsColors.success)),
                                        ),
                                        SizedBox(
                                          height: 30,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Redireccionando al login en",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14,
                                            color: UtilsColors.cris1),
                                      ),
                                      Text(
                                        " ${state.timerEnd}",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                            color: UtilsColors.success),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            )
                          : Container(
                              child: Column(
                                children: [
                                  SizedBox(height: 20),
                                  Text(
                                    "Cambio de Contraseña",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15,
                                        fontFamily: "Cocon",
                                        letterSpacing: 1),
                                    textAlign: TextAlign.center,
                                  ),
                                  Container(
                                    child: Column(
                                      children: [
                                        SizedBox(height: 20),
                                        Form(
                                          key: _formKey,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            children: [
                                              RoundedPassword(
                                                hintText:
                                                    "Ingrese Contraseña Actual",
                                                icon: Icons.lock,
                                                sufixicon: true,
                                                validator: (value) {
                                                  this._passwordCurrent = value;
                                                  return value.length != 0
                                                      ? null
                                                      : 'Ingrese Contraseña Actual';
                                                },
                                              ),
                                              RoundedPassword(
                                                hintText:
                                                    "Ingrese Contraseña Nueva",
                                                icon: Icons.lock,
                                                sufixicon: true,
                                                validator: (value) {
                                                  this._passwordNew1 = value;
                                                  return value.length != 0
                                                      ? null
                                                      : 'Ingrese Contraseña Nueva';
                                                },
                                              ),
                                              RoundedPassword(
                                                hintText:
                                                    "Confirmar Contraseña Nueva",
                                                icon: Icons.lock,
                                                sufixicon: true,
                                                validator: (value) {
                                                  this._passwordNew2 = value;
                                                  return value.length != 0
                                                      ? null
                                                      : 'Confirmar Contraseña Nueva';
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          width: double.infinity,
                                          margin: EdgeInsets.symmetric(
                                              vertical: 0, horizontal: 20),
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 20, vertical: 5),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Caracteristicas de la contraseña:",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 13),
                                                textAlign: TextAlign.start,
                                              ),
                                              Text("Como mínimo 8 caracteres."),
                                              Text("Como mínimo 1 número."),
                                              Text(
                                                  "Como mínimo 1 carácter especial."),
                                              Text(
                                                  "Como mínimo 1 letra mayúscula."),
                                            ],
                                          ),
                                        ),
                                        SizedBox(height: 20),
                                        Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 20),
                                          child: RoundedButton(
                                            color: UtilsColors.secundary,
                                            text: "GUARDAR",
                                            press: () {
                                              if (_formKey.currentState
                                                  .validate()) {
                                                _bloc.add(NewPassword(
                                                    oldPasswd: _passwordCurrent,
                                                    password1: _passwordNew1,
                                                    password2: _passwordNew2,
                                                    context: context));
                                              }
                                            },
                                            loading: state.status ?? false,
                                          ),
                                        ),
                                        SizedBox(height: 5),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                    ],
                  ),
                ),
              ),
              Container(
                child: Text(
                  "Enotria S.A. Ⓒ2021  v${appversion.toString()}",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 13, color: UtilsColors.tersary),
                ),
              )
              // Align(
              //   alignment: Alignment.bottomCenter,
              //   child: FutureBuilder(
              //     future: rootBundle.loadString("pubspec.yaml"),
              //     builder: (context, snapshot) {
              //       if (snapshot.hasData) {
              //         var yaml = loadYaml(snapshot.data);

              //         _version = yaml["appversion"];
              //       }

              //       return Container(
              //         child: Text(
              //           "Enotria S.A. Ⓒ2021  v${_version.toString()}",
              //           textAlign: TextAlign.center,
              //           style:
              //               TextStyle(fontSize: 13, color: UtilsColors.tersary),
              //         ),
              //       );
              //     },
              //   ),

              //   // Your fixed Footer here,
              // ),
            ],
          );
        }),
      ),
    );
  }
}
