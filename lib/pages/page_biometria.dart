import 'package:enotria/bloc/bloc_resumen/resumen_bloc.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PageBiometria extends StatefulWidget {
  final String type;
  final String productName;
  final String productCode;
  final String enterpriseName;
  final String deliveryCode;
  final String code;
  final Map<String, dynamic> selectAddressMap;
  final Map<String, dynamic> selectPersonMap;
  final List<String> cargo;
  final List<String> evidencia;
  final Map estados;
  final Map subestados;
  PageBiometria(
      {Key key,
      this.type = "REGISTRO",
      this.productName,
      this.productCode,
      this.enterpriseName,
      this.deliveryCode,
      this.code,
      this.selectAddressMap,
      this.selectPersonMap,
      this.cargo,
      this.evidencia,
      this.estados,
      this.subestados})
      : super(key: key);

  @override
  _PageBiometriaState createState() => _PageBiometriaState();
}

class _PageBiometriaState extends State<PageBiometria>
    with WidgetsBindingObserver {
  GlobalKey<FormState> _formKey = GlobalKey();

  ResumenBloc _bloc = new ResumenBloc();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final MediaQueryData media = MediaQuery.of(context);
    final Size size = media.size;
    // ignore: unused_local_variable
    final EdgeInsets padding = media.padding;

    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        backgroundColor: UtilsColors.white,
        appBar: AppBar(
            elevation: 0,
            backgroundColor: UtilsColors.primary,
            centerTitle: true,
            title: Text(
              "BIOMETRIA",
              style: TextStyle(color: UtilsColors.white, fontSize: 15),
            ),
            automaticallyImplyLeading: false),
        body: BlocBuilder<ResumenBloc, ResumenState>(builder: (_, state) {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: 1100,
                  child: Column(
                    children: [
                      SizedBox(height: 1),
                      Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 5),
                        padding: EdgeInsets.symmetric(
                          horizontal: 5,
                          vertical: 5,
                        ),
                        width: size.width * 11,
                        decoration: BoxDecoration(
                          color: UtilsColors.cris0,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(
                          children: [
                            Form(
                                key: _formKey,
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          width: 80,
                                          child: Text(
                                            "Nro. Pedido ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(0),
                                          child: Text(
                                            ': FGHFGHFGHFGH',
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: 12,
                                              color: UtilsColors.cris1,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          width: 80,
                                          child: Text(
                                            "Dni ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(0),
                                          child: Text(
                                            ':FGHFGHFGH',
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: 12,
                                              color: UtilsColors.cris1,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          width: 80,
                                          child: Text(
                                            "Nombres ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                        ),
                                        Flexible(
                                          child: Container(
                                            padding: EdgeInsets.all(0),
                                            child: Text(
                                              ': FGHFGHFGHFG',
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontSize: 12,
                                                color: UtilsColors.cris1,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          width: 80,
                                          child: Text(
                                            "Fecha/Hora ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(0),
                                          child: Text(
                                            ':FGHFGHFGHFGH',
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: 12,
                                              color: UtilsColors.cris1,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ))
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      RoundedButton(
                        color: UtilsColors.secundary,
                        text: "FINALIZAR",
                        press: () {},
                        loading: false,
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 50,
                  padding: new EdgeInsets.only(top: 0.0, left: 10),
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Enotria S.A. Ⓒ2021",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 13, color: UtilsColors.tersary),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        }),
      ),
    );
  }
}
