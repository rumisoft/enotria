import 'package:enotria/bloc/bloc_dni_consult/bloc_dni_consult_bloc.dart';
import 'package:enotria/model/EstadoProductoModel.dart';
import 'package:enotria/pages/page_resumen.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/responsive.dart';
import 'package:enotria/widget/all/AlertDialog.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/RoundedInput.dart';
import 'package:enotria/widget/all/menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

class DatosNoHitPage extends StatefulWidget {
  DatosNoHitPage({
    this.valbio,
    this.productName,
    this.productCode,
    this.enterpriseName,
    this.deliveryCode,
    this.code,
    this.selectAddressMap,
    this.selectPersonMap,
    this.cargo,
    this.evidencia,
    this.estados,
    this.subestados,
  });

  final ValidacionBioModel valbio;
  final String productName;
  final String productCode;
  final String enterpriseName;
  final String deliveryCode;
  final String code;
  final Map<String, dynamic> selectAddressMap;
  final Map<String, dynamic> selectPersonMap;

  final List<String> cargo;
  final List<String> evidencia;
  final Map estados;
  final Map subestados;

  @override
  State<DatosNoHitPage> createState() => _DatosNoHitPageState();
}

class _DatosNoHitPageState extends State<DatosNoHitPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  BlocDniConsultBloc _bloc = BlocDniConsultBloc();
  int numintentos;

  @override
  void initState() {
    super.initState();

    numintentos = widget.valbio.numeroIntentos - widget.valbio.cantRestante;
  }

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: UtilsColors.primary,
            centerTitle: true,
            title: Text(
              "Enotria",
              style: TextStyle(color: UtilsColors.white),
            ),
            leading: Theme(
              data: Theme.of(context).copyWith(
                canvasColor: UtilsColors.cris1,
              ),
              child: IconButton(
                icon: Icon(
                  Icons.more_vert,
                  size: 40,
                  color: Colors.white,
                ),
                onPressed: () => _scaffoldKey.currentState.openDrawer(),
              ),
            ),
          ),
          drawer: DrawerUser(),
          body: Container(
            // onWillPop: () {
            //   return onBackPressed(context);
            // },
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: responsive.hp(3)),

                  Container(
                      height: responsive.hp(5),
                      width: double.infinity,
                      color: UtilsColors.secundary,
                      child: Center(
                          child: Text("BIOMETRÍA",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: responsive.ip(2))))),
                  SizedBox(height: responsive.hp(4)),
                  Text("Datos del Cliente",
                      style: TextStyle(
                          color: UtilsColors.cris1,
                          fontWeight: FontWeight.bold,
                          fontSize: responsive.ip(2))),
                  SizedBox(height: responsive.hp(1)),

                  // SizedBox(height: responsive.hp(4)),

                  //imagenes
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: UtilsColors.danger,
                          style: BorderStyle.solid,
                          width: 1.0),
                      // borderRadius: BorderRadius.circular(25.0)
                    ),
                    margin: EdgeInsets.symmetric(
                        horizontal: responsive.ip(1),
                        vertical: responsive.ip(0.5)),
                    padding: EdgeInsetsDirectional.all(responsive.hp(1)),
                    height: responsive.hp(20),
                    width: responsive.wp(28),
                    child: Container(
                      height: responsive.hp(10),
                      width: responsive.wp(24),
                      child: SvgPicture.asset(
                        "assets/svg/huella.svg",
                        // color: ViteColors.primary,
                      ),
                    ),
                  ),
                  SizedBox(height: responsive.hp(3)),

                  Text(
                      "Huella de cliente no coincide con \n información de RENIEC",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: UtilsColors.danger,
                          //color: UtilsColors.success,
                          fontWeight: FontWeight.bold,
                          fontSize: responsive.ip(2))),
                  SizedBox(height: responsive.hp(3)),

                  BlocBuilder<BlocDniConsultBloc, BlocDniConsultState>(
                    builder: (context, state) {
                      var confirmBio = Column(children: [
                        Text(
                            "Error de biometría, le quedan $numintentos intentos",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                // color: Colors.green,
                                fontWeight: FontWeight.w500,
                                fontSize: responsive.ip(2))),
                        SizedBox(height: responsive.hp(5)),
                        Text("Volver a realizar la Biometría",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                // color: Colors.green,
                                fontWeight: FontWeight.bold,
                                fontSize: responsive.ip(2))),
                        SizedBox(height: responsive.hp(3)),
                        Container(
                          width: responsive.wp(45),
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          child: RoundedButton(
                            color: UtilsColors.primary,
                            text: "Biometria",
                            press: () {
                              //contador de intentos
                              widget.valbio.cantRestante++;
                              Navigator.pop(context);
                            },
                            loading: false,
                            icon: "assets/svg/huella.svg",
                          ),
                        ),
                        SizedBox(height: responsive.hp(5)),
                      ]);
                      var boton_sgt = Column(children: [
                        RoundedButton(
                          color: UtilsColors.primary,
                          text: "Siguiente",
                          press: () {
                            final route = MaterialPageRoute(
                                builder: (BuildContext _) => PageResumen(
                                      type: "REGISTRO",
                                      productName: widget.productName,
                                      productCode: widget.productCode,
                                      enterpriseName: widget.enterpriseName,
                                      deliveryCode: widget.deliveryCode,
                                      code: widget.deliveryCode,
                                      selectAddressMap: widget.selectAddressMap,
                                      selectPersonMap: widget.selectPersonMap,
                                      cargo: widget.cargo,
                                      evidencia: widget.evidencia,
                                      valbio: widget.valbio,
                                      estados: widget.estados,
                                      subestados: widget.subestados,
                                      op: "0",
                                    ));

                            Navigator.push(context, route);
                          },
                          loading: false,
                          //icon: Icons.arrow_right_alt,
                        )
                      ]);

                      return Column(
                          children: [numintentos > 0 ? confirmBio : boton_sgt]);
                    },
                  ),

                  SizedBox(height: responsive.hp(14)),

                  PiePagina()
                ],
              ),
            ),
          )),
    );
  }
}
