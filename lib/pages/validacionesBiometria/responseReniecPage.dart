import 'package:enotria/bloc/bloc_consulta_reniec/bloc_biometria/bloc_biometria_reniec_bloc.dart';
import 'package:enotria/model/EstadoProductoModel.dart';
import 'package:enotria/model/log_data_reniec/logBiometriaModel.dart';
import 'package:enotria/pages/page_resumen.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/responsive.dart';
import 'package:enotria/widget/all/AlertDialog.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/RoundedInput.dart';
import 'package:enotria/widget/all/menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

class DatosHitPage extends StatefulWidget {
  DatosHitPage({
    this.productName,
    this.productCode,
    this.enterpriseName,
    this.deliveryCode,
    this.code,
    this.selectAddressMap,
    this.selectPersonMap,
    this.cargo,
    this.evidencia,
    this.estados,
    this.subestados,
    this.valbio,
    this.apiType,
    this.horaConsulta,
    this.huellader,
  });

  final String productName;
  final String productCode;
  final String enterpriseName;
  final String deliveryCode;
  final String code;
  final Map<String, dynamic> selectAddressMap;
  final Map<String, dynamic> selectPersonMap;
  final List<String> cargo;
  final List<String> evidencia;
  final Map estados;
  final Map subestados;
  final ValidacionBioModel valbio;
  final String apiType;
  final dynamic huellader;
  final String horaConsulta;

  @override
  State<DatosHitPage> createState() => _DatosHitPageState();
}

class _DatosHitPageState extends State<DatosHitPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final logDataModel = LogBioModel();
  int numintentos;
  final biometriaBloc = BlocBiometriaReniecBloc();

  @override
  void initState() {
    numintentos = widget.valbio.numeroIntentos - widget.valbio.cantRestante;
    biometriaBloc.add(VerificarIdentidadWsq(
      widget.selectPersonMap,
      widget.deliveryCode,
      widget.huellader,
    ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return BlocProvider.value(
      value: biometriaBloc,
      child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: UtilsColors.primary,
            centerTitle: true,
            title: Text(
              "Enotria",
              style: TextStyle(color: UtilsColors.white),
            ),
            leading: Theme(
              data: Theme.of(context).copyWith(
                canvasColor: UtilsColors.cris1,
              ),
              child: IconButton(
                icon: Icon(
                  Icons.more_vert,
                  size: 40,
                  color: Colors.white,
                ),
                onPressed: () => _scaffoldKey.currentState.openDrawer(),
              ),
            ),
          ),
          drawer: DrawerUser(),
          body: Container(
            child: WillPopScope(
              onWillPop: () {
                return onBackPressed(context);
              },
              child: Container(
                //color: Colors.grey,
                height: responsive.hp(90),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(height: responsive.hp(1)),
                      Container(
                          height: responsive.hp(6),
                          width: double.infinity,
                          color: UtilsColors.secundary,
                          child: Center(
                              child: Text("BIOMETRÍA",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: responsive.ip(2))))),
                      SizedBox(height: responsive.hp(6)),
                      Text("Datos del Cliente",
                          style: TextStyle(
                              color: UtilsColors.cris1,
                              fontWeight: FontWeight.bold,
                              fontSize: responsive.ip(2))),

                      SizedBox(height: responsive.hp(2)),

                      //imagenes
                      BlocBuilder<BlocBiometriaReniecBloc,
                          BlocBiometriaReniecState>(builder: (context, state) {
                        return state.consultaBiometriaModel == null
                            ? Padding(
                                padding:
                                    EdgeInsets.only(left: responsive.wp(7)),
                                child: CupertinoActivityIndicator(),
                              )
                            : Column(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.green,
                                          style: BorderStyle.solid,
                                          width: 1.0),
                                      // borderRadius: BorderRadius.circular(25.0)
                                    ),
                                    margin: EdgeInsets.symmetric(
                                        horizontal: responsive.ip(1),
                                        vertical: responsive.ip(0.5)),
                                    padding: EdgeInsetsDirectional.all(
                                        responsive.hp(1)),
                                    height: responsive.hp(22),
                                    width: responsive.wp(30),
                                    child: Container(
                                        // height: responsive.hp(14),
                                        // width: responsive.wp(28),
                                        child: state.consultaBiometriaModel
                                                    .coResultado
                                                    .toString() ==
                                                "70006"
                                            ? SvgPicture.asset(
                                                "assets/svg/huella_ok.svg",
                                                // color: ViteColors.primary,
                                              )
                                            : SvgPicture.asset(
                                                "assets/svg/huella.svg",
                                                // color: ViteColors.primary,
                                              )),
                                  ),
                                  SizedBox(height: responsive.hp(3)),
                                  state.consultaBiometriaModel.error != null
                                      ? Text(
                                          "Error al realizar la solicitud a reniec.gob.pe",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: UtilsColors.danger,
                                              //color: UtilsColors.success,
                                              fontWeight: FontWeight.bold,
                                              fontSize: responsive.ip(2)))
                                      : state.consultaBiometriaModel.error ==
                                                  null &&
                                              state.consultaBiometriaModel
                                                      .nuDni ==
                                                  null
                                          ? Text(
                                              state.consultaBiometriaModel
                                                  .deResultado
                                                  .toString(),
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: responsive.ip(2)))
                                          : state.consultaBiometriaModel
                                                      .coResultado ==
                                                  "70006"
                                              ? Column(
                                                  children: [
                                                    Text(
                                                        "¡Biometría exitosa!", //state.rpta reniec
                                                        style: TextStyle(
                                                            color: Colors.green,
                                                            //color: UtilsColors.success,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: responsive
                                                                .ip(2))),
                                                    Text(
                                                        "Dni: ${state
                                                            .consultaBiometriaModel
                                                            .nuDni
                                                            .toString()}",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color: Colors.green,
                                                            //color: UtilsColors.success,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: responsive
                                                                .ip(2))),
                                                  ],
                                                )
                                              : Column(
                                                  children: [
                                                    Text(
                                                        "Huella de cliente no coincide con \n información de RENIEC",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color: UtilsColors
                                                                .danger,
                                                            //color: UtilsColors.success,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: responsive
                                                                .ip(2))),
                                                    Text(
                                                        "Cod. ${state
                                                            .consultaBiometriaModel
                                                            .coResultado
                                                            .toString()}",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(

                                                            //color: UtilsColors.success,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: responsive
                                                                .ip(2))),
                                                    Text(
                                                        state
                                                            .consultaBiometriaModel
                                                            .nuDni
                                                            .toString(),
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(

                                                            //color: UtilsColors.success,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: responsive
                                                                .ip(2))),
                                                  ],
                                                ),
                                  SizedBox(height: responsive.hp(7)),
                                  state.consultaBiometriaModel.error != null
                                      ? Container()
                                      : state.consultaBiometriaModel
                                                  .coResultado ==
                                              "70006"
                                          ? Text(
                                              "¡Proceso registrado de \n manera exitosa!",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.green,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: responsive.ip(2)))
                                          : Column(children: [
                                              Text(
                                                  "Error de biometría, le quedan $numintentos intento(s)",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      // color: Colors.green,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize:
                                                          responsive.ip(2))),
                                              SizedBox(
                                                  height: responsive.hp(5)),
                                              numintentos == 0
                                                  ? Container()
                                                  : Text(
                                                      "Volver a realizar la Biometría",
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          // color: Colors.green,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: responsive
                                                              .ip(2))),
                                              SizedBox(
                                                  height: responsive.hp(3)),
                                              numintentos > 0
                                                  ? Container(
                                                      width: responsive.wp(45),
                                                      margin:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 20),
                                                      child: RoundedButton(
                                                        color:
                                                            UtilsColors.primary,
                                                        text: "Biometria",
                                                        press: () {
                                                          //contador de intentos
                                                          widget.valbio
                                                              .cantRestante++;
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        loading: false,
                                                        icon:
                                                            "assets/svg/huella.svg",
                                                      ),
                                                    )
                                                  : Container(),
                                              SizedBox(
                                                  height: responsive.hp(5)),
                                            ]),
                                  SizedBox(height: responsive.hp(5)),
                                  state.consultaBiometriaModel.coResultado ==
                                              "70006" ||
                                          numintentos == 0
                                      ? RoundedButton(
                                          color: UtilsColors.primary,
                                          text: "Siguiente",
                                          press: () {
                                            final route = MaterialPageRoute(
                                                builder: (BuildContext _) =>
                                                    PageResumen(
                                                      type: "REGISTRO",
                                                      productName:
                                                          widget.productName,
                                                      productCode:
                                                          widget.productCode,
                                                      enterpriseName:
                                                          widget.enterpriseName,
                                                      deliveryCode:
                                                          widget.deliveryCode,
                                                      code: widget.deliveryCode,
                                                      selectAddressMap: widget
                                                          .selectAddressMap,
                                                      selectPersonMap: widget
                                                          .selectPersonMap,
                                                      cargo: widget.cargo,
                                                      evidencia:
                                                          widget.evidencia,
                                                      valbio: widget.valbio,
                                                      op: state.consultaBiometriaModel
                                                                  .coResultado ==
                                                              "70006"
                                                          ? "1"
                                                          : "0",
                                                      estados: widget.estados,
                                                      subestados:
                                                          widget.subestados,
                                                    ));

                                            Navigator.push(context, route);
                                          },
                                          loading: false,
                                          //icon: Icons.arrow_right_alt,
                                        )
                                      : Container()
                                ],
                              );
                      }),

                      SizedBox(height: responsive.hp(10)),

                      PiePagina()
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
