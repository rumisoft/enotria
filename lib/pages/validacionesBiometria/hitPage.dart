import 'package:enotria/bloc/bloc_consulta_reniec/bloc_mejor_huella/bloc_mejor_huella_bloc.dart';
import 'package:enotria/model/EstadoProductoModel.dart';
import 'package:enotria/native/my_first_platform_channel.dart';
import 'package:enotria/pages/validacionesBiometria/responseReniecPage.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/responsive.dart';
import 'package:enotria/util/utils.dart' as utils;
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/RoundedInput.dart';
import 'package:enotria/widget/all/menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_restart/flutter_restart.dart';
import 'package:flutter_svg/svg.dart';

class HitBiometriaPage extends StatefulWidget {
  HitBiometriaPage({
    //this.type = "REGISTRO",
    this.valbio,
    this.productName,
    this.productCode,
    this.enterpriseName,
    this.deliveryCode,
    this.code,
    this.selectAddressMap,
    this.selectPersonMap,
    this.cargo,
    this.evidencia,
    this.estados,
    this.subestados,
    this.apiType,
    this.horaConsultaApi,
  });

  //final String type;
  final ValidacionBioModel valbio;
  final String productName;
  final String productCode;
  final String enterpriseName;
  final String deliveryCode;
  final String code;
  final Map<String, dynamic> selectAddressMap;
  final Map<String, dynamic> selectPersonMap;

  final List<String> cargo;
  final List<String> evidencia;
  final Map estados;
  final Map subestados;
  final String apiType;
  final String horaConsultaApi;

  @override
  State<HitBiometriaPage> createState() => _HitBiometriaPageState();
}

class _HitBiometriaPageState extends State<HitBiometriaPage> {
  final mejorHuellaBloc = BlocMejorHuellaBloc();
  final _cargando = ValueNotifier<bool>(false);

  String type = "";
  final resHuella = "qwerty";
  var _bioMethodChannel =
      MethodChannel("com.enotriasa/biometria_platform_channel");

  @override
  void initState() {
    _bioMethodChannel.setMethodCallHandler(callBackNative);

    //consultar res de mejor huella
    mejorHuellaBloc.add(
        ObtenerMejorHuellaEvent(widget.selectPersonMap, widget.deliveryCode));
    super.initState();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return BlocProvider.value(
        value: mejorHuellaBloc,
        child: Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              elevation: 0,
              backgroundColor: UtilsColors.primary,
              centerTitle: true,
              title: Text(
                "Enotria",
                style: TextStyle(color: UtilsColors.white),
              ),
              leading: Theme(
                data: Theme.of(context).copyWith(
                  canvasColor: UtilsColors.cris1,
                ),
                child: IconButton(
                  icon: Icon(
                    Icons.more_vert,
                    size: 40,
                    color: Colors.white,
                  ),
                  onPressed: () => _scaffoldKey.currentState.openDrawer(),
                ),
              ),
            ),
            drawer: DrawerUser(),
            body: SingleChildScrollView(
              child: WillPopScope(
                onWillPop: _onBackPressed,
                child: Column(
                  children: [
                    SizedBox(height: responsive.hp(1)),

                    Container(
                        height: responsive.hp(6),
                        width: double.infinity,
                        color: UtilsColors.secundary,
                        child: Center(
                            child: Text("BIOMETRÍA",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: responsive.ip(2))))),
                    SizedBox(height: responsive.hp(5)),
                    Text("Datos del Cliente",
                        style: TextStyle(
                            color: UtilsColors.cris1,
                            fontWeight: FontWeight.bold,
                            fontSize: responsive.ip(2))),
                    // SizedBox(height: responsive.hp(1)),

                    ContainerWidget(
                      height: 15,
                      width: 80,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 7),
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                  "DNI: ${widget.selectPersonMap["documentId"]}",
                                  style: TextStyle(
                                      // color: UtilsColors.cris1,
                                      fontWeight: FontWeight.bold,
                                      fontSize: responsive.ip(1.7))),
                              Text(
                                  "Nombre: ${widget.selectPersonMap["fullName"]}",
                                  style: TextStyle(
                                      // color: UtilsColors.cris1,
                                      fontWeight: FontWeight.bold,
                                      fontSize: responsive.ip(1.7))),
                              Container(
                                width: responsive.wp(75),
                                child: Row(
                                  children: [
                                    Text("Mejor Huella: ",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: responsive.ip(1.7))),
                                    //dibujar con los datos del bloc de consulta
                                    BlocBuilder<BlocMejorHuellaBloc,
                                        BlocMejorHuellaState>(
                                      builder: (context, state) {
                                        return state.consultamejorHuellaModel ==
                                                null
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    left: responsive.wp(7)),
                                                child:
                                                    CupertinoActivityIndicator(),
                                              )
                                            : Expanded(
                                                child: state.consultamejorHuellaModel
                                                            .msgResponse
                                                            .toString() ==
                                                        "OK"
                                                    ? Text(state.consultamejorHuellaModel.descHuellaDer,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: responsive
                                                                .ip(1.7)))
                                                    : state.consultamejorHuellaModel.error ==
                                                            null
                                                        ? Text(
                                                            state
                                                                .consultamejorHuellaModel
                                                                .msgResponse
                                                                .toString(),
                                                            style: TextStyle(
                                                                fontWeight: FontWeight.bold,
                                                                fontSize: responsive.ip(1.7)))
                                                        : Text(
                                                            "Error al realizar la solicitud a https://devsvb.reniec.gob.pe",
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize:
                                                                  responsive
                                                                      .ip(1.7),
                                                            ),
                                                          ),
                                              );
                                      },
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: responsive.hp(3)),

                    //imagenes
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black,
                            style: BorderStyle.solid,
                            width: 1.0),
                        // borderRadius: BorderRadius.circular(25.0)
                      ),
                      margin: EdgeInsets.symmetric(
                          horizontal: responsive.ip(1),
                          vertical: responsive.ip(0.5)),
                      padding: EdgeInsetsDirectional.all(responsive.hp(1)),
                      height: responsive.hp(24),
                      width: responsive.wp(28),
                      child: Container(
                        height: responsive.hp(9),
                        width: responsive.wp(16),
                        child: SvgPicture.asset(
                          "assets/svg/huella.svg", color: UtilsColors.cris1,
                          // color: ViteColors.primary,
                        ),
                      ),
                    ),
                    SizedBox(height: responsive.hp(4)),

                    Column(
                      children: [
                        ValueListenableBuilder(
                          valueListenable: _cargando,
                          builder:
                              (BuildContext context, bool value, Widget child) {
                            return Stack(
                              children: [
                                Center(
                                  child: RoundedButton(
                                    color: _cargando.value == false
                                        ? UtilsColors.secundary
                                        : Colors.orange[200],
                                    text: "Capturar Biometría",
                                    press: _cargando.value == true
                                        ? () {}
                                        : () async {
                                            await consultahuella(
                                                _cargando, context);
                                          },
                                  ),
                                ),
                                value
                                    ? Positioned(
                                        top: 10,
                                        left: 190,
                                        child: Container(
                                          width: 30,
                                          height: 30,
                                          child: CircularProgressIndicator(
                                            strokeWidth: 6,
                                            color: UtilsColors.white,
                                          ),
                                        ),
                                      )
                                    : Container()
                              ],
                            );
                          },
                        ),
                        SizedBox(height: responsive.hp(3)),
                        // RoundedButton(
                        //   color: UtilsColors.primary,
                        //   text: "ir a finalizar",
                        //   press: () async {
                        //     final route = MaterialPageRoute(
                        //         builder: (BuildContext _) => PageResumen(
                        //               type: "REGISTRO",
                        //               productName: widget.productName,
                        //               productCode: widget.productCode,
                        //               enterpriseName: widget.enterpriseName,
                        //               deliveryCode: widget.deliveryCode,
                        //               code: widget.deliveryCode,
                        //               selectAddressMap: widget.selectAddressMap,
                        //               selectPersonMap: widget.selectPersonMap,
                        //               cargo: widget.cargo,
                        //               evidencia: widget.evidencia,
                        //               valbio: widget.valbio,
                        //               op: "0",
                        //               estados: widget.estados,
                        //               subestados: widget.subestados,
                        //             ));

                        //     Navigator.push(context, route);
                        //   },
                        //   loading: false,
                        //   //icon: "assets/svg/huella.svg",
                        // ),
                      ],
                    ),

                    SizedBox(height: responsive.hp(7)),

                    PiePagina()
                  ],
                ),
              ),
            )));
  }

  Future<void> consultahuella(
      ValueNotifier<bool> _cargando, BuildContext context) async {
    await _bioMethodChannel.invokeMethod("abrirScaner");
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      child: SvgPicture.asset(
                        "assets/svg/tiempo.svg",
                        // color: ViteColors.primary,
                      ),
                    ),
                    SizedBox(height: 25),
                    Text(
                      "¿Estás seguro?",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: UtilsColors.cris1,
                          fontSize: 12),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        "¿Quieres salir de la aplicación?",
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: UtilsColors.secundary,
                            fontSize: 11),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Row(
                      children: [
                        RoundedButtonDialog(
                          color: UtilsColors.secundary,
                          text: "Aceptar",
                          press: () async {
                            // ignore: unused_local_variable
                            final result = await FlutterRestart.restartApp();
                            //Navigator.of(context).pop(true);
                          },
                          loading: false,
                        ),
                        RoundedButtonDialog(
                          color: UtilsColors.secundary,
                          text: "Cancelar",
                          press: () async {
                            Navigator.of(context).pop(false);
                          },
                          loading: false,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        ) ??
        false;
  }

  Future<dynamic> openScan() async {
    final data = await _bioMethodChannel.invokeMethod("abrirScaner");

    print("resultado---$data");
    return data;
  }

  Future<dynamic> stopScan() async {
    final data = await _bioMethodChannel.invokeMethod("cerrarScaner");

    print("resultado cerrando scaner---$data");

    return data;
  }

  Future<dynamic> callBackNative(MethodCall call) async {
    // final String args = call.arguments;
    final verHuella = UtilsBiometria();
    switch (call.method) {
      case "allowDeviceOne":
        _cargando.value = true;
        await startScan(verHuella); //startScan
        await Future.delayed(Duration(seconds: 1), () {}); //startScan
        _cargando.value = false;

        break;
      case "allowDevice":
        _cargando.value = true;
        await startScan(verHuella);
        await Future.delayed(Duration(seconds: 1), () {}); //startScan
        _cargando.value = false;
        break;
      case "denyDevice":
        utils.showToast1("Usuario denegó el permiso", 2);
        break;
      case "noDeviceFound":
        print("noDeviceFound...");
        utils.showToast1("No se encontró ningún dispositivo conectado", 2);
        break;
      case "NoOpen":
        print("NoOpen dispositivo...");
        utils.showToast1("Can't open scanner device", 2);
        break;
      default:
        throw MissingPluginException('notImplemented');
    }
  }

  Future<void> startScan(UtilsBiometria verHuella) async {
    await Future.delayed(Duration(seconds: 5), () {});

    var bytehuella = await stopScan();
    int resHuella = await verHuella.verificarHuella(bytehuella);
    print("res start scan...$resHuella");
    if (resHuella == 1) {
      String apiType = "***Verificar Identidad Based wsq";

      //await verHuella.saveHuellaFile(bytehuella);

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext _) => DatosHitPage(
                    productName: widget.productName,
                    productCode: widget.productCode,
                    enterpriseName: widget.enterpriseName,
                    deliveryCode: widget.deliveryCode,
                    code: widget.deliveryCode,
                    selectAddressMap: widget.selectAddressMap,
                    selectPersonMap: widget.selectPersonMap,
                    cargo: widget.cargo,
                    evidencia: widget.evidencia,
                    estados: widget.estados,
                    subestados: widget.subestados,
                    valbio: widget.valbio,
                    apiType: apiType,
                    huellader: bytehuella,
                  )));
    } else {
      print("Imagen de huella no válida");
      utils.showToast1("Imagen de huella no válida", 2);
      _cargando.value = false;
      return;
    }
    //return resHuella;
  }
}
