import 'dart:io';
import 'dart:typed_data';

import 'package:enotria/api/api_consultaDatosReniec.dart';
import 'package:enotria/bloc/bloc_huellero/bloc_pruebahuellero_bloc.dart';
import 'package:enotria/model/consulta_reniec/consultaBiometriaReniecModel.dart';
import 'package:enotria/util/utils.dart' as utils2;
import 'package:enotria/native/my_first_platform_channel.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/formato_fecha.dart' as utils;
import 'package:enotria/util/responsive.dart';
import 'package:flutter/services.dart';
import 'package:image/image.dart' as IMG;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';

//import 'package:pdf/widgets.dart';

class PruebaMethodPage extends StatefulWidget {
  PruebaMethodPage({Key key}) : super(key: key);

  @override
  State<PruebaMethodPage> createState() => _PruebaMethodPageState();
}

class _PruebaMethodPageState extends State<PruebaMethodPage> {
  final dnicontroller = TextEditingController();
  //var _myvariable2 = HuelleroBiometrico();
  final _cargando = ValueNotifier<bool>(false);
  final _cargandoDer = ValueNotifier<bool>(false);

  var wsqModel = IdentidadWsqModel();

  final wsqBloc = BlocPruebahuelleroBloc();

  String res = "dd";
  var res2;
  var response;
  var huellaDer;

  //mejor huella
  var resmejorHuella1 = '';
  var resmejorHuella2 = '';
  var resmejorHuella3 = '';
  var resmejorHuella4 = '';
  var resmejorHuella5 = '';
  bool carga = false;

  var compareList;

  String _batteryLevel = 'Unknown battery level.';
  var _bioMethodChannel =
      MethodChannel("com.enotriasa/biometria_platform_channel");
  var _myvariable2 = HuelleroBiometrico();

  @override
  void initState() {
    _bioMethodChannel.setMethodCallHandler(callBackNative);
    super.initState();
  }

  @override
  void dispose() {
    _cargando.dispose();
    dnicontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Enotria"),
      ),
      body: BlocProvider.value(
        value: wsqBloc,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: responsive.hp(2)),
                Container(
                    width: responsive.wp(70),
                    height: responsive.hp(5),
                    child: TextField(
                      controller: dnicontroller,
                      keyboardType: TextInputType.number,
                      maxLength: 8,
                      style: TextStyle(fontSize: 30),
                      decoration:
                          InputDecoration(hintText: "Ingrese el N° Dni"),
                      onChanged: (String value) {
                        value = dnicontroller.text;
                      },
                    )),
                SizedBox(height: responsive.hp(.2)),
                Stack(
                  children: [
                    Center(
                      child: MaterialButton(
                        onPressed: () async {
                          //await _myvariable2.stopScan();
                          setState(() {
                            carga = true;
                          });
                          var resMejorHuella = await ApiConsultaDatosReniec()
                              .getMejorHuellaReniec(dnicontroller.text);

                          setState(() {
                            resmejorHuella1 = resMejorHuella["coHuellaDer"];
                            resmejorHuella2 = resMejorHuella["descHuellaDer"];
                            resmejorHuella3 = resMejorHuella["coHuellaIzq"];
                            resmejorHuella4 = resMejorHuella["descHuellaIzq"];
                            resmejorHuella5 = resMejorHuella["msgResponse"];
                            carga = false;
                            print("resmejor huella----$resMejorHuella");
                          });
                        },
                        child: Text("Mejor Huella",
                            style:
                                TextStyle(fontSize: 20, color: Colors.white)),
                        color: UtilsColors.secundary,
                      ),
                    ),
                    carga
                        ? Center(child: CircularProgressIndicator())
                        : Container()
                  ],
                ),
                SizedBox(height: responsive.hp(2)),
                Container(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text("Mensaje: ",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold)),
                          Expanded(
                            child: Text("$resmejorHuella5",
                                style: TextStyle(fontSize: 18)),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text("cod HuellaDer: ",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold)),
                          Text("$resmejorHuella1",
                              style: TextStyle(fontSize: 18)),
                        ],
                      ),
                      Row(
                        children: [
                          Text("descrip HuellaDer: ",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold)),
                          Text("$resmejorHuella2",
                              style: TextStyle(fontSize: 18)),
                        ],
                      ),
                      Row(
                        children: [
                          Text("cod HuellaIzq: ",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold)),
                          Text("$resmejorHuella3",
                              style: TextStyle(fontSize: 18)),
                        ],
                      ),
                      Row(
                        children: [
                          Text("descrip HuellaIzq",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold)),
                          Text("$resmejorHuella4",
                              style: TextStyle(fontSize: 18)),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: responsive.hp(3)),
                Text("Huella Digital",
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                SizedBox(height: responsive.hp(10)),

                // Container(
                //   width: responsive.wp(70),
                //   height: responsive.hp(25),
                //   child: ValueListenableBuilder(
                //       valueListenable: _cargando,
                //       builder:
                //           (BuildContext context, bool value, Widget child) {
                //         return Stack(
                //           children: [
                //             Center(
                //               child: Container(
                //                 child: res2 == null
                //                     ? Container(
                //                         color: Colors.grey[300],
                //                         width: responsive.wp(45),
                //                         height: responsive.hp(24),
                //                       )
                //                     : res2 == "No image"
                //                         ? Container(
                //                             color: Colors.grey[200],
                //                             width: responsive.wp(45),
                //                             height: responsive.hp(24),
                //                             child: Center(
                //                                 child: Text(
                //                               "No image",
                //                               style: TextStyle(fontSize: 20),
                //                             )),
                //                           )
                //                         : Image.memory(res2,
                //                             width: responsive.wp(44),
                //                             height: responsive.hp(24),
                //                             fit: BoxFit.contain),
                //               ),
                //             ),
                //             (value)
                //                 ? Positioned(
                //                     left: responsive.wp(8),
                //                     right: responsive.wp(8),
                //                     child: Container(
                //                         color: Colors.white.withOpacity(0.7),
                //                         margin: EdgeInsets.symmetric(
                //                             horizontal: responsive.wp(8),
                //                             vertical: responsive.wp(1)),
                //                         height: responsive.ip(24),
                //                         child: CupertinoActivityIndicator(
                //                             radius: 15)),
                //                   )
                //                 : Container()
                //           ],
                //         );
                //       }),
                // ),

                Padding(
                  padding: const EdgeInsets.only(top: 7.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      ValueListenableBuilder(
                          valueListenable: _cargandoDer,
                          builder: (BuildContext context, bool value1,
                              Widget child) {
                            return Stack(
                              children: [
                                BtnBiometria(
                                  responsive: responsive,
                                  onPress: () async {
                                    // _cargandoDer.value = true;

                                    // await Future.delayed(
                                    //     Duration(seconds: 5), () {

                                    // });
                                    print("""object""");

                                    await _bioMethodChannel
                                        .invokeMethod("abrirScaner");

                                    // setState(() {
                                    //   huellaDer = der;
                                    // });
                                    // _cargandoDer.value = false;
                                    print("huellaDer: $huellaDer");
                                  },
                                  txt: 'Biometria',
                                ),
                                value1
                                    ? Positioned(
                                        left: responsive.wp(6),
                                        right: responsive.wp(8),
                                        child: CircularProgressIndicator(),
                                      )
                                    : Container()
                              ],
                            );
                          }),
                      // BtnBiometria(
                      //   responsive: responsive,
                      //   onPress: () async {
                      //     try {
                      //       _cargando.value = true;

                      //       var resp2 = await _myvariable2.openScan();

                      //       if (resp2.length > 64) {
                      //         //tomar los primeros 64 elementos del array
                      //         var topList = resp2.take(192);

                      //         //convertir topList a lista con 192 elementos
                      //         var lista1 = topList.toList();
                      //         //comparar ambas listas: true o false
                      //         compareList = listEquals(lista1, noImage);

                      //         // setState(()  {
                      //         if (compareList == true) {
                      //           res2 = "No image";
                      //         } else {
                      //           //imagen de la huella
                      //           res2 = resp2;
                      //           print("Biometría: $res2");

                      //           wsqBloc.add(VerificarIdentidadWsqEvent(
                      //               "72676295", res2, res2));
                      //           //dnicontroller.text
                      //           //await _saveHuellaFile(huellaDer);

                      //         }

                      //         //print("res----$res2");
                      //         // });
                      //       } else {
                      //         res2 = null;
                      //         print("resdd----${resp2.toString()}");
                      //       }

                      //       _cargando.value = false;
                      //     } catch (e) {
                      //       print("error--- $e");
                      //       _cargando.value = false;
                      //     }
                      //   },
                      //   txt: 'Izquierda',
                      // ),
                    ],
                  ),
                ),
                SizedBox(height: 20),

                // Center(
                //   child: Column(
                //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //     children: [
                //       RaisedButton(
                //         child: Text('Get Battery Level'),
                //         onPressed: () async {
                //           _batteryLevel = await _myvariable2.getBatteryLevel();
                //           setState(() {});
                //         },
                //       ),
                //       Text(_batteryLevel),
                //     ],
                //   ),
                // )
                // BlocBuilder<BlocPruebahuelleroBloc, BlocPruebahuelleroState>(
                //   builder: (context, state) {
                //     return state.identidadWsqModel == null
                //         ? Container()
                //         : Container(
                //             child: Column(
                //               children: [
                //                 Row(
                //                   mainAxisAlignment: MainAxisAlignment.start,
                //                   children: [
                //                     Text("Código: ",
                //                         style: TextStyle(
                //                             fontSize: 18,
                //                             fontWeight: FontWeight.bold)),
                //                     Text(
                //                         "${state.identidadWsqModel.codResultado}",
                //                         style: TextStyle(fontSize: 18)),
                //                   ],
                //                 ),
                //                 // SizedBox(height: responsive.hp(2)),
                //                 Padding(
                //                   padding: const EdgeInsets.all(2.0),
                //                   child: Row(
                //                     mainAxisAlignment: MainAxisAlignment.start,
                //                     children: [
                //                       Text("Resultado: ",
                //                           style: TextStyle(
                //                               fontSize: 18,
                //                               fontWeight: FontWeight.bold)),
                //                       Expanded(
                //                         child: Text(
                //                             "${state.identidadWsqModel.descripResultado}",
                //                             style: TextStyle(fontSize: 18)),
                //                       ),
                //                     ],
                //                   ),
                //                 ),
                //                 // SizedBox(height: responsive.hp(2)),
                //                 Row(
                //                   mainAxisAlignment: MainAxisAlignment.start,
                //                   children: [
                //                     Text("Dni: ",
                //                         style: TextStyle(
                //                             fontSize: 18,
                //                             fontWeight: FontWeight.bold)),
                //                     Text("${state.identidadWsqModel.dni}",
                //                         style: TextStyle(fontSize: 18)),
                //                   ],
                //                 ),
                //                 // SizedBox(height: responsive.hp(2)),
                //                 Row(
                //                   mainAxisAlignment: MainAxisAlignment.start,
                //                   children: [
                //                     Text("Cod. de Restricción: ",
                //                         style: TextStyle(
                //                             fontSize: 18,
                //                             fontWeight: FontWeight.bold)),
                //                     Text(
                //                         "${state.identidadWsqModel.coRestriccion}",
                //                         style: TextStyle(fontSize: 18)),
                //                   ],
                //                 ),
                //               ],
                //             ),
                //           );
                //   },
                // )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<dynamic> callBackNative(MethodCall call) async {
    // final String args = call.arguments;
    final verHuella = UtilsBiometria();
    switch (call.method) {
      case "allowDeviceOne":
        _cargando.value = true;
        await startScan(verHuella); //startScan
        await Future.delayed(Duration(seconds: 1), () {}); //startScan
        _cargando.value = false;

        break;
      case "allowDevice":
        _cargando.value = true;
        await startScan(verHuella);
        await Future.delayed(Duration(seconds: 1), () {}); //startScan
        _cargando.value = false;
        break;
      case "denyDevice":
        utils2.showToast1("Usuario denegó el permiso", 2);
        break;
      case "noDeviceFound":
        print("noDeviceFound...");
        utils2.showToast1("No se encontró ningún dispositivo conectado", 2);
        break;
      case "NoOpen":
        print("NoOpen dispositivo...");
        utils2.showToast1("Can't open scanner device", 2);
        break;
      default:
        throw MissingPluginException('notImplemented');
    }
  }

  Future<void> startScan(UtilsBiometria verHuella) async {
    await Future.delayed(Duration(seconds: 5), () {});

    var bytehuella = await _myvariable2.stopScan();
    int resHuella = await verHuella.verificarHuella(bytehuella);
    print("res start scan...$resHuella");
   // if (resHuella == 1) {
      String apiType = "***Verificar Identidad Based wsq";

      await verHuella.saveHuellaFile(bytehuella);
    // } else {
    //   print("Imagen de huella no válida");
    //   utils2.showToast1("Imagen de huella no válida", 2);
    //   _cargando.value = false;
    //   return;
    // }
    //return resHuella;
  }
}

Future<bool> callTest() async {
  bool res = false;
  var _platform =
      const MethodChannel("com.enotriasa/biometria_platform_channel");

  try {
    res = await _platform.invokeMethod('myMethod') == 1;
  } on PlatformException catch (e) {
    print('Error = $e');
  }

  print('Is call successful? $res');
  return res;
}

Future<void> _fromNative(MethodCall call) async {
  if (call.method == 'abrirScaner') {
    print('callTest result = ${call.arguments}');
  }
}

Future<Uint8List> resizeImage(Uint8List huella) async {
  Uint8List resizedData = huella;
  IMG.Image img = IMG.decodeImage(huella);
  IMG.Image resized = IMG.copyResize(img, width: 512, height: 512);
  resizedData = IMG.encodeJpg(resized);
  return resizedData;
}

Future<void> _saveHuellaFile(huellaDer) async {
  // UserPreferences prefs = new UserPreferences();
  Uint8List archivoHuella = huellaDer;
  //directorio en donde guardará
  final directorio = await getTemporaryDirectory();
  print("directorio---- $directorio");

  //hora actual
  DateTime horaActual = DateTime.now();
  //formato de hora
  final date = utils.obtenerFechaHoraDate(horaActual);
  //nombre del archivo
  String nameFile = "Prueba" + date + '.wsq';

  File newFile = await File('${directorio.path}/$nameFile').create();

  //final resizeHuella= await  resizeImage(archivoHuella);

  newFile.writeAsBytesSync(archivoHuella);
  print("peso: ${newFile.lengthSync()}");

//Tu archivo Uint8List ya es un File
}

class BtnBiometria extends StatelessWidget {
  const BtnBiometria({
    Key key,
    @required this.responsive,
    @required this.txt,
    @required this.onPress,
  }) : super(key: key);

  final Responsive responsive;
  final String txt;
  final VoidCallback onPress;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: responsive.wp(30),
      child: MaterialButton(
          child: Text(txt, style: TextStyle(fontSize: 20, color: Colors.white)),
          onPressed: onPress),
      color: UtilsColors.secundary,
    );
  }
}
