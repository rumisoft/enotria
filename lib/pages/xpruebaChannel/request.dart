import 'package:enotria/native/biometria_channel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RequestPage extends StatefulWidget {
  @override
  State<RequestPage> createState() => _RequestPageState();
}

class _RequestPageState extends State<RequestPage> {
  Future<void> _requestPermission() async {
    
    final PermissionStatus status = await BiometriaChannel.instance.requestPermission();

    if (status == PermissionStatus.granted) {
      Navigator.pushNamed(context, "pruebaMethod");
    } else {
      //mostrar una alerta de aceptar los Permisos
      print("debe aceptar los permisos para continuar");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 10),
            CupertinoButton(
                child: Text(
                  "ALLOW",
                  style: TextStyle(
                    letterSpacing: 1,
                  ),
                ),
                color: Colors.redAccent,
                borderRadius: BorderRadius.circular(30),
                onPressed: () async {
                  await _requestPermission();
                })
          ],
        ),
      ),
    );
  }
}
