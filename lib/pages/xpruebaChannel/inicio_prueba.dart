import 'package:enotria/native/biometria_channel.dart';
import 'package:enotria/pages/xpruebaChannel/request.dart';
import 'package:flutter/material.dart';

class InicioPage extends StatefulWidget {
  InicioPage({Key key}) : super(key: key);

  @override
  State<InicioPage> createState() => _InicioPageState();
}

class _InicioPageState extends State<InicioPage> {
  @override
  void initState() {
    super.initState();
    _init();
  }

  Future<void> _init() async {
    await Future.delayed(Duration(seconds: 2));
    // final PermissionStatus status =await BiometriaChannel.instance.checkPermission();

    // if (status == PermissionStatus.granted) {
    //   Navigator.pushNamed(context, "pruebaMethod");
    // } else {
    //   Navigator.push(context,MaterialPageRoute(
    //     builder: (BuildContext _) => RequestPage(),
    //   ));
      
    // }
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext _) => RequestPage(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
