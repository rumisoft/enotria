

import 'package:enotria/bloc/bloc_password_recovery/bloc_recovery_password_bloc.dart';
import 'package:enotria/pages/recuperar_contrasenia/page_writeUser.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/constast.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:yaml/yaml.dart';

class RecoveryPassEnd extends StatefulWidget {
  final bool status;
  final String response;
  RecoveryPassEnd({Key key, this.status, this.response}) : super(key: key);

  @override
  _RecoveryPassEndState createState() => _RecoveryPassEndState();
}

class _RecoveryPassEndState extends State<RecoveryPassEnd> {
  // ignore: unused_field
  GlobalKey<FormState> _formKey = GlobalKey();
  BlocRecoveryPasswordBloc _bloc = BlocRecoveryPasswordBloc();
  String _version;
  bool _status;
  // ignore: unused_field
  String _response;

  @override
  void initState() {
    _status = widget.status;
    _response = widget.response;
    _bloc.add(Process());
    _bloc.add(TimerSend(context: context));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.bold,
                color: UtilsColors.white),
          ),
        ),
        backgroundColor: UtilsColors.white,
        body: BlocBuilder<BlocRecoveryPasswordBloc, BlocRecoveryPasswordState>(
          builder: (_, state) {
            return Stack(
              children: <Widget>[
                SingleChildScrollView(
                  child: Container(
                      padding: EdgeInsets.all(5),
                      child: Center(
                        child: Column(
                          children: [
                            (!_status)
                                ? Column(
                                    children: [
                                      SizedBox(height: 20),
                                      Text(
                                        "Recuperación de Contraseña",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15,
                                            fontFamily: "Cocon",
                                            letterSpacing: 1),
                                        textAlign: TextAlign.center,
                                      ),
                                      SizedBox(
                                        height: 100,
                                      ),
                                      Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: SvgPicture.asset(
                                          "assets/svg/error.svg",
                                          height: 60,
                                          color: UtilsColors.danger,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: Text(
                                            "¡Error cambio de contraseña!",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 14,
                                                color: UtilsColors.danger)),
                                      ),
                                      SizedBox(
                                        height: 30,
                                      ),
                                      Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: RoundedButton(
                                          color: UtilsColors.danger,
                                          text: "Reintentar",
                                          press: () {
                                            Navigator.of(context)
                                                .pushAndRemoveUntil(
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          WriteUser(),
                                                    ),
                                                    (Route<dynamic> route) =>
                                                        false);
                                          },
                                          loading: state.process ? true : false,
                                        ),
                                      ),
                                    ],
                                  )
                                : Column(
                                    children: [
                                      SizedBox(height: 20),
                                      Text(
                                        "Recuperación de Contraseña",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15,
                                            fontFamily: "Cocon",
                                            letterSpacing: 1),
                                        textAlign: TextAlign.center,
                                      ),
                                      SizedBox(
                                        height: 100,
                                      ),
                                      Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: SvgPicture.asset(
                                          "assets/svg/check.svg",
                                          height: 60,
                                          color: UtilsColors.success,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: Text(
                                            "¡Se realizo tu cambio de contraseña!",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 14,
                                                color: UtilsColors.success)),
                                      ),
                                      SizedBox(
                                        height: 30,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Redireccionando al login en",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 14,
                                                color: UtilsColors.cris1),
                                          ),
                                          Text(
                                            " ${state.timeSend}",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18,
                                                color: UtilsColors.success),
                                          )
                                        ],
                                      ),
                                    ],
                                  )
                          ],
                        ),
                      )),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                        child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: "Enotria S.A. Ⓒ2021  v${appversion.toString()}",
                            recognizer: TapGestureRecognizer()..onTap = () {},
                            style: TextStyle(
                              color: UtilsColors.tersary,
                              fontSize: 13,
                              letterSpacing: 0.1,
                            ),
                          )
                        ],
                      ),
                    ))
                  // FutureBuilder(
                  //   future: rootBundle.loadString("pubspec.yaml"),
                  //   builder: (context, snapshot) {
                  //     if (snapshot.hasData) {
                  //       var yaml = loadYaml(snapshot.data);

                  //       _version = yaml["appversion"];
                  //     }

                  //     return Container(
                  //         child: RichText(
                  //       text: TextSpan(
                  //         children: [
                  //           TextSpan(
                  //             text:
                  //                 "Enotria S.A. Ⓒ2021  v${_version.toString()}",
                  //             recognizer: TapGestureRecognizer()..onTap = () {},
                  //             style: TextStyle(
                  //               color: UtilsColors.tersary,
                  //               fontSize: 13,
                  //               letterSpacing: 0.1,
                  //             ),
                  //           )
                  //         ],
                  //       ),
                  //     ));
                  //   },
                  // ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
