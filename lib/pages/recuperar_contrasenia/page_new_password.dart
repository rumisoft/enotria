import 'package:enotria/bloc/bloc_password_recovery/bloc_recovery_password_bloc.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/constast.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/RoundedPassword.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yaml/yaml.dart';

class NewPasswordPage extends StatefulWidget {
  final String hash;
  NewPasswordPage({Key key, this.hash}) : super(key: key);

  @override
  _NewPasswordPageState createState() => _NewPasswordPageState();
}

class _NewPasswordPageState extends State<NewPasswordPage> {
  GlobalKey<FormState> _formKey = GlobalKey();
  BlocRecoveryPasswordBloc _bloc = BlocRecoveryPasswordBloc();
  String _version;
  String _hash;
  // ignore: unused_field
  String _code;
  // ignore: unused_field
  String _pass1;
  // ignore: unused_field
  String _pass2;
  @override
  void initState() {
    _hash = widget.hash;
    _bloc.add(Process());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.bold,
                color: UtilsColors.white),
          ),
        ),
        backgroundColor: UtilsColors.white,
        body: BlocBuilder<BlocRecoveryPasswordBloc, BlocRecoveryPasswordState>(
          builder: (_, state) {
            return Stack(
              children: <Widget>[
                SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: [
                              SizedBox(height: 20),
                              Text(
                                "Nueva Contraseña",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    fontFamily: "Cocon",
                                    letterSpacing: 1),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 50,
                              ),
                              Form(
                                key: _formKey,
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    RoundedPassword(
                                      hintText: "Ingrese Contraseña Nueva",
                                      icon: Icons.lock,
                                      sufixicon: true,
                                      validator: (value) {
                                        this._pass1 = value;
                                        return value.length != 0
                                            ? null
                                            : 'Ingrese Contraseña Nueva';
                                      },
                                    ),
                                    RoundedPassword(
                                      hintText: "Confirmar Contraseña Nueva",
                                      icon: Icons.lock,
                                      sufixicon: true,
                                      validator: (value) {
                                        this._pass2 = value;
                                        return value.length != 0
                                            ? null
                                            : 'Confirmar Contraseña Nueva';
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                width: double.infinity,
                                margin: EdgeInsets.symmetric(
                                    vertical: 0, horizontal: 20),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Caracteristicas de la contraseña:",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 13),
                                      textAlign: TextAlign.start,
                                    ),
                                    Text("Como mínimo 8 caracteres."),
                                    Text("Como mínimo 1 número."),
                                    Text("Como mínimo 1 carácter especial."),
                                    Text("Como mínimo 1 letra mayúscula."),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 20),
                                child: RoundedButton(
                                  color: UtilsColors.secundary,
                                  text: "Guardar",
                                  press: () {
                                    if (_formKey.currentState.validate()) {
                                      print("$_hash");
                                      _bloc.add(NewPassword(
                                          contextNew: context,
                                          pass1: _pass1,
                                          pass2: _pass2,
                                          hash: _hash));
                                    }
                                  },
                                  loading: state.process ? true : false,
                                ),
                              ),
                              SizedBox(height: 5),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                        child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text:
                                "Enotria S.A. Ⓒ2021  v${appversion.toString()}",
                            recognizer: TapGestureRecognizer()..onTap = () {},
                            style: TextStyle(
                              color: UtilsColors.tersary,
                              fontSize: 13,
                              letterSpacing: 0.1,
                            ),
                          )
                        ],
                      ),
                    ))

                    // FutureBuilder(
                    //   future: rootBundle.loadString("pubspec.yaml"),
                    //   builder: (context, snapshot) {
                    //     if (snapshot.hasData) {
                    //       var yaml = loadYaml(snapshot.data);

                    //       _version = yaml["appversion"];
                    //     }

                    //     return Container(
                    //         child: RichText(
                    //       text: TextSpan(
                    //         children: [
                    //           TextSpan(
                    //             text:
                    //                 "Enotria S.A. Ⓒ2021  v${_version.toString()}",
                    //             recognizer: TapGestureRecognizer()..onTap = () {},
                    //             style: TextStyle(
                    //               color: UtilsColors.tersary,
                    //               fontSize: 13,
                    //               letterSpacing: 0.1,
                    //             ),
                    //           )
                    //         ],
                    //       ),
                    //     ));
                    //   },
                    // ),
                    ),
              ],
            );
          },
        ),
      ),
    );
  }
}
