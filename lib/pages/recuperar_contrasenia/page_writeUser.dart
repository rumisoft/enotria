import 'package:enotria/bloc/bloc_password_recovery/bloc_recovery_password_bloc.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/constast.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/RoundedInput.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yaml/yaml.dart';

class WriteUser extends StatefulWidget {
  WriteUser({Key key}) : super(key: key);

  @override
  _WriteUserState createState() => _WriteUserState();
}

class _WriteUserState extends State<WriteUser> {
  GlobalKey<FormState> _formKey = GlobalKey();
  BlocRecoveryPasswordBloc _bloc = BlocRecoveryPasswordBloc();
  String _version;
  String _user;
  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: Text(
            "",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.bold,
                color: UtilsColors.white),
          ),
        ),
        backgroundColor: UtilsColors.white,
        body: BlocBuilder<BlocRecoveryPasswordBloc, BlocRecoveryPasswordState>(
          builder: (_, state) {
            return Stack(
              children: <Widget>[
                SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: [
                              SizedBox(height: 20),
                              Text(
                                "Recuperar Contraseña",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    fontFamily: "Cocon",
                                    letterSpacing: 1),
                                textAlign: TextAlign.center,
                              ),
                              Container(
                                padding: EdgeInsets.all(20),
                                child: Text(
                                  "Para recuperar tu contraseña enviaremos un código via SMS al celular relacionado a tu usuario",
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                      fontFamily: "Gelion", fontSize: 15),
                                ),
                              ),
                              Form(
                                key: _formKey,
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    RoundedInput(
                                      hintText: "Ingrese Usuario",
                                      keyboardType: TextInputType.emailAddress,
                                      icon: Icons.person,
                                      validator: (value) {
                                        this._user = value;
                                        return value != ""
                                            ? null
                                            : 'El usuario es obligatorio.';
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 20),
                                child: RoundedButton(
                                  color: UtilsColors.secundary,
                                  text: "Enviar Código",
                                  press: () {
                                    print("$_user");

                                    if (_formKey.currentState.validate()) {


                                       _bloc.add(ValidateUser(
                                           user: _user, context: context));

                                    }
                                  },
                                  loading: state.process ? true : false,
                                ),
                              ),
                              SizedBox(height: 5),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                        child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text:
                                "Enotria S.A. Ⓒ2021  v${appversion.toString()}",
                            recognizer: TapGestureRecognizer()..onTap = () {},
                            style: TextStyle(
                              color: UtilsColors.tersary,
                              fontSize: 13,
                              letterSpacing: 0.1,
                            ),
                          )
                        ],
                      ),
                    ))
                  
                  // FutureBuilder(
                  //   future: rootBundle.loadString("pubspec.yaml"),
                  //   builder: (context, snapshot) {
                  //     // if (snapshot.hasData) {
                  //     //   var yaml = loadYaml(snapshot.data);

                  //     //   _version = yaml["appversion"];
                  //     // }

                  //     return Container(
                  //         child: RichText(
                  //       text: TextSpan(
                  //         children: [
                  //           TextSpan(
                  //             text:
                  //                 "Enotria S.A. Ⓒ2021  v${appversion.toString()}",
                  //             recognizer: TapGestureRecognizer()..onTap = () {},
                  //             style: TextStyle(
                  //               color: UtilsColors.tersary,
                  //               fontSize: 13,
                  //               letterSpacing: 0.1,
                  //             ),
                  //           )
                  //         ],
                  //       ),
                  //     ));
                  //   },
                  // 
                  // ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
