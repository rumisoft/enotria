import 'package:enotria/bloc/bloc_password_recovery/bloc_recovery_password_bloc.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/constast.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/RoundedInput.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:yaml/yaml.dart';

class ValidateCode extends StatefulWidget {
  final String user;
  final String phone;
  ValidateCode({Key key, this.user, this.phone}) : super(key: key);

  @override
  _ValidateCodeState createState() => _ValidateCodeState();
}

class _ValidateCodeState extends State<ValidateCode> {
  GlobalKey<FormState> _formKey = GlobalKey();
  BlocRecoveryPasswordBloc _bloc = BlocRecoveryPasswordBloc();
  String _version;
  String _user;
  String _phone;
  // ignore: unused_field
  String _code;
  @override
  void initState() {
    _user = widget.user;
    _phone = widget.phone;
    _bloc.add(Process());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.bold,
                color: UtilsColors.white),
          ),
        ),
        backgroundColor: UtilsColors.white,
        body: BlocBuilder<BlocRecoveryPasswordBloc, BlocRecoveryPasswordState>(
          builder: (_, state) {
            return Stack(
              children: <Widget>[
                SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: [
                              SizedBox(height: 20),
                              Text(
                                "Validar código",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    fontFamily: "Cocon",
                                    letterSpacing: 1),
                                textAlign: TextAlign.center,
                              ),
                              Container(
                                padding: EdgeInsets.all(20),
                                child: Text(
                                  "Se ha enviado un código po SMS al número celular ${_phone.toString()}",
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                      fontFamily: "Gelion", fontSize: 15),
                                ),
                              ),
                              Form(
                                key: _formKey,
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    RoundedInput(
                                      hintText: "Ingrese Código",
                                      keyboardType: TextInputType.emailAddress,
                                      icon: Icons.phone_android,
                                      validator: (value) {
                                        _code = value;
                                        return value != ""
                                            ? null
                                            : 'El código es obligatorio.';
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 20),
                                child: RoundedButton(
                                  color: UtilsColors.secundary,
                                  text: "Validar",
                                  press: () {
                                    if (_formKey.currentState.validate()) {
                                      _bloc.add(ValidateCodeSms(
                                          user: _user,
                                          code: _code,
                                          contextSms: context));
                                    }
                                  },
                                  loading: state.process ? true : false,
                                ),
                              ),
                              SizedBox(height: 35),
                              Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.replay_outlined,
                                      color: UtilsColors.tersary,
                                    ),
                                    RichText(
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                            text: 'No recibí el código',
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                showTopSnackBar(
                                                  context,
                                                  CustomSnackBar.info(
                                                    message:
                                                        "Enviando nuevo código a: ${_user.toString()}",
                                                  ),
                                                );
                                                _bloc.add(ValidateUser(
                                                    user: _user,
                                                    context: context,
                                                    type: "yes"));
                                              },
                                            style: TextStyle(
                                              color: UtilsColors.tersary,
                                              fontSize: 17,
                                              letterSpacing: 0.5,
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                        child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: "Enotria S.A. Ⓒ2021  v${appversion.toString()}",
                            recognizer: TapGestureRecognizer()..onTap = () {},
                            style: TextStyle(
                              color: UtilsColors.tersary,
                              fontSize: 13,
                              letterSpacing: 0.1,
                            ),
                          )
                        ],
                      ),
                    ))
                  // FutureBuilder(
                  //   future: rootBundle.loadString("pubspec.yaml"),
                  //   builder: (context, snapshot) {
                  //     if (snapshot.hasData) {
                  //       var yaml = loadYaml(snapshot.data);

                  //       _version = yaml["appversion"];
                  //     }

                  //     return Container(
                  //         child: RichText(
                  //       text: TextSpan(
                  //         children: [
                  //           TextSpan(
                  //             text:
                  //                 "Enotria S.A. Ⓒ2021  v${_version.toString()}",
                  //             recognizer: TapGestureRecognizer()..onTap = () {},
                  //             style: TextStyle(
                  //               color: UtilsColors.tersary,
                  //               fontSize: 13,
                  //               letterSpacing: 0.1,
                  //             ),
                  //           )
                  //         ],
                  //       ),
                  //     ));
                  //   },
                  // ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
