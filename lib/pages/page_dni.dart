import 'package:enotria/bloc/bloc_busqueda_dani/bloc_busqueda_dni_bloc.dart';
import 'package:enotria/pages/page_resumen.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/userPreferences.dart';
import 'package:enotria/widget/all/AlertDialog.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/RoundedInput.dart';
import 'package:enotria/widget/all/dialogGetx.dart';
import 'package:enotria/widget/all/menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_restart/flutter_restart.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qrscan/qrscan.dart' as scanner;

class PageDni extends StatefulWidget {
  PageDni({Key key}) : super(key: key);

  @override
  _PageDniState createState() => _PageDniState();
}

class _PageDniState extends State<PageDni> with WidgetsBindingObserver {
  //instancia del bloc de busqueda x dni
  BlocBusquedaDniBloc _bloc = BlocBusquedaDniBloc();
  //key del form
  GlobalKey<FormState> _formKeyy = GlobalKey();
  String _document;

  UserPreferences _pre = new UserPreferences();
  DateTime _salida;
  DateTime _entrada;
  int _timeInactive;
  TextEditingController _outputController;

  sendResumenFinalize(BlocBusquedaDniState state, int index) {
    _bloc.add(SendConsultDni(datum: state.data[index], context: context));
  }

  sendImageUpdateBloc(
      BlocBusquedaDniState state, int index, Map estados, Map subestados) {
    final route = MaterialPageRoute(
        builder: (BuildContext _) => PageResumen(
              type: "IMG",
              productName: state.data[0].product.productName,
              productCode: state.data[0].product.productCode,
              enterpriseName: state.data[0].enterprise.enterpriseName,
              deliveryCode: state.data[0].deliveryCode,
              code: state.data[0].deliveryCode,
              selectAddressMap: state.addressList[0],
              selectPersonMap: state.personList[0],
              cargo: state.cargoList,
              evidencia: state.evidenciaList,
              estados: estados,
              subestados: subestados,
            ));

    Navigator.push(context, route);

    // _bloc.add(SendConsultDni(datum: state.data[index], context: context));
  }

  @override
  // ignore: missing_return
  Future<void> initState() {
    super.initState();
    _timeInactive = int.parse(_pre.downtime);
    //_bloc.timeOperation();
    this._outputController = new TextEditingController();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _bloc.close();
    super.dispose();
  }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState statee) async {
    if (statee == AppLifecycleState.resumed) {
      _entrada = DateTime.parse(formatISOTime(DateTime.now()));

      Duration _diastotales = _entrada.difference(_salida);

      int _timeouth = _diastotales.inMinutes;

      if (_timeouth >= _timeInactive) {
        // ignore: unused_local_variable
        var f = await await newMethodDialog();
      }
    } else if (statee == AppLifecycleState.inactive) {
      setState(() {
        _salida = DateTime.parse(formatISOTime(DateTime.now()));
      });
    } else if (statee == AppLifecycleState.paused) {
    } else if (statee == AppLifecycleState.detached) {}
  }

  Future _scan() async {
    await Permission.camera.request();
    String barcode = await scanner.scan();
    if (barcode == null) {
      print('nothing return.');
    } else {
      this._outputController.text = barcode;
    }
  }

  @override
  Widget build(BuildContext context) {
    final MediaQueryData media = MediaQuery.of(context);

    final Size size = media.size;
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();
   // _outputController.text = "PED0043684";
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: UtilsColors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: UtilsColors.primary,
          centerTitle: true,
          title: Text(
            "DESPACHO ",
            style: TextStyle(color: UtilsColors.white),
          ),
          leading: Theme(
            data: Theme.of(context).copyWith(
              canvasColor: UtilsColors.cris1,
            ),
            child: IconButton(
              icon: Icon(
                Icons.more_vert,
                size: 40,
                color: Colors.white,
              ),
              onPressed: () => _scaffoldKey.currentState.openDrawer(),
            ),
          ),
        ),
        drawer: DrawerUser(),
        body: BlocBuilder<BlocBusquedaDniBloc, BlocBusquedaDniState>(
          builder: (_, state) {
            return WillPopScope(
                child: Column(
                  children: [
                    SizedBox(height: 5),
                    Form(
                      key: _formKeyy,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(
                                vertical: 0,
                                horizontal: 20,
                              ),
                              padding: EdgeInsets.symmetric(
                                horizontal: 15,
                                vertical: 0,
                              ),
                              width: size.width * 0.10,
                              decoration: BoxDecoration(
                                color: UtilsColors.cris0,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  RoundedInputSearch(
                                    controller: this._outputController,
                                    hintText: "CÓDIGO:",
                                    keyboardType: TextInputType.text,
                                    icon: Icons.person,
                                    validator: (value) {
                                      this._document = value;
                                      if (value != "" && value.length <= 15) {
                                        return null;
                                      }
                                      return 'Código incorrecto.';
                                    },
                                  ),
                                  Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 0),
                                      child: Row(
                                        children: [
                                          RoundedButtonSearch(
                                            icon: Icons.search,
                                            color: UtilsColors.secundary,
                                            text: "search",
                                            press: () {
                                              if (_formKeyy.currentState
                                                  .validate()) {
                                                state.timer = 0;

                                                _bloc.add(SearchByDocument(
                                                  document: this._document,
                                                  contex: context,
                                                ));
                                              } else {}
                                            },
                                            loading: state.search,
                                          ),
                                          RoundedButtonSearch(
                                            icon: Icons.qr_code_outlined,
                                            color: UtilsColors.secundary,
                                            text: "search",
                                            press: _scan,
                                            loading: false,
                                          ),
                                        ],
                                      ))
                                ],
                              ),
                            ),
                            SizedBox(height: 10),
                            RoundedInputRead(
                              read: true,
                              title: "Nombre completo del Cliente:",
                              hintText: '${state.enterpriseName}',
                              keyboardType: TextInputType.text,
                              icon: Icons.home,
                            ),
                          ]),
                    ),
                    SizedBox(height: 10),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Text(
                          "  Entregas:",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: UtilsColors.cris1),
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                    Expanded(
                      child: ListView.builder(
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              print(
                                  "---------------------------------------------- INFO ---------------------------------");
                              print(
                                  "---------------------------------------------- ${state.data[index].updateImg}---------------------------------");

                              /*
                              0=registro normal
                              1=actualizar imagenes
                              2=regitro || actualizar imagenes

                              */

                              switch (state.data[index].updateImg) {
                                case 0:
                                  print("CASO 0");
                                  sendResumenFinalize(state, index);
                                  break;
                                case 1:
                                  alertDialog(
                                      context: context,
                                      pressCANCEL: () {},
                                      pressOK: () {},
                                      type: "INFO",
                                      text:
                                          "¿Desea actualizar las imagenes de ultima Gestion realizada?",
                                      buttonOkText: "Actualizar Imagenes",
                                      buttonOkpress: () {
                                        Map _estado = Map();
                                        _estado["code"] =
                                            state.data[index].state.code;
                                        _estado["description"] =
                                            state.data[index].state.description;

                                        Map _subestado = Map();
                                        _subestado["code"] =
                                            state.data[index].substate.code;
                                        _subestado["description"] = state
                                            .data[index].substate.description;

                                        sendImageUpdateBloc(
                                            state, index, _estado, _subestado);
                                      });
                                  break;
                                case 2:
                                  alertDialog2Button(
                                    context: context,
                                    pressCANCEL: () {},
                                    pressOK: () {},
                                    type: "INFO",
                                    text:
                                        "Se tiene una gestión registrada ¿Desea actualizar las imagenes o registrar Nueva Gestión?",
                                    buttonOkText: "Actualizar Imagenes",
                                    buttonOkpress: () {
                                      Map _estado = Map();
                                      _estado["code"] =
                                          state.data[index].state.code;
                                      _estado["description"] =
                                          state.data[index].state.description;

                                      Map _subestado = Map();
                                      _subestado["code"] =
                                          state.data[index].substate.code;
                                      _subestado["description"] = state
                                          .data[index].substate.description;
                                      sendImageUpdateBloc(
                                          state, index, _estado, _subestado);
                                    },
                                    buttonOkText2: "Nueva Gestión",
                                    buttonOkpress2: () {
                                      print("Nueva Gestion");
                                      sendResumenFinalize(state, index);
                                    },
                                  );
                                  break;
                                default:
                              }
                            },
                            child: new Card(
                              child: new SizedBox(
                                height: 60.0,
                                child: new Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Expanded(
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                width: 82,
                                                child: Text(
                                                  " Cod. Entrega ",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 11),
                                                ),
                                              ),
                                              Container(
                                                padding: EdgeInsets.all(0.0),
                                                child: Text(
                                                  ': ${state.data[index].deliveryCode}',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    color: UtilsColors.cris1,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                width: 82,
                                                child: Text(
                                                  " Empresa ",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 11),
                                                ),
                                              ),
                                              Container(
                                                padding: EdgeInsets.all(0.0),
                                                child: Text(
                                                  ': ${state.data[index].enterprise.enterpriseName}',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    color: UtilsColors.cris1,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                width: 82,
                                                child: Text(
                                                  " Producto ",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 11),
                                                ),
                                              ),
                                              Flexible(
                                                child: Container(
                                                  padding: EdgeInsets.all(0.0),
                                                  child: Text(
                                                    ': ${state.data[index].product.productName}',
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: new TextStyle(
                                                      fontSize: 12,
                                                      color: UtilsColors.cris1,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.center,
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 1.0),
                                      child: Icon(
                                        Icons.arrow_right_rounded,
                                        size: 35,
                                        color: UtilsColors.primary,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                        itemCount: state.data.length,
                      ),
                    ),
                    Container(
                      height: 50,
                      padding: new EdgeInsets.only(top: 0.0, left: 10),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Enotria S.A. Ⓒ2021",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 13, color: UtilsColors.cris1),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                onWillPop: _onBackPressed);
          },
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      child: SvgPicture.asset(
                        "assets/svg/tiempo.svg",
                        // color: ViteColors.primary,
                      ),
                    ),
                    SizedBox(height: 25),
                    Text(
                      "¿Estás seguro?",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: UtilsColors.cris1,
                          fontSize: 12),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        "¿Quieres salir de la aplicación?",
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: UtilsColors.secundary,
                            fontSize: 11),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Row(
                      children: [
                        RoundedButtonDialog(
                          color: UtilsColors.secundary,
                          text: "Aceptar",
                          press: () async {
                            // ignore: unused_local_variable
                            final result = await FlutterRestart.restartApp();
                            //Navigator.of(context).pop(true);
                          },
                          loading: false,
                        ),
                        RoundedButtonDialog(
                          color: UtilsColors.secundary,
                          text: "Cancelar",
                          press: () async {
                            Navigator.of(context).pop(false);
                          },
                          loading: false,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        ) ??
        false;
  }
}
