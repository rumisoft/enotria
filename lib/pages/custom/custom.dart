import 'package:enotria/util/UtilsColors.dart';
import 'package:flutter/material.dart';

class CustomInputs {
  static InputDecoration loginInputDecoration(
      { String hint,
      IconData icon,
      bool isPrefixIcon = true,
      double borderRadius = 30,
      Color fillcolor = UtilsColors.cris0}) {
    return InputDecoration(
      filled: true,
      fillColor: fillcolor,
      border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(borderRadius)),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(borderRadius)),
      hintText: hint,
      prefixIcon: isPrefixIcon ? Icon(icon) : null,
      labelStyle: TextStyle(color: UtilsColors.secundary),
      hintStyle: TextStyle(color: UtilsColors.secundary.withOpacity(0.5)),
    );
  }

 
  static InputDecoration searchInputDecoration(
      { String hint,  IconData icon}) {
    return InputDecoration(
        border: InputBorder.none,
        enabledBorder: InputBorder.none,
        hintText: hint,
        prefixIcon: Icon(icon, color: UtilsColors.tersary),
        labelStyle: TextStyle(color: UtilsColors.tersary),
        hintStyle: TextStyle(color: UtilsColors.tersary));
  }

    static InputDecoration dropDownInputDecoration(
      { String laveltext,  IconData icon}) {
    return InputDecoration(
      filled: true,
      fillColor: UtilsColors.white,
      labelText: laveltext,
      suffixIcon: Icon(icon),
      border: OutlineInputBorder(
          borderSide: BorderSide.none, borderRadius: BorderRadius.circular(10)),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide.none, borderRadius: BorderRadius.circular(10)),
    );
  }

  static InputDecoration formInputDecoration({String labelText}) {
    return InputDecoration(
      filled: true,
      fillColor: UtilsColors.cris0,
      suffixIcon: Icon(Icons.arrow_drop_down),
      labelText: labelText,
      hoverColor: UtilsColors.tersary.withOpacity(0.1),
      border: OutlineInputBorder(
          borderSide: BorderSide.none, borderRadius: BorderRadius.circular(10)),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide.none, borderRadius: BorderRadius.circular(10)),
    );
  }
}
