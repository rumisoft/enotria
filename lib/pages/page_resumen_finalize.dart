import 'package:enotria/bloc/bloc_resumen_finalize/bloc_resumen_finalize_bloc.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/userPreferences.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/dialogGetx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_restart/flutter_restart.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';

class PageResumenFinalize extends StatefulWidget {
  final String type;
  final String productName;
  final String enterpriseName;
  final String deliveryCode;
  final String code;
  final Map<String, dynamic> selectAddressMap;
  final Map<String, dynamic> selectPersonMap;
  final String fecha;
  final String estado;
  final String subestado;

  final int countCargo;
  final int countEvidencias;
  final List<String> listCargo;
  final List<String> listEvidendia;
  final Position position;

  final String codEstado;
  final String codSubEstado;
  PageResumenFinalize({
    Key key,
    this.type,
    this.productName,
    this.enterpriseName,
    this.deliveryCode,
    this.selectAddressMap,
    this.selectPersonMap,
    this.fecha,
    this.code,
    this.estado,
    this.subestado,
    this.countCargo,
    this.countEvidencias,
    this.listCargo,
    this.listEvidendia,
    this.position,
    this.codEstado,
    this.codSubEstado,
  }) : super(key: key);

  @override
  _PageResumenFinalizeState createState() => _PageResumenFinalizeState();
}

class _PageResumenFinalizeState extends State<PageResumenFinalize>
    with WidgetsBindingObserver {
  BlocResumenFinalizeBloc _bloc = new BlocResumenFinalizeBloc();

  UserPreferences _pre = new UserPreferences();
  DateTime _salida;
  DateTime _entrada;
  int _timeInactive;

  @override
  void initState() {
    super.initState();
    _timeInactive = int.parse(_pre.downtime);
    //_bloc.timeOperation();
    WidgetsBinding.instance.addObserver(this);

    if (widget.type == "IMG") {
      print("ENVIANDO ACTUALIZACION DE IMAGEN...................");
      sendResumenUpdateImage(context);
    }
    if (widget.type == "REGISTRO") {
      print("ENVIANDO REGISTRO...................");
      sendResumen(context);
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _bloc.close();
    super.dispose();
  }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState statee) async {
    if (statee == AppLifecycleState.resumed) {
      // user returned to our app
      print(":::::::::::::::::RETURN TO APP");

      _entrada = DateTime.parse(formatISOTime(DateTime.now()));

      Duration _diastotales = _entrada.difference(_salida);

      int _timeouth = _diastotales.inMinutes;
      print("tiempo transcurrido : ${_timeouth.toString()}");
      if (_timeouth >= _timeInactive) {
        // ignore: unused_local_variable
        var f = await await newMethodDialog();
      }
    } else if (statee == AppLifecycleState.inactive) {
      print("SALIENTDO  RECUMEN FINALIZE...");
      setState(() {
        _salida = DateTime.parse(formatISOTime(DateTime.now()));
      });
    } else if (statee == AppLifecycleState.paused) {
      // user is about quit our app temporally
      // temporal
    } else if (statee == AppLifecycleState.detached) {
      // app suspended (not used in iOS)

    }
  }

  sendResumen(BuildContext context) {
    _bloc.add(
      SendResumenEnd(
        contex: context,
        idpedido: widget.code,
        latitud: "",
        longitud: "",
        statusType: widget.estado,
        subStatusType: widget.subestado,
        date: widget.fecha,
        fullName: widget.selectPersonMap["fullName"],
        documentType: widget.selectPersonMap["documentType"],
        documentId: widget.selectPersonMap["documentId"],
        cargos: widget.listCargo,
        evidencia: widget.listEvidendia,
        position: widget.position,
        codEstado: widget.codEstado,
        codSubEstado: widget.codSubEstado,
      ),
    );
  }

  sendResumenUpdateImage(BuildContext context) {
    _bloc.add(
      SendResumenUpdateImage (
        contex: context,
        deliveryCode:widget.deliveryCode,
        idpedido: widget.code,
        cargos: widget.listCargo,
        evidencia: widget.listEvidendia
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final MediaQueryData media = MediaQuery.of(context);
    final Size size = media.size;
    final EdgeInsets padding = media.padding;
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        appBar: AppBar(),
        backgroundColor: UtilsColors.white,
        body: BlocBuilder<BlocResumenFinalizeBloc, BlocResumenFinalizeState>(
          builder: (_, state) {
            return WillPopScope(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        height:
                            size.height - 100 - padding.top - padding.bottom,
                        child: Column(
                          children: [
                            SizedBox(height: 1),
                            Center(
                              child: Container(
                                child: Text(
                                  "Resumen del Pedido",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17,
                                      color: UtilsColors.cris1),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 5),
                              padding: EdgeInsets.symmetric(
                                horizontal: 5,
                                vertical: 5,
                              ),
                              width: size.width * 10,
                              decoration: BoxDecoration(
                                color: UtilsColors.cris0,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        width: 79,
                                        child: Text(
                                          "Nro. Pedido ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(0.0),
                                        child: Text(
                                          ': ${widget.deliveryCode}',
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: UtilsColors.cris1,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: 79,
                                        child: Text(
                                          "Dni ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(0.0),
                                        child: Text(
                                          ': ${widget.selectPersonMap["documentId"]}',
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: UtilsColors.cris1,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: 79,
                                        child: Text(
                                          "Nombres ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12),
                                        ),
                                      ),
                                      Flexible(
                                        child: Container(
                                          padding: EdgeInsets.all(0.0),
                                          child: Text(
                                            ': ${widget.selectPersonMap["fullName"]}',
                                            overflow: TextOverflow.ellipsis,
                                            style: new TextStyle(
                                              fontSize: 12,
                                              color: UtilsColors.cris1,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: 79,
                                        child: Text(
                                          "Fecha/Hora ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(0.0),
                                        child: Text(
                                          ': ${widget.fecha}',
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: UtilsColors.cris1,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: 79,
                                        child: Text(
                                          "Estado ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(0.0),
                                        child: Text(
                                          ': ${widget.estado}',
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: UtilsColors.cris1,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: 79,
                                        child: Text(
                                          "Sub estado ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(0.0),
                                        child: Text(
                                          ': ${widget.subestado}',
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: UtilsColors.cris1,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: 79,
                                        child: Text(
                                          "Cargos ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(0.0),
                                        child: Text(
                                          ': ${widget.countCargo}',
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: UtilsColors.cris1,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: 79,
                                        child: Text(
                                          "Evidencias ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(0.0),
                                        child: Text(
                                          ': ${widget.countEvidencias}',
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: UtilsColors.cris1,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 1),
                            Container(
                              height: 115,
                              width: 350,
                              child: (!state.proccessin)
                                  ? Column(
                                      children: [
                                        Text(
                                          widget.countCargo != 0 ||
                                                  widget.countEvidencias != 0
                                              ? "Enviando Imagenes..."
                                              : "Enviando...",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.green),
                                        ),
                                        CircularProgressIndicator(
                                          color: UtilsColors.primary,
                                        )
                                      ],
                                    )
                                  : (state.status)
                                      ? Container(
                                          height: double.infinity,
                                          child: Column(
                                            children: [
                                              Text(
                                                "¡Registrado exitosamente!",
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.green),
                                              ),
                                              SizedBox(height: 10),
                                              SvgPicture.asset(
                                                "assets/svg/check.svg",
                                                height: 79,
                                                width: 79,
                                                color: Colors.green,
                                              )
                                            ],
                                          ),
                                        )
                                      : Container(
                                          height: double.infinity,
                                          child: Column(
                                            children: [
                                              Text(
                                                "¡Error en registro!",
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.red),
                                              ),
                                              SizedBox(height: 10),
                                              SvgPicture.asset(
                                                "assets/svg/error.svg",
                                                height: 79,
                                                width: 79,
                                                color: Colors.red,
                                              )
                                            ],
                                          ),
                                        ),
                            ),
                            Container(
                              child: (!state.proccessin)
                                  ? CircularProgressIndicator(
                                      color: Colors.transparent,
                                    )
                                  : (!state.status)
                                      ? Column(
                                          children: [
                                            // RoundedButton(
                                            //   color: (!state.status)
                                            //       ? Colors.red
                                            //       : UtilsColors.secundary,
                                            //   text: "Enviar resumen otra vez",
                                            //   press: () {
                                            //     sendResumen(context);
                                            //   },
                                            //   loading: false,
                                            // ),
                                            RoundedButton(
                                              color: (!state.status)
                                                  ? UtilsColors.secundary
                                                  : UtilsColors.secundary,
                                              text: "Nueva consulta",
                                              press: () {
                                                Navigator.of(context)
                                                    .pushNamedAndRemoveUntil(
                                                        'Dni',
                                                        (Route<dynamic>
                                                                route) =>
                                                            false);
                                                // Navigator.pushNamed(
                                                //   context,
                                                //   'Consult',
                                                // );
                                              },
                                              loading: false,
                                            ),
                                          ],
                                        )
                                      : Column(
                                          children: [
                                            Text(
                                              "Consulte un nuevo pedido",
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            RoundedButton(
                                              color: UtilsColors.secundary,
                                              text: "Nueva Consulta",
                                              press: () {
                                                Navigator.of(context)
                                                    .pushNamedAndRemoveUntil(
                                                        'Dni',
                                                        (Route<dynamic>
                                                                route) =>
                                                            false);
                                                // Navigator.pushNamed(
                                                //   context,
                                                //   'Consult',
                                                // );
                                              },
                                              loading: false,
                                            )
                                          ],
                                        ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 50,
                        padding: new EdgeInsets.only(top: 0.0, left: 10),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Enotria S.A. Ⓒ2021",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 13, color: UtilsColors.tersary),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                onWillPop: _onBackPressed);
          },
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      child: SvgPicture.asset(
                        "assets/svg/tiempo.svg",
                        // color: ViteColors.primary,
                      ),
                    ),
                    SizedBox(height: 25),
                    Text(
                      "¿Estás seguro?",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: UtilsColors.cris1,
                          fontSize: 12),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        "¿Quieres salir de la aplicación?",
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: UtilsColors.secundary,
                            fontSize: 11),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Row(
                      children: [
                        RoundedButtonDialog(
                          color: UtilsColors.secundary,
                          text: "Aceptar",
                          press: () async {
                            // ignore: unused_local_variable
                            final result = await FlutterRestart.restartApp();
                            //Navigator.of(context).pop(true);
                          },
                          loading: false,
                        ),
                        RoundedButtonDialog(
                          color: UtilsColors.secundary,
                          text: "Cancelar",
                          press: () async {
                            Navigator.of(context).pop(false);
                          },
                          loading: false,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        ) ??
        false;
  }
}
