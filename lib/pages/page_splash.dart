import 'dart:async';
import 'dart:ui';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';

class PageSplash extends StatefulWidget {
  PageSplash({Key key}) : super(key: key);

  @override
  _PageSplashState createState() => _PageSplashState();
}

class _PageSplashState extends State<PageSplash> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 5), () => sendLogin());
  }

  void sendLogin() {
    Navigator.pushReplacementNamed(context, 'Login');
    //'pruebaMethod'
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            child: Center(
              child: ZoomIn(
                delay: Duration(milliseconds: 1700),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: 
                      //Text("Prueba", style: TextStyle(fontSize: 24),)
                      Image.asset(
                        'assets/png/logo.png',
                        width: 600.0,
                        height: 270.0,
                        fit: BoxFit.scaleDown,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
