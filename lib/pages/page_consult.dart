import 'package:dropdown_plus/dropdown_plus.dart';
import 'package:enotria/bloc/bloc_consult/bloc_consult_bloc.dart';
import 'package:enotria/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:enotria/pages/custom/custom.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/userPreferences.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/RoundedInput.dart';
import 'package:enotria/widget/all/menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_restart/flutter_restart.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class PageConsult extends StatefulWidget {
  PageConsult({Key key}) : super(key: key);

  @override
  _PageConsultState createState() => _PageConsultState();
}

class _PageConsultState extends State<PageConsult> with WidgetsBindingObserver {
  BlocConsultBloc _consultBloc = BlocConsultBloc();
  GlobalKey<FormState> _formKey = GlobalKey();
  String _code;
  UserPreferences _pre = new UserPreferences();

  // ignore: unused_field
  DateTime _salida;
  // ignore: unused_field
  DateTime _entrada;
  // ignore: unused_field
  int _timeInactive;

  sendResumenFinalize(BlocConsultState state) {
    if (state.deliveryCode == "") {
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: "Busque Una Entrega ",
        ),
      );
    } else if (state.selectPersonMap == null) {
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: "Seleccione Cliente",
        ),
      );
    } else if (state.selectAddressMap == null) {
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: "Seleccione Dirección",
        ),
      );
    } else if (state.deliveryCode != "" &&
        state.selectPersonMap != null &&
        state.selectAddressMap != null) {
      _consultBloc.add(
        SendResumenFinalize(
          context: context,
          code: _code,
          productName: state.productName,
          productCode: state.productCode,
          enterpriseName: state.enterpriseName,
          deliveryCode: state.deliveryCode,
          selectAddressMap: state.selectAddressMap,
          selectPersonMap: state.selectPersonMap,
        ),
      );
    }
  }

  @override
  void initState() {
    //timeOuthSession();
    super.initState();
    _timeInactive = int.parse(_pre.downtime);
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    _consultBloc.close();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final MediaQueryData media = MediaQuery.of(context);

    final Size size = media.size;
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    // ignore: unused_local_variable
    final _blocLogin = BlocProvider.of<BlocLoginBloc>(context);

    return BlocProvider.value(
      value: _consultBloc,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: UtilsColors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: UtilsColors.primary,
          centerTitle: true,
          title: Text(
            "DESPACHO",
            style: TextStyle(color: UtilsColors.white),
          ),
          leading: Theme(
            data: Theme.of(context).copyWith(
              canvasColor: UtilsColors.cris1,
            ),
            child: IconButton(
              icon: Icon(
                Icons.more_vert,
                size: 40,
                color: Colors.white,
              ),
              onPressed: () => _scaffoldKey.currentState.openDrawer(),
            ),
          ),
        ),
        drawer: DrawerUser(),
        body: BlocBuilder<BlocConsultBloc, BlocConsultState>(
          builder: (_, state) {
            return WillPopScope(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        height: 1100,
                        child: Column(
                          children: [
                            SizedBox(height: 5),
                            Form(
                              key: _formKey,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                      vertical: 0,
                                      horizontal: 20,
                                    ),
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 15,
                                      vertical: 0,
                                    ),
                                    width: size.width * 0.10,
                                    decoration: BoxDecoration(
                                      color: UtilsColors.cris0,
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        RoundedInputSearch(
                                          read: false,
                                          hintText: "Pedido:",
                                          length: 15,
                                          keyboardType: TextInputType.text,
                                          icon: Icons.person,
                                          validator: (value) {
                                            this._code = value;
                                            if (value != "" &&
                                                value.length <= 15) {
                                              return null;
                                            }
                                            return 'Código incorrecto.';

                                            // return value != ""
                                            //     ? null
                                            //     : 'Código  requerido.';
                                          },
                                          onChanged: (value) {
                                            //controllerDemo.text = value.toUpperCase();
                                            print(value.toUpperCase());
                                          },
                                        ),
                                        Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 0),
                                          child: RoundedButtonSearch(
                                            icon: Icons.search,
                                            color: UtilsColors.secundary,
                                            text: "search",
                                            press: () {
                                              if (_formKey.currentState
                                                  .validate()) {
                                                state.selectAddressMap = null;
                                                state.selectPersonMap = null;

                                                if (_code.substring(0, 3) !=
                                                        "PED" &&
                                                    _code.length >= 16) {
                                                  showTopSnackBar(
                                                    context,
                                                    CustomSnackBar.error(
                                                      message:
                                                          "Código Incorrecto.",
                                                    ),
                                                  );
                                                } else {
                                                  _consultBloc.add(
                                                    GetOrder(
                                                        context: context,
                                                        code: _code),
                                                  );
                                                }
                                              }
                                            },
                                            loading: state.search,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  RoundedInputRead(
                                    read: true,
                                    title: "Código de Entrega:",
                                    hintText: (state.search)
                                        ? state.deliveryCode = ""
                                        : state.deliveryCode,
                                    keyboardType: TextInputType.emailAddress,
                                    icon: Icons.qr_code,
                                  ),
                                  RoundedInputRead(
                                    read: true,
                                    title: "Empresa:",
                                    hintText: (state.search)
                                        ? state.enterpriseName = ""
                                        : state.enterpriseName,
                                    keyboardType: TextInputType.emailAddress,
                                    icon: Icons.house,
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                      vertical: 1,
                                      horizontal: 10,
                                    ),
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 10,
                                      vertical: 0,
                                    ),
                                    width: size.width * 0.9,
                                    decoration: BoxDecoration(
                                      color: UtilsColors.cris0,
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Nombre completo de cliente:",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: UtilsColors.cris1,
                                              fontSize: 12),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              child: Icon(Icons.person,
                                                  color: UtilsColors.primary),
                                            ),
                                            Expanded(
                                              child: DropdownFormField<
                                                  Map<String, dynamic>>(
                                                emptyText: "",
                                                emptyActionText: "",
                                                onEmptyActionPressed:
                                                    () async {},
                                                decoration: CustomInputs
                                                    .formInputDecoration(
                                                        labelText: ''),
                                                onSaved: (dynamic str) {},
                                                onChanged: (dynamic str) {
                                                  state.selectPersonMap = str;

                                                  print(state.selectPersonMap);

                                                  state.selectPerson =
                                                      str["fullName"];
                                                  state.document =
                                                      str["documentId"];
                                                },
                                                displayItemFn: (dynamic item) {
                                                  return Container(
                                                    padding:
                                                        EdgeInsets.all(0.0),
                                                    child: Text(
                                                      (state.search)
                                                          ? state.selectPerson =
                                                              "Seleccionar"
                                                          : state.selectPerson
                                                              .toString(),
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                        fontSize: 12,
                                                        color:
                                                            UtilsColors.cris1,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                      ),
                                                    ),
                                                  );
                                                },
                                                findFn: (dynamic str) async =>
                                                    state.personList,
                                                filterFn: (dynamic item, str) =>
                                                    item['fullName']
                                                        .toLowerCase()
                                                        .indexOf(str
                                                            .toLowerCase()) >=
                                                    0,
                                                dropdownItemFn: (dynamic item,
                                                        position,
                                                        focused,
                                                        dynamic
                                                            lastSelectedItem,
                                                        onTap) =>
                                                    (position == 0)
                                                        ? ListTile(
                                                            title: Text(
                                                              item['fullName'],
                                                              style: TextStyle(
                                                                  fontSize: 13,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                            subtitle: Text(
                                                                "${item['documentType']}: ${item['documentId']}" ??
                                                                    '',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        12,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold)),
                                                            tileColor: focused
                                                                ? Color
                                                                    .fromARGB(
                                                                        20,
                                                                        0,
                                                                        0,
                                                                        0)
                                                                : Colors
                                                                    .transparent,
                                                            onTap: onTap,
                                                          )
                                                        : ListTile(
                                                            title: Text(
                                                              item['fullName'],
                                                              style: TextStyle(
                                                                  fontSize: 13),
                                                            ),
                                                            subtitle: Text(
                                                                "${item['documentType']}: ${item['documentId']}" ??
                                                                    '',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        12)),
                                                            tileColor: focused
                                                                ? Color
                                                                    .fromARGB(
                                                                        20,
                                                                        0,
                                                                        0,
                                                                        0)
                                                                : Colors
                                                                    .transparent,
                                                            onTap: onTap,
                                                          ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  RoundedInputRead(
                                    read: true,
                                    title: "Documento de Identidad:",
                                    hintText: (state.search)
                                        ? state.document = ""
                                        : state.document,
                                    keyboardType: TextInputType.emailAddress,
                                    icon: Icons.document_scanner,
                                  ),
                                  RoundedInputRead(
                                    read: true,
                                    title: "Nombre del producto:",
                                    hintText: (!state.search)
                                        ? state.productName
                                        : "",
                                    keyboardType: TextInputType.emailAddress,
                                    icon: Icons.production_quantity_limits,
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                      vertical: 1,
                                      horizontal: 10,
                                    ),
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 10,
                                      vertical: 0,
                                    ),
                                    width: size.width * 0.8,
                                    decoration: BoxDecoration(
                                      color: UtilsColors.cris0,
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Direcciones:",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: UtilsColors.cris1,
                                              fontSize: 12),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              child: Icon(Icons.place,
                                                  color: UtilsColors.primary),
                                            ),
                                            Expanded(
                                              child: DropdownFormField<
                                                  Map<String, dynamic>>(
                                                emptyText: "",
                                                emptyActionText: "",
                                                onEmptyActionPressed:
                                                    () async {},
                                                decoration: CustomInputs
                                                    .formInputDecoration(
                                                        labelText: ''),
                                                onSaved: (dynamic str) {},
                                                onChanged: (dynamic str) {
                                                  state.selectAddressMap = str;
                                                  print(state.selectAddressMap);
                                                  state.selectAddress =
                                                      str["address"];
                                                },
                                                displayItemFn: (dynamic item) {
                                                  return Container(
                                                    padding:
                                                        EdgeInsets.all(0.0),
                                                    child: Text(
                                                      (state.search)
                                                          ? state.selectAddress =
                                                              "Seleccionar"
                                                          : state.selectAddress
                                                              .toString(),
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                        fontSize: 12,
                                                        color:
                                                            UtilsColors.cris1,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                      ),
                                                    ),
                                                  );
                                                },
                                                findFn: (dynamic str) async =>
                                                    state.addressList,
                                                filterFn: (dynamic item, str) =>
                                                    item['address']
                                                        .toLowerCase()
                                                        .indexOf(str
                                                            .toLowerCase()) >=
                                                    0,
                                                dropdownItemFn: (dynamic item,
                                                        position,
                                                        focused,
                                                        dynamic
                                                            lastSelectedItem,
                                                        onTap) =>
                                                    (position == 0)
                                                        ? ListTile(
                                                            title: Text(
                                                              item['address'],
                                                              style: TextStyle(
                                                                  fontSize: 13,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                            subtitle: Text(
                                                              "${item['districtName']} - ${item['provinceName']} -${item['departmentName']}" ??
                                                                  '',
                                                              style: TextStyle(
                                                                  fontSize: 12,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                            tileColor: focused
                                                                ? Color
                                                                    .fromARGB(
                                                                        20,
                                                                        0,
                                                                        0,
                                                                        0)
                                                                : Colors
                                                                    .transparent,
                                                            onTap: onTap,
                                                          )
                                                        : ListTile(
                                                            title: Text(
                                                              item['address'],
                                                              style: TextStyle(
                                                                  fontSize: 13),
                                                            ),
                                                            subtitle: Text(
                                                              "${item['districtName']} - ${item['provinceName']} -${item['departmentName']}" ??
                                                                  '',
                                                              style: TextStyle(
                                                                  fontSize: 12),
                                                            ),
                                                            tileColor: focused
                                                                ? Color
                                                                    .fromARGB(
                                                                        20,
                                                                        0,
                                                                        0,
                                                                        0)
                                                                : Colors
                                                                    .transparent,
                                                            onTap: onTap,
                                                          ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 20),
                                    child: RoundedButton(
                                      color: UtilsColors.secundary,
                                      text: "Continuar",
                                      press: () {
                                        sendResumenFinalize(state);
                                      },
                                      loading: false,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 50,
                        padding: new EdgeInsets.only(top: 0.0, left: 10),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Enotria S.A. Ⓒ2021",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 13, color: UtilsColors.tersary),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                onWillPop: _onBackPressed);
          },
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      child: SvgPicture.asset(
                        "assets/svg/tiempo.svg",
                        // color: ViteColors.primary,
                      ),
                    ),
                    SizedBox(height: 25),
                    Text(
                      "¿Estás seguro?",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: UtilsColors.cris1,
                          fontSize: 12),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        "¿Quieres salir de la aplicación?",
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: UtilsColors.secundary,
                            fontSize: 11),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Row(
                      children: [
                        RoundedButtonDialog(
                          color: UtilsColors.secundary,
                          text: "Aceptar",
                          press: () async {
                            // ignore: unused_local_variable
                            final result = await FlutterRestart.restartApp();
                            //Navigator.of(context).pop(true);
                          },
                          loading: false,
                        ),
                        RoundedButtonDialog(
                          color: UtilsColors.secundary,
                          text: "Cancelar",
                          press: () async {
                            Navigator.of(context).pop(false);
                          },
                          loading: false,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        ) ??
        false;
  }
}
