import 'package:enotria/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/constast.dart';
import 'package:enotria/util/userPreferences.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/RoundedInput.dart';
import 'package:enotria/widget/all/RoundedPassword.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_mac/get_mac.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:yaml/yaml.dart';

class PageLogin extends StatefulWidget {
  PageLogin({Key key}) : super(key: key);

  @override
  _PageLoginState createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  BlocLoginBloc _loginBloc = BlocLoginBloc();
  GlobalKey<FormState> _formKey = GlobalKey();
  UserPreferences _pre = UserPreferences();
  // ignore: unused_field
  String _user;
  // ignore: unused_field
  String _password;
  String _mac;
  String _version;

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    String platformVersion;

    try {
      //obtener la mac del equipo
      platformVersion = await GetMac.macAddress;
    } on PlatformException {
      platformVersion = 'error';
    }

    if (!mounted) return;

    setState(() {
      _mac = platformVersion;
      //guardar en las preferencias
      _pre.mac = _mac;
    });
  }

  @override
  Widget build(BuildContext context) {
    final MediaQueryData media = MediaQuery.of(context);
    // ignore: unused_local_variable
    final Size size = media.size;
    // ignore: unused_local_variable
    final EdgeInsets padding = media.padding;

    return BlocProvider.value(
      value: _loginBloc,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            _mac == 'error' ? 'MAC no disponible.' : "",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.bold,
                color: UtilsColors.white),
          ),
        ),
        backgroundColor: UtilsColors.white,
        body: BlocBuilder<BlocLoginBloc, BlocLoginState>(
          builder: (_, state) {
            return Stack(
              children: <Widget>[
                SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: [
                              Bienvenida(),
                              SizedBox(height: 20),
                              Form(
                                key: _formKey,
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    RoundedInput(
                                      hintText: "Usuario",
                                      keyboardType: TextInputType.emailAddress,
                                      icon: Icons.person,
                                      validator: (value) {
                                        print('value');
                                        this._user = value;
                                        return value != ""
                                            ? null
                                            : 'El usuario es obligatorio.';
                                      },
                                    ),
                                    RoundedPassword(
                                      hintText: "Contraseña",
                                      icon: Icons.lock,
                                      sufixicon: true,
                                      validator: (value) {
                                        this._password = value;
                                        return value.length >= 5
                                            ? null
                                            : 'La contraseña es obligatoria';
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 20),
                                child: RoundedButton(
                                  color: UtilsColors.secundary,
                                  text: "Acceder",
                                  press: () {
                                    if (_formKey.currentState.validate()) {
                                      _loginBloc.add(LoginUser(
                                          context: context,
                                          user: _user,
                                          password: _password));
                                    }
                                  },
                                  loading: state.proccessin ?? false,
                                ),
                              ),
                              SizedBox(height: 30),
                              Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.lock,
                                      color: UtilsColors.tersary,
                                    ),
                                    RichText(
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                            text: '¿Olvidaste tu contraseña?',
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                Navigator.pushNamed(
                                                    context, 'Write_User');
                                              },
                                            style: TextStyle(
                                              color: UtilsColors.tersary,
                                              fontSize: 15,
                                              letterSpacing: 0.5,
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                        child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: "Enotria S.A. Ⓒ2021  v${appversion.toString()}",
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                showTopSnackBar(
                                  context,
                                  CustomSnackBar.info(
                                    message: _mac,
                                  ),
                                );
                              },
                            style: TextStyle(
                              color: UtilsColors.tersary,
                              fontSize: 13,
                              letterSpacing: 0.1,
                            ),
                          )
                        ],
                      ),
                    ))
                  
                  // FutureBuilder(
                  //   future: rootBundle.loadString("pubspec.yaml"),
                  //   builder: (context, snapshot) {
                  //     if (snapshot.hasData) {
                  //       var yaml = loadYaml(snapshot.data);
                  //       _pre.app = yaml["desarrollo"];
                  //       _version = yaml["appversion"];
                  //     }

                  //     return Container(
                  //         child: RichText(
                  //       text: TextSpan(
                  //         children: [
                  //           TextSpan(
                  //             text:
                  //                 "Enotria S.A. Ⓒ2021  v${_version.toString()}",
                  //             recognizer: TapGestureRecognizer()
                  //               ..onTap = () {
                  //                 showTopSnackBar(
                  //                   context,
                  //                   CustomSnackBar.info(
                  //                     message: _mac,
                  //                   ),
                  //                 );
                  //               },
                  //             style: TextStyle(
                  //               color: UtilsColors.tersary,
                  //               fontSize: 13,
                  //               letterSpacing: 0.1,
                  //             ),
                  //           )
                  //         ],
                  //       ),
                  //     ));
                  //   },
                  // ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

class Bienvenida extends StatelessWidget {
  const Bienvenida({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Image.asset(
            'assets/png/logo.png',
            width: 600.0,
            height: 150.0,
            fit: BoxFit.scaleDown,
          ),
        ),
        Text(
          "¡Bienvenido!",
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              fontFamily: "Cocon",
              letterSpacing: 1),
          textAlign: TextAlign.center,
        ),
        Text(
          "Iniciar sesión",
          textAlign: TextAlign.center,
          style: TextStyle(fontFamily: "Gelion", fontSize: 15),
        ),
      ],
    );
  }
}
