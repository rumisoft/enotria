import 'package:dropdown_plus/dropdown_plus.dart';
import 'package:enotria/bloc/bloc_dni_consult/bloc_dni_consult_bloc.dart';
import 'package:enotria/bloc/bloc_resumen/resumen_bloc.dart';
import 'package:enotria/model/modelDni.dart';
import 'package:enotria/pages/custom/custom.dart';
import 'package:enotria/pages/page_resumen.dart';
import 'package:enotria/pages/validacionesBiometria/hitPage.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/responsive.dart';
import 'package:enotria/util/userPreferences.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:enotria/widget/all/RoundedInput.dart';
import 'package:enotria/widget/all/menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class PageDniConsult extends StatefulWidget {
  final Datum data;
  PageDniConsult({Key key, this.data}) : super(key: key);

  @override
  _PageDniConsultState createState() => _PageDniConsultState();
}

class _PageDniConsultState extends State<PageDniConsult>
    with WidgetsBindingObserver {
  BlocDniConsultBloc _bloc = BlocDniConsultBloc();
  ResumenBloc _resumenbloc = new ResumenBloc();

  GlobalKey<FormState> _formKey = GlobalKey();
  // ignore: unused_field
  String _code;
  // ignore: unused_field
  Datum _data;

  UserPreferences _pre = new UserPreferences();
  // ignore: unused_field
  DateTime _salida;
  // ignore: unused_field
  DateTime _entrada;
  // ignore: unused_field
  int _timeInactive;

  // final bioModel = ValidacionBioModel();

  sendResumenFinalize(BlocDniConsultState state) {
    if (state.selectPersonMap == null) {
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: "Seleccione Cliente",
        ),
      );
    } else if (state.selectAddressMap == null) {
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: "Seleccione Dirección",
        ),
      );
    } else if (state.selectPersonMap != null &&
        state.selectAddressMap != null) {
      final route = MaterialPageRoute(
          builder: (BuildContext _) => PageResumen(
                type: "REGISTRO",
                productName: state.data.product.productName,
                productCode: state.data.product.productCode,
                enterpriseName: state.data.enterprise.enterpriseName,
                deliveryCode: state.data.deliveryCode,
                code: state.data.deliveryCode,
                selectAddressMap: state.selectAddressMap,
                selectPersonMap: state.selectPersonMap,
                cargo: state.cargo,
                evidencia: state.evidencia,
              ));

      Navigator.push(context, route);
    }
  }

  init() {
    _bloc.add(InitData(data: widget.data));
  }

  @override
  void initState() {
    init();
    super.initState();
    _timeInactive = int.parse(_pre.downtime);
    //_bloc.timeOperation();
    WidgetsBinding.instance.addObserver(this);

    // Estados SubEstados
    _resumenbloc.add(GetEstadoSubestados(id: widget.data.product.productCode));
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final MediaQueryData media = MediaQuery.of(context);
    final responsive = Responsive.of(context);

    final Size size = media.size;
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: UtilsColors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: UtilsColors.primary,
        centerTitle: true,
        title: Text(
          "DESPACHO RESUMEN",
          style: TextStyle(color: UtilsColors.white),
        ),
        leading: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: UtilsColors.cris1,
          ),
          child: IconButton(
            icon: Icon(
              Icons.more_vert,
              size: 40,
              color: Colors.white,
            ),
            onPressed: () => _scaffoldKey.currentState.openDrawer(),
          ),
        ),
      ),
      drawer: DrawerUser(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Column(
              children: [
                SizedBox(height: 5),
                BlocProvider.value(
                  value: _bloc,
                  child: BlocBuilder<BlocDniConsultBloc, BlocDniConsultState>(
                      builder: (_, state) {
                    return Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(
                                vertical: 0,
                                horizontal: 20,
                              ),
                              padding: EdgeInsets.symmetric(
                                horizontal: 15,
                                vertical: 0,
                              ),
                              width: size.width * 0.10,
                              decoration: BoxDecoration(
                                color: UtilsColors.cris0,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  //        RoundedInputRead(
                                  //   read: true,
                                  //   title: "Código de Entrega:",
                                  //   hintText:hintText:(!state.load)?" ": "${state.data.deliveryCode}",
                                  //   keyboardType: TextInputType.emailAddress,
                                  //   icon: Icons.qr_code,
                                  // ),
                                  RoundedInputSearch(
                                    read: true,
                                    hintText: (!state.load)
                                        ? " "
                                        : "${state.data.deliveryCode}",
                                    length: 7,
                                    keyboardType: TextInputType.text,
                                    icon: Icons.person,
                                    validator: (value) {
                                      this._code = value;
                                      if (value != "" && value.length <= 15) {
                                        return null;
                                      }
                                      return 'Código Incorrecto.';

                                      // return value != ""
                                      //     ? null
                                      //     : 'Código  requerido.';
                                    },
                                  ),
                                  // Container(
                                  //   margin:
                                  //       EdgeInsets.symmetric(horizontal: 0),
                                  //   child: RoundedButtonSearch(
                                  //     color: UtilsColors.secundary,
                                  //     text: "search",
                                  //     press: () {
                                  //       if (_formKey.currentState
                                  //           .validate()) {

                                  //       }
                                  //     },
                                  //     loading: false,
                                  //   ),
                                  // )
                                ],
                              ),
                            ),
                            SizedBox(height: 10),
                            RoundedInputRead(
                              read: true,
                              title: "Código de Entrega:",
                              hintText: (!state.load)
                                  ? " "
                                  : "${state.data.deliveryCode}",
                              keyboardType: TextInputType.emailAddress,
                              icon: Icons.qr_code,
                            ),
                            RoundedInputRead(
                              read: true,
                              title: "Empresa:",
                              hintText: (!state.load)
                                  ? " "
                                  : "${state.data.enterprise.enterpriseName}",
                              keyboardType: TextInputType.emailAddress,
                              icon: Icons.house,
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(
                                vertical: 1,
                                horizontal: 10,
                              ),
                              padding: EdgeInsets.symmetric(
                                horizontal: 10,
                                vertical: 0,
                              ),
                              width: size.width * 0.9,
                              decoration: BoxDecoration(
                                color: UtilsColors.cris0,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Nombre completo de cliente:",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: UtilsColors.cris1,
                                        fontSize: 12),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        child: Icon(Icons.person,
                                            color: UtilsColors.primary),
                                      ),
                                      Expanded(
                                        child: DropdownFormField<
                                            Map<String, dynamic>>(
                                          emptyText: "",
                                          emptyActionText: "",
                                          onEmptyActionPressed: () async {},
                                          decoration:
                                              CustomInputs.formInputDecoration(
                                                  labelText: ''),
                                          onSaved: (dynamic str) {},
                                          onChanged: (dynamic str) {
                                            state.selectPersonMap = str;

                                            print(state.selectPersonMap);

                                            state.selectPerson =
                                                str["fullName"];
                                            state.document = str["documentId"];
                                          },
                                          displayItemFn: (dynamic item) {
                                            return Container(
                                              padding: EdgeInsets.all(0.0),
                                              child: Text(
                                                state.selectPerson.toString(),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: UtilsColors.cris1,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                            );
                                          },
                                          findFn: (dynamic str) async =>
                                              state.deliveryPerson,
                                          filterFn: (dynamic item, str) =>
                                              item['fullName']
                                                  .toLowerCase()
                                                  .indexOf(str.toLowerCase()) >=
                                              0,
                                          dropdownItemFn: (dynamic item,
                                                  position,
                                                  focused,
                                                  dynamic lastSelectedItem,
                                                  onTap) =>
                                              (position == 0)
                                                  ? ListTile(
                                                      title: Text(
                                                        item['fullName'],
                                                        style: TextStyle(
                                                            fontSize: 13,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      subtitle: Text(
                                                          "${item['documentType']}: ${item['documentId']}" ??
                                                              '',
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                      tileColor: focused
                                                          ? Color.fromARGB(
                                                              20, 0, 0, 0)
                                                          : Colors.transparent,
                                                      onTap: onTap,
                                                    )
                                                  : ListTile(
                                                      title: Text(
                                                        item['fullName'],
                                                        style: TextStyle(
                                                            fontSize: 13),
                                                      ),
                                                      subtitle: Text(
                                                          "${item['documentType']}: ${item['documentId']}" ??
                                                              '',
                                                          style: TextStyle(
                                                              fontSize: 12)),
                                                      tileColor: focused
                                                          ? Color.fromARGB(
                                                              20, 0, 0, 0)
                                                          : Colors.transparent,
                                                      onTap: onTap,
                                                    ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            RoundedInputRead(
                              read: true,
                              title: "Documento de Identidad:",
                              hintText: state.document,
                              keyboardType: TextInputType.emailAddress,
                              icon: Icons.document_scanner,
                            ),
                            RoundedInputRead(
                              read: true,
                              title: "Nombre del producto:",
                              hintText: (!state.load)
                                  ? " "
                                  : "${state.data.product.productName}",
                              keyboardType: TextInputType.emailAddress,
                              icon: Icons.production_quantity_limits,
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(
                                vertical: 1,
                                horizontal: 10,
                              ),
                              padding: EdgeInsets.symmetric(
                                horizontal: 10,
                                vertical: 0,
                              ),
                              width: size.width * 0.8,
                              decoration: BoxDecoration(
                                color: UtilsColors.cris0,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Direcciones:",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: UtilsColors.cris1,
                                        fontSize: 12),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        child: Icon(Icons.place,
                                            color: UtilsColors.primary),
                                      ),
                                      Expanded(
                                        child: DropdownFormField<
                                            Map<String, dynamic>>(
                                          emptyText: "",
                                          emptyActionText: "",
                                          onEmptyActionPressed: () async {},
                                          decoration:
                                              CustomInputs.formInputDecoration(
                                                  labelText: ''),
                                          onSaved: (dynamic str) {},
                                          onChanged: (dynamic str) {
                                            state.selectAddressMap = str;
                                            print(state.selectAddressMap);
                                            //direccion
                                            state.selectAddress = str["address"];
                                          },
                                          displayItemFn: (dynamic item) {
                                            return Container(
                                              padding: EdgeInsets.all(0.0),
                                              child: Text(
                                                state.selectAddress.toString(),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: UtilsColors.cris1,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                            );
                                          },
                                          findFn: (dynamic str) async =>
                                              state.deliveryAddress,
                                          filterFn: (dynamic item, str) =>
                                              item['address']
                                                  .toLowerCase()
                                                  .indexOf(str.toLowerCase()) >=
                                              0,
                                          dropdownItemFn: (dynamic item,
                                                  position,
                                                  focused,
                                                  dynamic lastSelectedItem,
                                                  onTap) =>
                                              (position == 0)
                                                  ? ListTile(
                                                      title: Text(
                                                        item['address'],
                                                        style: TextStyle(
                                                            fontSize: 13,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      subtitle: Text(
                                                        "${item['districtName']} - ${item['provinceName']} -${item['departmentName']}" ??
                                                            '',
                                                        style: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      tileColor: focused
                                                          ? Color.fromARGB(
                                                              20, 0, 0, 0)
                                                          : Colors.transparent,
                                                      onTap: onTap,
                                                    )
                                                  : ListTile(
                                                      title: Text(
                                                        item['address'],
                                                        style: TextStyle(
                                                            fontSize: 13),
                                                      ),
                                                      subtitle: Text(
                                                        "${item['districtName']} - ${item['provinceName']} -${item['departmentName']}" ??
                                                            '',
                                                        style: TextStyle(
                                                            fontSize: 12),
                                                      ),
                                                      tileColor: focused
                                                          ? Color.fromARGB(
                                                              20, 0, 0, 0)
                                                          : Colors.transparent,
                                                      onTap: onTap,
                                                    ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 5),
                        
                          ],
                        ));
                  }),
                ),
                BlocProvider.value(
                  value: _resumenbloc,
                  child: BlocBuilder<ResumenBloc, ResumenState>(
                    builder: (context, state) {
                      if (state.validacionBio == null) {
                        return Center(
                          child: Column(
                            children: [
                              SizedBox(height: responsive.hp(2)),
                              CupertinoActivityIndicator(),
                            ],
                          ),
                        );
                      }

                      if (state.validacionBio.tipoBiometria.trim() ==
                              "Biometría Dactilar" &&
                          state.validacionBio.saltoBiometria == false) {
                        //solo boton de biometria
                        return Column(
                          children: [
                            SizedBox(height: responsive.hp(3)),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: RoundedButton(
                                color: UtilsColors.secundary,
                                text: "Biometria",
                                press: () {
                                  print('hacia datos hit---------');
                                  if (_bloc.state.selectPersonMap == null) {
                                    showTopSnackBar(
                                      context,
                                      CustomSnackBar.info(
                                        message: "Seleccione Cliente",
                                      ),
                                    );
                                  } else if (_bloc.state.selectAddressMap ==
                                      null) {
                                    showTopSnackBar(
                                      context,
                                      CustomSnackBar.info(
                                        message: "Seleccione Dirección",
                                      ),
                                    );
                                  } else {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (BuildContext _) =>
                                                HitBiometriaPage(
                                                  //type: "REGISTRO",
                                                  valbio: state.validacionBio,
                                                  productName: _bloc.state.data
                                                      .product.productName,
                                                  productCode: _bloc.state.data
                                                      .product.productCode,
                                                  enterpriseName: _bloc
                                                      .state
                                                      .data
                                                      .enterprise
                                                      .enterpriseName,
                                                  deliveryCode: _bloc
                                                      .state.data.deliveryCode,
                                                  code: _bloc
                                                      .state.data.deliveryCode,
                                                  selectAddressMap: _bloc
                                                      .state.selectAddressMap,
                                                  selectPersonMap: _bloc
                                                      .state.selectPersonMap,
                                                  cargo: _bloc.state.cargo,
                                                  evidencia:
                                                      _bloc.state.evidencia,
                                                  estados: state.estadosList[0],
                                                  subestados:
                                                      state.estadosList[0],
                                                )));
                                  }
                                },
                                loading: false,
                                icon: "assets/svg/huella.svg",
                              ),
                            ),
                          ],
                        );
                      } else if (state.validacionBio.tipoBiometria.trim() ==
                              "Biometría Dactilar" &&
                          state.validacionBio.saltoBiometria == true) {
                        //ambos btones
                        return Column(
                          children: [
                            SizedBox(height: responsive.hp(3)),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: RoundedButton(
                                color: UtilsColors.secundary,
                                text: "Biometria",
                                press: () {
                                  //print('hacia datos hit---------');
                                  if (_bloc.state.selectPersonMap == null) {
                                    showTopSnackBar(
                                      context,
                                      CustomSnackBar.info(
                                        message: "Seleccione Cliente",
                                      ),
                                    );
                                  } else if (_bloc.state.selectAddressMap ==
                                      null) {
                                    showTopSnackBar(
                                      context,
                                      CustomSnackBar.info(
                                        message: "Seleccione Dirección",
                                      ),
                                    );
                                  } else {
                                    //obtener la hora actual para llamar a la api de mejor huella
                                    DateTime horaConsultaApi = DateTime.now();
                                    String apiType =
                                        "Consulta Verificacion de mejor huella";
                                    

                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext _) =>
                                            HitBiometriaPage(
                                          //type: "REGISTRO",
                                          valbio: state.validacionBio,
                                          productName: _bloc
                                              .state.data.product.productName,
                                          productCode: _bloc
                                              .state.data.product.productCode,
                                          enterpriseName: _bloc.state.data
                                              .enterprise.enterpriseName,
                                          deliveryCode:
                                              _bloc.state.data.deliveryCode,
                                          code: _bloc.state.data.deliveryCode,
                                          selectAddressMap:
                                              _bloc.state.selectAddressMap,
                                          selectPersonMap:
                                              _bloc.state.selectPersonMap,
                                          cargo: _bloc.state.cargo,
                                          evidencia: _bloc.state.evidencia,
                                          estados: state.estadosList[0],
                                          subestados: state.estadosList[0],
                                          horaConsultaApi:
                                              horaConsultaApi.toString(),
                                          apiType: apiType,
                                        ),
                                      ),
                                    );
                                  }
                                },
                                loading: false,
                                icon: "assets/svg/huella.svg",
                              ),
                            ),
                            // SizedBox(height: responsive.hp(1)),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: RoundedButton(
                                color: Colors.white,
                                textColor: Colors.black,
                                text: "Saltar Biometria >>",
                                press: () async {
                                  if (_bloc.state.selectPersonMap == null) {
                                    showTopSnackBar(
                                      context,
                                      CustomSnackBar.info(
                                        message: "Seleccione Cliente",
                                      ),
                                    );
                                  } else if (_bloc.state.selectAddressMap ==
                                      null) {
                                    showTopSnackBar(
                                      context,
                                      CustomSnackBar.info(
                                        message: "Seleccione Dirección",
                                      ),
                                    );
                                  } else {
                                    await _onBackPressed(_bloc.state);
                                  }
                                },
                                loading: false,
                              ),
                            ),
                          ],
                        );
                      } else {
                        return Column(
                          children: [
                            SizedBox(height: responsive.hp(3)),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: RoundedButton(
                                color: UtilsColors.secundary,
                                text: "Continuar",
                                press: () {
                                  sendResumenFinalize(_bloc.state);
                                },
                                loading: false,
                              ),
                            ),
                          ],
                        );
                      }
                    },
                  ),
                )
              ],
            ),
            SizedBox(height: responsive.hp(3)),
            PiePagina()
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed(BlocDniConsultState state) {
    final responsive = Responsive.of(context);

    return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "¿Está seguro que NO desea realizar Biometría?",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: UtilsColors.cris1,
                          fontSize: 12),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        RoundedButtonDialog2(
                          color: UtilsColors.secundary,
                          text: "SI",
                          press: () async {
                            Navigator.pop(context);
                            sendResumenFinalize(state);
                          },
                          loading: false,
                        ),
                        SizedBox(
                          width: responsive.wp(1.5),
                        ),
                        RoundedButtonDialog2(
                          color: UtilsColors.secundary,
                          text: "NO",
                          press: () {
                            Navigator.pop(context);
                          },
                          loading: false,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        ) ??
        false;
  }
}
