import 'dart:convert';
import 'dart:io' as Io;
import 'dart:io';

import 'package:dropdown_plus/dropdown_plus.dart';
import 'package:enotria/bloc/bloc_resumen/resumen_bloc.dart';
import 'package:enotria/model/EstadoProductoModel.dart';
import 'package:enotria/pages/custom/custom.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/userPreferences.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:collection/collection.dart';

class PageResumen extends StatefulWidget {
  final String type;
  final String productName;
  final String productCode;
  final String enterpriseName;
  final String deliveryCode;
  final String code;
  final Map<String, dynamic> selectAddressMap;
  final Map<String, dynamic> selectPersonMap;
  final List<String> cargo;
  final List<String> evidencia;
  final Map estados;
  final Map subestados;
  final ValidacionBioModel valbio;
  final String op;

  PageResumen(
      {Key key,
      this.type = "REGISTRO",
      this.productName,
      this.productCode,
      this.enterpriseName,
      this.deliveryCode,
      this.code,
      this.selectAddressMap,
      this.selectPersonMap,
      this.cargo,
      this.evidencia,
      this.estados,
      this.subestados,
      this.valbio,
      this.op})
      : super(key: key);

  @override
  _PageResumenState createState() => _PageResumenState();
}

class _PageResumenState extends State<PageResumen> with WidgetsBindingObserver {
  GlobalKey<FormState> _formKey = GlobalKey();

  // ignore: unused_field
  //List<Map<String, dynamic>> _subestado2 = [];

  ResumenBloc _bloc = new ResumenBloc();

  String _datetime, currentLocation, id;

  Position position;

  String select1 = "", select2 = "", cod1 = "", cod2 = "";

  UserPreferences _pre = new UserPreferences();

  // ignore: unused_field
  DateTime _salida;

  // ignore: unused_field
  DateTime _entrada;

  // ignore: unused_field
  int _timeInactive;

  List<String> _cargoExists = [];

  List<String> _evidenciaExists = [];

  Map _estado;

  Map _subestado;

  int _canEstados = 0;

  int _canSubEstados = 0;

//  nuevo camara

  final List<FilesPhoto> _cargos = <FilesPhoto>[];

  final List<FilesPhoto> _evidencia = <FilesPhoto>[];

  final ImagePicker _picker = ImagePicker();

  // ignore: unused_field
  String _typeProcess;
  String estadoNoHit = '';
  String idEstNoHit = '';
  String subEstNoHit = '';
  String idSubEstNoHit = '';

  Future<void> _openCamera(int cameraPosition, ImageSource source) async {
    try {
      final pickedFile = await _picker.pickImage(
        source: source,
      );
      final data =
          _cargos.firstWhereOrNull((element) => element.id == cameraPosition);
      File fileImage = await compressFile(File(pickedFile.path));

      print('peso antes ${File(pickedFile.path).lengthSync()}');
      print("comrpimido ${fileImage.lengthSync()}");

      if (data != null) {
        final bytes = await fileImage.readAsBytes();
        //convertir a base
        final baseimage64 = base64Encode(bytes);
        data.img = fileImage;
        data.b64 = baseimage64;
      } else {}

      setState(() {});
    } catch (e) {
      print(
          "ERROR::::::::::::::::::::::::::::::::::::::::::::: ${e.toString()}");
    }
  }

  Future<void> _openCameraE(int cameraPosition, ImageSource source) async {
    try {
      final pickedFile = await _picker.pickImage(
        source: source,
      );
      final data = _evidencia
          .firstWhereOrNull((element) => element.id == cameraPosition);
      File fileImage = await compressFile(File(pickedFile.path));
      print('open camera peso antes ${File(pickedFile.path).lengthSync()}');
      print("comrpimido ${fileImage.lengthSync()}");
      if (data != null) {
        final bytes = await fileImage.readAsBytes();
        final baseimage64 = base64Encode(bytes);
        data.img = fileImage;
        data.b64 = baseimage64;
      } else {}

      setState(() {});
    } catch (e) {
      print(
          "ERROR::::::::::::::::::::::::::::::::::::::::::::: ${e.toString()}");
    }
  }

  Future<File> compressFile(File file) async {
    File compressedFile = await FlutterNativeImage.compressImage(
      file.path,
      percentage: 100,
      quality: 50, // CALIDAD PESO
      targetWidth: 1000,
      targetHeight: 800,
    );
    return compressedFile;
  }

  @override
  void initState() {
    super.initState();
    _bloc.init();
    _cargos.add(FilesPhoto(id: 1, img: null, b64: ""));
    _cargos.add(FilesPhoto(id: 2, img: null, b64: ""));
    _cargos.add(FilesPhoto(id: 3, img: null, b64: ""));
    _cargos.add(FilesPhoto(id: 4, img: null, b64: ""));

    _evidencia.add(FilesPhoto(id: 1, img: null, b64: ""));
    _evidencia.add(FilesPhoto(id: 2, img: null, b64: ""));
    _evidencia.add(FilesPhoto(id: 3, img: null, b64: ""));
    _timeInactive = int.parse(_pre.downtime);
    //_bloc.timeOperation();
    WidgetsBinding.instance.addObserver(this);
    datetimeApp();

    // Estados SubEstados
    _bloc.add(GetEstadoSubestados(id: widget.productCode));
    // cargos evidencias Exists
    _cargoExists = widget.cargo;
    _evidenciaExists = widget.evidencia;

    _cargoExists.forEach((element) {
      print("*********************es**********************${element}");
      if (element == "yes") {
        _canEstados++;
      }
    });
    _evidenciaExists.forEach((element) {
      print("**********************sub*********************${element}");
      if (element == "yes") {
        _canSubEstados++;
      }
    });

    this.id = widget.productCode;
    this._typeProcess = widget.type;

    this._estado = widget.estados;
    this._subestado = widget.subestados;
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _bloc.close();
    super.dispose();
  }

  datetimeApp() {
    final df = new DateFormat('dd/MM/yyyy hh:mm a');
    this._datetime = df.format(DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    final MediaQueryData media = MediaQuery.of(context);
    final Size size = media.size;
    // ignore: unused_local_variable
    final EdgeInsets padding = media.padding;

    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        backgroundColor: UtilsColors.white,
        appBar: AppBar(
            elevation: 0,
            backgroundColor: UtilsColors.primary,
            centerTitle: true,
            title: Text(
              "RESUMEN DE PEDIDO",
              style: TextStyle(color: UtilsColors.white, fontSize: 15),
            ),
            automaticallyImplyLeading: false),
        body: BlocBuilder<ResumenBloc, ResumenState>(builder: (context, state) {
          for (var i = 0; i < state.estadosList.length; i++) {
            if (state.estadosList[i]["descripcion"] == "Motivado") {
              estadoNoHit = state.estadosList[i]["descripcion"];
              idEstNoHit = state.estadosList[i]["abreviatura"];

              for (var j = 0; j < state.estadosList[i]["valores"].length; j++) {
                //subestado
                subEstNoHit = state.estadosList[i]["valores"][j]["descripcion"];
                idSubEstNoHit = state.estadosList[i]["valores"][j]["abreviatura"];
              }
            }
          }

          Future.delayed(Duration(seconds: 4), () {
            print("reconociendo ubicacion...");
            if (state.gpsEnabled == false) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 150,
                      width: 150,
                      child: SvgPicture.asset(
                        "assets/svg/ubicacion.svg",
                        //color: ViteColors.primary,
                      ),
                    ),
                    SizedBox(height: 25),
                    Text(
                      "PERMISO REQUERIDO",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        "Para garantizar la entrega del pedido, Enotria requiere"
                        " la ubicación,"
                        " de esta forma podemos mejorar el servicio",
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: UtilsColors.secundary,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    RoundedButton(
                      color: UtilsColors.secundary,
                      text: "Activar GPS",
                      press: () => _bloc.onGPS(),
                      loading: false,
                    ),
                  ],
                ),
              );
            }
          });

          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: 1100,
                  child: Column(
                    children: [
                      SizedBox(height: 1),
                      Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 5),
                        padding: EdgeInsets.symmetric(
                          horizontal: 5,
                          vertical: 5,
                        ),
                        width: size.width * 11,
                        decoration: BoxDecoration(
                          color: UtilsColors.cris0,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(
                          children: [
                            Form(
                                key: _formKey,
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          width: 80,
                                          child: Text(
                                            "Nro. Pedido ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(0),
                                          child: Text(
                                            ': ${widget.deliveryCode}',
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: 12,
                                              color: UtilsColors.cris1,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          width: 80,
                                          child: Text(
                                            "Dni ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(0),
                                          child: Text(
                                            ': ${widget.selectPersonMap["documentId"]}',
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: 12,
                                              color: UtilsColors.cris1,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          width: 80,
                                          child: Text(
                                            "Nombres ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                        ),
                                        Flexible(
                                          child: Container(
                                            padding: EdgeInsets.all(0),
                                            child: Text(
                                              ': ${widget.selectPersonMap["fullName"]}',
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontSize: 12,
                                                color: UtilsColors.cris1,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    //   Row(
                                    //   children: [
                                    //     Container(
                                    //       width: 80,
                                    //       child: Text(
                                    //         "Dirección: ",
                                    //         style: TextStyle(
                                    //             fontWeight: FontWeight.bold,
                                    //             fontSize: 12),
                                    //       ),
                                    //     ),
                                    //     Expanded(
                                    //       child: Container(
                                    //         padding: EdgeInsets.all(0),
                                    //         child: Text(
                                    //           ': ${widget.selectAddressMap["address"]}',
                                    //          // overflow: TextOverflow.ellipsis,
                                    //           style: TextStyle(
                                    //             fontSize: 12,
                                    //             color: UtilsColors.cris1,
                                    //             fontWeight: FontWeight.bold,
                                    //           ),
                                    //         ),
                                    //       ),
                                    //     ),
                                    //   ],
                                    // ),
                                    Row(
                                      children: [
                                        Container(
                                          width: 80,
                                          child: Text(
                                            "Fecha/Hora ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(0),
                                          child: Text(
                                            ': ${this._datetime}',
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: 12,
                                              color: UtilsColors.cris1,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: 80,
                                          child: Text(
                                            "Estado",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                        ),
                                        Expanded(
                                          child: (this._typeProcess == "IMG")
                                              ? Text(
                                                  ": ${this._estado['description']}",
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    color: UtilsColors.cris1,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                )
                                              : (widget.op == "0")
                                                  ? Text(": $estadoNoHit",
                                                      style: TextStyle(
                                                        fontSize: 12,
                                                        color:
                                                            UtilsColors.cris1,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ))
                                                  : DropdownFormField<
                                                      Map<String, dynamic>>(
                                                      emptyText: "",
                                                      emptyActionText: "",
                                                      dropdownHeight: 200,
                                                      onEmptyActionPressed:
                                                          () async {},
                                                      decoration: CustomInputs
                                                          .formInputDecoration(
                                                              labelText: ''),
                                                      onSaved: (dynamic str) {},
                                                      onChanged: (dynamic str) {
                                                        select1 =
                                                            str["descripcion"];
                                                        cod1 =
                                                            str["abreviatura"];
                                                        select2 = "";
                                                        cod2 = "";

                                                        print(str["valores"]);
                                                        Map<String, dynamic>
                                                            _mapGeneric = Map<
                                                                String,
                                                                dynamic>();
                                                        List<
                                                                Map<String,
                                                                    dynamic>>
                                                            _subestados = [];
                                                        for (_mapGeneric
                                                            in str["valores"]) {
                                                          _subestados
                                                              .add(_mapGeneric);
                                                        }

                                                        _bloc.add(GetSubestados(
                                                            subestados:
                                                                _subestados));
                                                      },
                                                      displayItemFn:
                                                          (dynamic item) =>
                                                              Text(
                                                        ': ${(item ?? {})["descripcion"] ?? ""}',
                                                        style: TextStyle(
                                                            color: UtilsColors
                                                                .cris1,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 12),
                                                      ),
                                                      findFn:
                                                          (dynamic str) async =>
                                                              state.estadosList,
                                                      selectedFn:
                                                          (dynamic item1,
                                                              dynamic item2) {
                                                        if (item1 != null &&
                                                            item2 != null) {
                                                          return item1[
                                                                  'descripcion'] ==
                                                              item2[
                                                                  'descripcion'];
                                                        }
                                                        return false;
                                                      },
                                                      filterFn: (dynamic item,
                                                              str) =>
                                                          item['descripcion']
                                                              .toLowerCase()
                                                              .indexOf(str
                                                                  .toLowerCase()) >=
                                                          0,
                                                      dropdownItemFn:
                                                          (dynamic item,
                                                                  int position,
                                                                  bool focused,
                                                                  bool selected,
                                                                  Function()
                                                                      onTap) =>
                                                              ListTile(
                                                        title: Text(item[
                                                            'descripcion']),
                                                        tileColor: focused
                                                            ? Colors.red
                                                            : Colors
                                                                .transparent,
                                                        onTap: onTap,
                                                      ),
                                                    ),
                                        )
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: 80,
                                          child: Text(
                                            "Sub Estado",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                        ),
                                        Expanded(
                                          child: (this._typeProcess == "IMG")
                                              ? Text(
                                                  ": ${this._subestado['description']}",
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    color: UtilsColors.cris1,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                )
                                              : (widget.op == "0")
                                                  ? Text(": $subEstNoHit",
                                                      style: TextStyle(
                                                        fontSize: 12,
                                                        color:
                                                            UtilsColors.cris1,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ))
                                                  : DropdownFormField<
                                                      Map<String, dynamic>>(
                                                      emptyText: "",
                                                      emptyActionText: "",
                                                      dropdownHeight: 200,
                                                      onEmptyActionPressed:
                                                          () async {},
                                                      decoration: CustomInputs
                                                          .formInputDecoration(
                                                              labelText: ''),
                                                      onSaved: (dynamic str) {},
                                                      onChanged: (dynamic str) {
                                                        select2 =
                                                            str["descripcion"];
                                                        cod2 =
                                                            str["abreviatura"];
                                                      },
                                                      displayItemFn:
                                                          (dynamic item) =>
                                                              Container(
                                                        padding:
                                                            EdgeInsets.all(5.0),
                                                        child: Text(
                                                          select2 != ""
                                                              ? ': ${(item ?? {})["descripcion"] ?? ""}'
                                                              : ': ',
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 1,
                                                          style: TextStyle(
                                                            fontSize: 12,
                                                            color: UtilsColors
                                                                .cris1,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                      ),
                                                      findFn: (dynamic
                                                              str) async =>
                                                          state.subestadosList,
                                                      selectedFn:
                                                          (dynamic item1,
                                                              dynamic item2) {
                                                        if (item1 != null &&
                                                            item2 != null) {
                                                          return item1[
                                                                  'descripcion'] ==
                                                              item2[
                                                                  'descripcion'];
                                                        }
                                                        return false;
                                                      },
                                                      filterFn: (dynamic item,
                                                              str) =>
                                                          item['descripcion']
                                                              .toLowerCase()
                                                              .indexOf(str
                                                                  .toLowerCase()) >=
                                                          0,
                                                      dropdownItemFn:
                                                          (dynamic item,
                                                                  int position,
                                                                  bool focused,
                                                                  bool selected,
                                                                  Function()
                                                                      onTap) =>
                                                              ListTile(
                                                        title: Text(item[
                                                            'descripcion']),
                                                        tileColor: focused
                                                            ? Colors.red
                                                            : Colors
                                                                .transparent,
                                                        onTap: onTap,
                                                      ),
                                                    ),
                                        )
                                      ],
                                    ),
                                    (this._typeProcess == "IMG")
                                        ? Container(
                                            child: Column(
                                              children: [
                                                Row(
                                                  children: [
                                                    Container(
                                                      width: 90,
                                                      child: Text(
                                                        "Im. Cargos ",
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 12),
                                                      ),
                                                    ),
                                                    Container(
                                                      padding:
                                                          EdgeInsets.all(0.0),
                                                      child: Text(
                                                        ': ${_canEstados.toString()}',
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                          fontSize: 12,
                                                          color:
                                                              UtilsColors.cris1,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: [
                                                    Container(
                                                      width: 90,
                                                      child: Text(
                                                        "Im. Evidencias ",
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 12),
                                                      ),
                                                    ),
                                                    Container(
                                                      padding:
                                                          EdgeInsets.all(0.0),
                                                      child: Text(
                                                        ': ${_canSubEstados.toString()}',
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                          fontSize: 12,
                                                          color:
                                                              UtilsColors.cris1,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          )
                                        : Container(),
                                    SizedBox(height: 10),
                                  ],
                                ))
                          ],
                        ),
                      ),
                      SizedBox(height: 5),
                      Container(
                        child: Text(
                          "Adjuntar Cargo(físico)",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Gallery(
                            active: _cargoExists[0] == "x" ? true : false,
                            onPressed: () => {
                              _openCamera(
                                1,
                                (this._typeProcess == "IMG")
                                    ? ImageSource.gallery
                                    : ImageSource.camera,
                              )
                            },
                            file: _cargos[0].img,
                          ),
                          Gallery(
                            active: _cargoExists[1] == "x" ? true : false,
                            onPressed: () => {
                              _openCamera(
                                2,
                                (this._typeProcess == "IMG")
                                    ? ImageSource.gallery
                                    : ImageSource.camera,
                              )
                            },
                            file: _cargos[1].img,
                          ),
                          Gallery(
                            active: _cargoExists[2] == "x" ? true : false,
                            onPressed: () => {
                              _openCamera(
                                3,
                                (this._typeProcess == "IMG")
                                    ? ImageSource.gallery
                                    : ImageSource.camera,
                              )
                            },
                            file: _cargos[2].img,
                          ),
                          Gallery(
                            active: _cargoExists[3] == "x" ? true : false,
                            onPressed: () => {
                              _openCamera(
                                4,
                                (this._typeProcess == "IMG")
                                    ? ImageSource.gallery
                                    : ImageSource.camera,
                              )
                            },
                            file: _cargos[3].img,
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Container(
                        child: Text(
                          "Adjuntar Evidencia",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Gallery(
                            active: _evidenciaExists[0] == "x" ? true : false,
                            onPressed: () => {
                              _openCameraE(
                                1,
                                (this._typeProcess == "IMG")
                                    ? ImageSource.gallery
                                    : ImageSource.camera,
                              )
                            },
                            file: _evidencia[0].img,
                          ),
                          Gallery(
                            active: _evidenciaExists[1] == "x" ? true : false,
                            onPressed: () => {
                              _openCameraE(
                                2,
                                (this._typeProcess == "IMG")
                                    ? ImageSource.gallery
                                    : ImageSource.camera,
                              )
                            },
                            file: _evidencia[1].img,
                          ),
                          Gallery(
                            active: _evidenciaExists[2] == "x" ? true : false,
                            onPressed: () => {
                              _openCameraE(
                                3,
                                (this._typeProcess == "IMG")
                                    ? ImageSource.gallery
                                    : ImageSource.camera,
                              )
                            },
                            file: _evidencia[2].img,
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      RoundedButton(
                        color: UtilsColors.secundary,
                        text: "FINALIZAR",
                        press: () {
                          /*NO REQUIERO VALIDAR ESTADOS SOLO ENVIAR IMAGENES  */

                          switch (widget.type) {
                            case "IMG":
                              print("solo imagen::::::::::::::::::");

                              _bloc.add(
                                SendResumenFinalize(
                                    context: context,
                                    type: "IMG",
                                    productName: widget.productName,
                                    enterpriseName: widget.enterpriseName,
                                    deliveryCode: widget.deliveryCode,
                                    fecha: _datetime,
                                    code: widget.code,
                                    selectAddressMap: widget.selectAddressMap,
                                    selectPersonMap: widget.selectPersonMap,
                                    estado: this._estado["description"],
                                    codEstado: this._estado["code"],
                                    subestado: this._subestado["description"],
                                    codSubEstado: this._subestado["code"],
                                    cargos: _cargos,
                                    evidencia: _evidencia,
                                    position: state.myLocation),
                              );
                              break;
                            case "REGISTRO":
                              if (select1 == "" &&
                                  this._estado['abreviatura'] != "G" &&
                                  estadoNoHit != "Motivado") {
                                showTopSnackBar(
                                  context,
                                  CustomSnackBar.info(
                                    message: "Seleccione Estado",
                                  ),
                                );
                              } else if(select1 == "" && this._estado==null
                              //TODO: REVISAR CUANDO SE SALTA LA BIOMETRIA
                              ){
                                showTopSnackBar(
                                  context,
                                  CustomSnackBar.info(
                                    message: "Seleccione Estado",
                                  ),
                                );
                              }
                              
                              else if (select2 == "" &&
                                  estadoNoHit != "Motivado") {
                                showTopSnackBar(
                                  context,
                                  CustomSnackBar.info(
                                    message: "Seleccione Sub Estado",
                                  ),
                                );
                              } else if (state.myLocation == null) {
                                showTopSnackBar(
                                  context,
                                  CustomSnackBar.info(
                                    message: "Ubicacion GPS no obtenida.",
                                  ),
                                );
                              }
                              if (select1 != "" && select2 != "") {
                                print("----> ${widget.type}");
                                _bloc.add(
                                  SendResumenFinalize(
                                      context: context,
                                      type: "REGISTRO",
                                      productName: widget.productName,
                                      enterpriseName: widget.enterpriseName,
                                      deliveryCode: widget.deliveryCode,
                                      fecha: _datetime,
                                      code: widget.code,
                                      selectAddressMap: widget.selectAddressMap,
                                      selectPersonMap: widget.selectPersonMap,
                                      estado: select1,
                                      codEstado: cod1,
                                      subestado: select2,
                                      codSubEstado: cod2,
                                      cargos: _cargos,
                                      evidencia: _evidencia,
                                      position: state.myLocation),
                                );
                              } else if (estadoNoHit == "Motivado") {
                                select1 = estadoNoHit;
                                cod1 = idEstNoHit;
                                select2 = subEstNoHit;
                                cod2 = idSubEstNoHit;

                                _bloc.add(
                                  SendResumenFinalize(
                                      context: context,
                                      type: "REGISTRO",
                                      productName: widget.productName,
                                      enterpriseName: widget.enterpriseName,
                                      deliveryCode: widget.deliveryCode,
                                      fecha: _datetime,
                                      code: widget.code,
                                      selectAddressMap: widget.selectAddressMap,
                                      selectPersonMap: widget.selectPersonMap,
                                      estado: select1,
                                      codEstado: cod1,
                                      subestado: select2,
                                      codSubEstado: cod2,
                                      cargos: _cargos,
                                      evidencia: _evidencia,
                                      position: state.myLocation),
                                );
                              }

                              break;
                            case "IMG":
                              break;
                            default:
                          }
                        },
                        loading: false,
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 50,
                  padding: new EdgeInsets.only(top: 0.0, left: 10),
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Enotria S.A. Ⓒ2021",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 13, color: UtilsColors.tersary),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        }),
      ),
    );
  }
}

class Gallery extends StatefulWidget {
  final void Function() onPressed;
  final Io.File file;
  final bool active;
  Gallery({Key key, this.onPressed, this.file, this.active}) : super(key: key);

  @override
  _GalleryState createState() => _GalleryState();
}

class _GalleryState extends State<Gallery> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: 60,
      child: widget.file == null
          ? Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  icon: (widget.active)
                      ? Icon(Icons.add_a_photo)
                      : Icon(
                          Icons.cancel_presentation,
                          color: Colors.transparent,
                        ),
                  onPressed: (widget.active) ? widget.onPressed : null,
                  color: UtilsColors.cris1,
                ),
              ],
            )
          : Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.file(
                    widget.file,
                    height: 85,
                    width: 85,
                    fit: BoxFit.cover,
                  ),
                ),
                CircleWithIcon(
                  function: widget.onPressed,
                  icon: Icons.edit,
                  coloricon: Colors.black,
                ),
              ],
            ),
      decoration: BoxDecoration(
          color: UtilsColors.cris0,
          border: Border.all(
            color: UtilsColors.cris0, // Set border color
            width: 1.0,
          ), // Set border width
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ), // Set rounded corner radius
          boxShadow: [
            BoxShadow(
              blurRadius: 1,
              color: UtilsColors.secundary,
              offset: Offset(0, 0),
            )
          ] // Make rounded corner of border
          ),
    );
  }
}

class CircleWithIcon extends StatelessWidget {
  final IconData icon;
  final void Function() function;
  final Color coloricon;

  CircleWithIcon(
      {@required this.icon, @required this.function, @required this.coloricon})
      : assert(icon != null),
        assert(function != null);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      child: CupertinoButton(
          padding: EdgeInsets.zero,
          minSize: 35,
          child: Container(
              width: 20,
              height: 20,
              child: Center(
                child: Icon(
                  icon,
                  size: 15,
                  color: coloricon,
                ),
              ),
              decoration: BoxDecoration(
                  color: Color(0xfff0f0f0),
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        blurRadius: 10,
                        offset: Offset(5, 5)),
                  ])),
          onPressed: function),
      right: 0,
      top: 0,
    );
  }
}

class FilesPhoto {
  int id;
  File img;
  String b64;

  FilesPhoto({this.id, this.img, this.b64});
}
