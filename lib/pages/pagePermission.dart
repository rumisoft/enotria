import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionLocation extends StatefulWidget {
  PermissionLocation({Key key}) : super(key: key);

  @override
  _PermissionLocationState createState() => _PermissionLocationState();
}

class _PermissionLocationState extends State<PermissionLocation>
    with AfterLayoutMixin {
  @override
  void afterFirstLayout(BuildContext context) {
    this.check();
  }

  check() async {
    //comprobar si los permisos de gps estan habilitados
    final bool hassAccess = await Permission.locationWhenInUse.isGranted;
    if (hassAccess) {
      Navigator.pushReplacementNamed(context, 'Dni');
    } else {
      Navigator.pushReplacementNamed(context, 'PermissionGPS');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          color: Colors.white,
        ),
      ),
    );
  }
}
