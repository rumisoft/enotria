import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:permission_handler/permission_handler.dart';


import 'dart:io' show Platform;

class PermissionGPS extends StatefulWidget {
  PermissionGPS({Key key}) : super(key: key);

  @override
  _PermissionGPSState createState() => _PermissionGPSState();
}

class _PermissionGPSState extends State<PermissionGPS> with WidgetsBindingObserver {
  bool fromSetting = false;

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed && fromSetting == true) {
      fromSetting = false;
      this._check();
    }
  }

  _check() async {
    final bool hassAccess = await Permission.locationWhenInUse.isGranted;
    if (hassAccess) {
      //si tiene permiso de acceso ingresar al home
      gotuhome();
    }
  }

  gotuhome() {
    print("al home bro");
    Navigator.pushReplacementNamed(context, 'Dni');
  }

  openSettings() async {
    await openAppSettings();

    fromSetting = true;
  }

  Future<void> _request() async {
    final PermissionStatus _status =
        await Permission.locationWhenInUse.request();

    switch (_status) {
      case PermissionStatus.denied:
        // DENEGADO.
        if (Platform.isIOS) {
          this.openSettings();
        }
        break;
      case PermissionStatus.granted:
        // ACEPTADO.
        this.gotuhome();
        break;
      case PermissionStatus.restricted:
        // Handle this case.
        break;
      case PermissionStatus.limited:
        // THandle this case.
        break;
      case PermissionStatus.permanentlyDenied:
        // DENEGADO PERMANNENTE
        this.openSettings();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 150,
              width: 150,
              child: SvgPicture.asset(
                "assets/svg/ubicacion.svg",
              ),
            ),
            SizedBox(height: 25),
            Text(
              "PERMISO REQUERIDO",
              style: TextStyle(fontWeight: FontWeight.bold,color:UtilsColors.cris1),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                "Para garantizar un buen servicio, Enotria recopila los datos de"
                " tu ubicación desde el momento en el que abres la app,"
                " de esta forma podemos mejorar el servicio",
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: UtilsColors.secundary,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            RoundedButton(
              color: UtilsColors.secundary,
              text: "Permitir",
              press: () => _request(),
              loading: false,
            ),
          ],
        ),
      ),
    );
  }
}
