import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RoundedInput extends StatelessWidget {
  final String initialValue;
  final String hintText;
  final IconData icon;
  final TextInputType keyboardType;
  final ValueChanged<String> onChanged;
  final String Function(String) validator;

  final Color boxColor;
  const RoundedInput({
    Key key,
    this.initialValue,
    this.hintText,
    this.icon = Icons.person,
    this.keyboardType,
    this.validator,
    this.boxColor = UtilsColors.cris0,
    this.onChanged,
  }) : super(key: key);

  get tipoApk => null;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: boxColor,
        borderRadius: BorderRadius.circular(8),
      ),
      child: TextFormField(
      // initialValue: "imb.jhernandez",
       // tipoApk == "develop" ? "splp.qds.test1": "", //"3al.jhernandez",
        keyboardType: keyboardType,
        onChanged: onChanged,
        cursorColor: UtilsColors.primary,
        decoration: InputDecoration(
          icon: Icon(icon, color: UtilsColors.primary),
          hintText: hintText,
          border: InputBorder.none,
        ),
        validator: validator,
      ),
    );
  }
}

class RoundedInputRead extends StatelessWidget {
  final bool read;
  final String initialValue;
  final String hintText;
  final IconData icon;
  final TextInputType keyboardType;
  final ValueChanged<String> onChanged;
  final String Function(String) validator;
  final String title;

  final Color boxColor;
  const RoundedInputRead({
    Key key,
    this.read,
    this.title = "",
    this.initialValue,
    this.hintText,
    this.icon = Icons.person,
    this.keyboardType,
    this.validator,
    this.boxColor = UtilsColors.cris0,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 1, horizontal: 10),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: boxColor,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: UtilsColors.cris1,
                fontSize: 12),
          ),
          TextFormField(
            readOnly: read,
            keyboardType: keyboardType,
            onChanged: onChanged,
            cursorColor: UtilsColors.primary,
            decoration: InputDecoration(
              icon: Icon(
                icon,
                color: UtilsColors.primary,
                size: 20,
              ),
              hintStyle: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.normal,
                  color: UtilsColors.cris1),
              hintText: hintText,
              border: InputBorder.none,
            ),
            validator: validator,
          )
        ],
      ),
    );
  }
}

class RoundedInputSearch extends StatelessWidget {
  final bool read;
  final String hintText;
  final int length;
  final IconData icon;
  final TextInputType keyboardType;
  final ValueChanged<String> onChanged;
  final String Function(String) validator;
  final Color boxColor;
  final TextEditingController controller;
  const RoundedInputSearch({
    Key key,
    this.read = false,
    this.hintText,
    this.length = 15,
    this.icon = Icons.person,
    this.keyboardType,
    this.validator,
    this.boxColor = UtilsColors.cris0,
    this.onChanged,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.6,
      decoration: BoxDecoration(
        color: boxColor,
        borderRadius: BorderRadius.circular(8),
      ),
      child: TextFormField(
        controller: controller,
        keyboardType: TextInputType.text,
        textCapitalization: TextCapitalization.characters,
        maxLength: length,
        //maxLengthEnforcement: MaxLengthEnforcement.truncateAfterCompositionEnds,
        //readOnly: read,
        style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.bold,
            letterSpacing: 1,
            color: UtilsColors.cris1),
        //initialValue:"PED00135303",// "PED001353940",// "PED00135303",
        //keyboardType: keyboardType,
        onChanged: onChanged,
        cursorColor: UtilsColors.primary,
        decoration: InputDecoration(
          hintText: hintText,
          border: InputBorder.none,
        ),
        validator: validator,
        // inputFormatters: [
        //   // UpperCaseTextFormatter(),
        //   //new LengthLimitingTextInputFormatter(length),
        // ],
      ),
    );
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text?.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => true;
}

class ContainerWidget extends StatelessWidget {
  final Widget child;
  final Color color;
  final double width;
  final double height;
  const ContainerWidget({
    Key key,
    this.child,
    this.color,
    this.width,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Center(
      child: Container(
        width: responsive.wp(width),
        height: responsive.hp(height),
        margin: EdgeInsets.symmetric(
            horizontal: responsive.ip(1.1), vertical: responsive.ip(0.5)),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5), color: UtilsColors.cris0),
        child: child,
      ),
    );
  }
}


class PiePagina extends StatelessWidget {
  const PiePagina({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      padding: new EdgeInsets.only(top: 0.0, left: 10),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Enotria S.A. Ⓒ2021",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 13, color: UtilsColors.tersary),
            )
          ],
        ),
      ),
    );
  }
}
