import 'package:enotria/util/UtilsColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final VoidCallback press;
  final Color color, textColor;
  final Color borderColor;
  final bool loading;
  final String icon;

  const RoundedButton({
    Key key,
    this.text,
    this.press,
    this.color = Colors.red,
    this.borderColor,
    this.textColor = Colors.white,
    this.loading = false,
    this.icon = "",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 1),
      width: size.width * 0.8,
      height: 50,
      decoration: BoxDecoration(
          border: Border.all(color: borderColor ?? Colors.transparent),
          borderRadius: BorderRadius.circular(8)),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: MaterialButton(
          padding: const EdgeInsets.symmetric(
            vertical: 5,
            horizontal: 10,
          ),
          color: color,
          disabledColor: Colors.transparent,
          highlightColor:Colors.transparent ,
          onPressed: press,
          child: (loading)
              ? Container(
                  width: 25,
                  height: 25,
                  child: CircularProgressIndicator(
                    color: UtilsColors.white,
                  ),
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      text,
                      style: TextStyle(
                        color: textColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                    (icon == "")
                        ? Container()
                        : Container(
                            height: 50,
                            width: 50,
                            child: SvgPicture.asset(
                              "$icon", color: UtilsColors.cris1,
                              // color: ViteColors.primary,
                            ),
                          ),
                  ],
                ),
        ),
      ),
    );
  }
}

class RoundedButtonDialog extends StatelessWidget {
  final String text;
  final VoidCallback press;
  final Color color, textColor;
  final Color borderColor;
  final bool loading;

  const RoundedButtonDialog(
      {Key key,
      this.text,
      this.press,
      this.color = Colors.red,
      this.borderColor,
      this.textColor = Colors.white,
      this.loading = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 1),
      width: size.width * 0.3,
      decoration: BoxDecoration(
          border: Border.all(color: borderColor ?? Colors.transparent),
          borderRadius: BorderRadius.circular(8)),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: MaterialButton(
          padding: const EdgeInsets.symmetric(
            vertical: 8,
            horizontal: 10,
          ),
          color: color,
          disabledColor: UtilsColors.tersary,
          onPressed: press,
          child: (loading)
              ? Container(
                  width: 25,
                  height: 25,
                  child: CircularProgressIndicator(
                    color: UtilsColors.white,
                  ),
                )
              : Text(
                  text,
                  style: TextStyle(
                    color: textColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 11,
                  ),
                ),
        ),
      ),
    );
  }
}

class RoundedButtonSearch extends StatelessWidget {
  final String text;
  final VoidCallback press;
  final Color color, textColor;
  final Color borderColor;
  final bool loading;
  final IconData icon;

  const RoundedButtonSearch(
      {Key key,
      this.text,
      this.press,
      this.color = Colors.red,
      this.borderColor,
      this.textColor = Colors.white,
      this.loading = false,
      this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: 40,
      height: 40,
      decoration: BoxDecoration(
          border: Border.all(color: borderColor ?? Colors.transparent),
          borderRadius: BorderRadius.circular(8)),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: MaterialButton(
          padding: const EdgeInsets.symmetric(
            vertical: 0,
            horizontal: 0,
          ),
          color: color,
          disabledColor: UtilsColors.tersary,
          onPressed: press,
          child: (loading)
              ? Container(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(
                    color: UtilsColors.white,
                  ),
                )
              : Center(
                  child: Container(
                    width: 25,
                    height: 25,
                    child: Icon(icon, color: UtilsColors.white),
                  ),
                ),
        ),
      ),
    );
  }
}


class RoundedButtonDialog2 extends StatelessWidget {
  final String text;
  final VoidCallback press;
  final Color color, textColor;
  final Color borderColor;
  final bool loading;

  const RoundedButtonDialog2(
      {Key key,
      this.text,
      this.press,
      this.color = Colors.red,
      this.borderColor,
      this.textColor = Colors.white,
      this.loading = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 1),
      width: size.width * 0.2,
      decoration: BoxDecoration(
          border: Border.all(color: borderColor ?? Colors.transparent),
          borderRadius: BorderRadius.circular(8)),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: MaterialButton(
          padding: const EdgeInsets.symmetric(
            vertical: 8,
            horizontal: 10,
          ),
          color: color,
          disabledColor: UtilsColors.tersary,
          onPressed: press,
          child: (loading)
              ? Container(
                  width: 25,
                  height: 25,
                  child: CircularProgressIndicator(
                    color: UtilsColors.white,
                  ),
                )
              : Text(
                  text,
                  style: TextStyle(
                    color: textColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 11,
                  ),
                ),
        ),
      ),
    );
  }
}
