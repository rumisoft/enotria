import 'package:flutter/material.dart';

class RoundedSocialButton extends StatelessWidget {
  final String text;
  final void Function() press;
  final Color color, textColor;
  final String imagePath;
  final Color borderColor;

  const RoundedSocialButton({
    Key key,
    this.text,
    this.press,
    this.color = Colors.red,
    this.borderColor,
    this.imagePath,
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * 0.8,
      decoration: BoxDecoration(
          border: Border.all(color: borderColor ?? Colors.transparent),
          color: color,
          borderRadius: BorderRadius.circular(29)),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(29),
        child: MaterialButton(
          padding: EdgeInsets.symmetric(
            vertical: 20,
            horizontal: 40,
          ),
          onPressed: press,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                image: AssetImage(imagePath),
                height: 20.0,
              ),
              SizedBox(width: 20),
              Text(
                text,
                style: TextStyle(color: textColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
