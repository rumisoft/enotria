import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/constast.dart';
import 'package:flutter/material.dart';

class RoundedPassword extends StatefulWidget {
  final String hintText;
  final IconData icon;
  final TextInputType keyboardType;
  final ValueChanged<String> onChanged;
  final String Function(String) validator;

  final Color boxColor;
  final bool sufixicon;
  const RoundedPassword({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.keyboardType,
    this.sufixicon = false,
    this.validator,
    this.boxColor = UtilsColors.cris0,
    this.onChanged,
  }) : super(key: key);

  @override
  _RoundedPasswordState createState() => _RoundedPasswordState();
}

class _RoundedPasswordState extends State<RoundedPassword> {
  bool _obscureText = true;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: widget.boxColor,
        borderRadius: BorderRadius.circular(8),
      ),
      child: TextFormField(
      // initialValue:"Eno*2022*",
       // initialValue:  tipoApk == "develop" ? "Qds_test1" : "",
        // "xHsWz#8?Bpp\$",
        keyboardType: widget.keyboardType,
        onChanged: widget.onChanged,
        obscureText: _obscureText,
        cursorColor: UtilsColors.primary,
        decoration: InputDecoration(
            icon: Icon(
              widget.icon,
              color: UtilsColors.primary,
            ),
            hintText: widget.hintText,
            border: InputBorder.none,
            suffixIcon: widget.sufixicon == true
                ? IconButton(
                    splashColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    icon: Icon(
                      _obscureText ? Icons.visibility_off : Icons.visibility,
                      color: UtilsColors.primary,
                    ),
                    onPressed: () {
                      setState(() {
                        _obscureText = !_obscureText;
                      });
                    },
                  )
                : null),
        validator: widget.validator,
      ),
    );
  }
}
