import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_restart/flutter_restart.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

newMethodDialog() async {
  Get.dialog(
    AlertDialog(
      title: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 50,
              width: 50,
              child: SvgPicture.asset(
                "assets/svg/tiempo.svg",color: UtilsColors.cris1,
                // color: ViteColors.primary,
              ),
            ),
            SizedBox(height: 25),
            Text(
              "SESIÓN EXPIRADA",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: UtilsColors.cris1,
                  fontSize: 12),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                "Sesión expirada por inactividad.",
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: UtilsColors.secundary,
                    fontSize: 11),
                textAlign: TextAlign.center,
              ),
            ),
            RoundedButton(
              color: UtilsColors.secundary,
              text: "Aceptar",
              press: () async {
                final result = await FlutterRestart.restartApp();
                print(result);
              },
              loading: false,
            ),
          ],
        ),
      ),
      backgroundColor: UtilsColors.white,
    ),
    barrierDismissible: false,
  );
}

String formatISOTime(DateTime date) {
  var duration = date.timeZoneOffset;
  if (duration.isNegative)
    return (date.toIso8601String());
  else
    return (date.toIso8601String());
}
