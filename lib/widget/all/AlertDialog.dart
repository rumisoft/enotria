import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/widget/all/RoundedButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_restart/flutter_restart.dart';
import 'package:flutter_svg/flutter_svg.dart';

AwesomeDialog alertDialog({
  BuildContext context,
  String type,
  String text,
  String buttonOkText,
  Function pressOK,
  Function pressCANCEL,
  void Function() buttonOkpress,
}) {
  return AwesomeDialog(
    context: context,
    dialogType: DialogType.NO_HEADER,
    dialogBackgroundColor: UtilsColors.white,
    btnOkColor: UtilsColors.primary,
    btnCancelColor: UtilsColors.danger,
    btnCancelText: "cancelar",
    dismissOnTouchOutside: true,
    dismissOnBackKeyPress: true,
    width: 390,
    buttonsBorderRadius: BorderRadius.all(Radius.circular(45)),
    headerAnimationLoop: true,
    animType: AnimType.BOTTOMSLIDE,
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 50,
            width: 50,
            child: SvgPicture.asset(
              "assets/svg/imagen.svg", color: UtilsColors.cris1,
              // color: ViteColors.primary,
            ),
          ),
          SizedBox(height: 25),
          // Text(
          //   "SESIÓN EXPIRADA",
          //   style: TextStyle(
          //       fontWeight: FontWeight.bold,
          //       color: UtilsColors.cris1,
          //       fontSize: 12),
          // ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text(
              text,
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: UtilsColors.cris1,
                  fontSize: 13),
              textAlign: TextAlign.center,
            ),
          ),
          RoundedButton(
            color: UtilsColors.secundary,
            text: buttonOkText,
            press: buttonOkpress,
            loading: false,
          ),
        ],
      ),
    ),
    showCloseIcon: false,
    //btnCancelOnPress: pressCANCEL,
    //btnOkOnPress: pressOK,
  )..show();
}


AwesomeDialog alertDialog2Button({
  BuildContext context,
  String type,
  String text,
  Function pressOK,
  Function pressCANCEL,
  String buttonOkText,
  void Function() buttonOkpress,
  String buttonOkText2,
  void Function() buttonOkpress2,
}) {
  return AwesomeDialog(
    context: context,
    dialogType: DialogType.NO_HEADER,
    dialogBackgroundColor: UtilsColors.white,
    btnOkColor: UtilsColors.primary,
    btnCancelColor: UtilsColors.danger,
    btnCancelText: "cancelar",
    dismissOnTouchOutside: true,
    dismissOnBackKeyPress: true,
    width: 390,
    buttonsBorderRadius: BorderRadius.all(Radius.circular(45)),
    headerAnimationLoop: true,
    animType: AnimType.BOTTOMSLIDE,
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 50,
            width: 50,
            child: SvgPicture.asset(
              "assets/svg/shuffle.svg", color: UtilsColors.cris1,
              // color: ViteColors.primary,
            ),
          ),
          SizedBox(height: 25),
          // Text(
          //   "SESIÓN EXPIRADA",
          //   style: TextStyle(
          //       fontWeight: FontWeight.bold,
          //       color: UtilsColors.cris1,
          //       fontSize: 12),
          // ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text(
              text,
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: UtilsColors.cris1,
                  fontSize: 13),
              textAlign: TextAlign.center,
            ),
          ),
          RoundedButton(
            color: UtilsColors.secundary,
            text: buttonOkText,
            press: buttonOkpress,
            loading: false,
          ),
          RoundedButton(
            color: UtilsColors.secundary,
            text: buttonOkText2,
            press: buttonOkpress2,
            loading: false,
          ),
        ],
      ),
    ),
    showCloseIcon: false,
    //btnCancelOnPress: pressCANCEL,
    //btnOkOnPress: pressOK,
  )..show();
}

Color colorsType(String color) {
  switch (color) {
    case "INFO":
      return UtilsColors.white;
    case "WARNING":
      return UtilsColors.warning;
    case "ERROR":
      return UtilsColors.danger;
    case "SUCCES":
      return UtilsColors.white;
    default:
      return UtilsColors.white;
  }
}

Widget svg(String type) {
  switch (type) {
    case "INFO":
      return SvgPicture.asset("assets/svg/info.svg", color: UtilsColors.white);

    case "WARNING":
      return SvgPicture.asset("assets/svg/warning.svg",
          color: UtilsColors.warning);

    case "ERROR":
      return SvgPicture.asset("assets/svg/error.svg",
          color: UtilsColors.danger);

    case "SUCCES":
      return SvgPicture.asset("assets/svg/check.svg", color: UtilsColors.white);

    default:
      return SvgPicture.asset("assets/svg/warning.svg",
          color: UtilsColors.warning);
  }




  
}
  //WillPopScope
Future<bool> onBackPressed(BuildContext context) {
  return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 50,
                    width: 50,
                    child: SvgPicture.asset(
                      "assets/svg/tiempo.svg",
                      // color: ViteColors.primary,
                    ),
                  ),
                  SizedBox(height: 25),
                  Text(
                    "¿Estás seguro?",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: UtilsColors.cris1,
                        fontSize: 12),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text(
                      "¿Quieres salir de la aplicación?",
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: UtilsColors.secundary,
                          fontSize: 11),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Row(
                    children: [
                      RoundedButtonDialog(
                        color: UtilsColors.secundary,
                        text: "Aceptar",
                        press: () async {
                          // ignore: unused_local_variable
                          final result = await FlutterRestart.restartApp();
                          //Navigator.of(context).pop(true);
                        },
                        loading: false,
                      ),
                      RoundedButtonDialog(
                        color: UtilsColors.secundary,
                        text: "Cancelar",
                        press: () async {
                          Navigator.of(context).pop(false);
                        },
                        loading: false,
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        },
      ) ??
      false;
}
