import 'dart:io';

import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/userPreferences.dart';
import 'package:flutter/material.dart';
import 'package:list_tile_more_customizable/list_tile_more_customizable.dart';
import 'package:share_extend/share_extend.dart';
import 'package:sqflite/sqflite.dart';

class DrawerUser extends StatelessWidget {
  const DrawerUser({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    UserPreferences prefe = UserPreferences();

    return Drawer(
      child: Container(
        decoration: BoxDecoration(color: Colors.black),
        child: Column(
          children: <Widget>[
            Container(
              child: DrawerHeader(
                child: GestureDetector(
                  onTap: () {
                    //Navigator.pushNamed(context, PagesUpdateUser.routeName);
                  },
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      // Align(
                      //   alignment: Alignment.center,
                      //   child: Avatar(
                      //     photoUrl: prefe.userImage,
                      //     size: 100,
                      //   ),
                      // ),
                      SizedBox(
                        width: 5.0,
                      ), //Distancia entre Foto Logo y Usuario/Editar Perfil
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(height: 30.0),
                                Align(
                                  alignment: Alignment.topCenter,
                                  child: Text(
                                    "nombres",
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "nombres",
                                  ),
                                ),
                              ],
                            ),
                          ]),
                    ],
                  ),
                ),
              ),
            ),
            const Divider(
              color: UtilsColors.white,
              height: 1,
              thickness: 1,
              indent: 1,
              endIndent: 1,
            ),
            Expanded(
              child: Column(children: <Widget>[
                ListTile(
                  title: Text('DESPACHO',
                      style: TextStyle(color: UtilsColors.white)),
                ),
                ListTileMoreCustomizable(
                  title: Row(
                    children: <Widget>[
                      Icon(
                        Icons.arrow_right,
                        color: UtilsColors.primary,
                        size: 20,
                      ),
                      SizedBox(
                        height: 5,
                        width: 5,
                      ),
                      Container(
                        width:
                            220, //This helps the text widget know what the maximum width is again! You may also opt to use an Expanded widget instead of a Container widget, if you want to use all remaining space.
                        child: Center(
                          //I added this widget to show that the width limiting widget doesn't need to be a direct parent.
                          child: Text(
                            "Consulta General                                          ",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                  onTap: (details) {
                    Navigator.pushNamed(context, 'Dni');
                  },
                ),
                ListTileMoreCustomizable(
                  title: Row(
                    children: <Widget>[
                      Icon(
                        Icons.arrow_right,
                        color: UtilsColors.primary,
                        size: 20,
                      ),
                      SizedBox(
                        height: 5,
                        width: 5,
                      ),
                      Container(
                        width:
                            220, //This helps the text widget know what the maximum width is again! You may also opt to use an Expanded widget instead of a Container widget, if you want to use all remaining space.
                        child: Center(
                          //I added this widget to show that the width limiting widget doesn't need to be a direct parent.
                          child: Text(
                            "Consulta por Código de entrega                                          ",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                  onTap: (details) {
                    Navigator.pushNamed(context, 'Consult');
                  },
                ),
                ListTile(
                  title: Text('SEGURIDAD',
                      style: TextStyle(color: UtilsColors.white)),
                ),
                ListTileMoreCustomizable(
                  title: Row(
                    children: <Widget>[
                      Icon(
                        Icons.arrow_right,
                        color: UtilsColors.primary,
                        size: 20,
                      ),
                      SizedBox(
                        height: 5,
                        width: 5,
                      ),
                      Container(
                        width:
                            220, //This helps the text widget know what the maximum width is again! You may also opt to use an Expanded widget instead of a Container widget, if you want to use all remaining space.
                        child: Center(
                          //I added this widget to show that the width limiting widget doesn't need to be a direct parent.
                          child: Text(
                            "Cambio de Contraseña                                      ",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                  onTap: (details) {
                    Navigator.pushNamed(context, 'Update_Password');
                  },
                  onLongPress: (details) {
                    print("aqui......");
                  },
                ),
                ListTile(
                  onTap: () {
                    _shareStorageFile();
                  },
                  leading: Icon(
                    Icons.share,
                    color: UtilsColors.primary,
                    size: 25,
                  ),
                  title: Text('Shared Log',
                      style: TextStyle(color: UtilsColors.white)),
                ),
                ListTile(
                  onTap: () {
                    // prefe. = false;
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        'Login', (Route<dynamic> route) => false);

                    // Navigator.pushReplacement(context, PageLogin());
                  },
                  leading: Icon(
                    Icons.arrow_circle_up_sharp,
                    color: UtilsColors.primary,
                    size: 25,
                  ),
                  title:
                      Text('SALIR', style: TextStyle(color: UtilsColors.white)),
                ),
              ]),
            ),
            Container(
              height: 50,
              padding: new EdgeInsets.only(top: 0.0, left: 10),
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Enotria S.A. Ⓒ2021",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 13, color: UtilsColors.tersary),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _shareStorageFile() async {
    var databasesPath = await getDatabasesPath();
    File testFile = File("${databasesPath.toString()}/EnotriaLOG.db");
    if (!await testFile.exists()) {
      await testFile.create(recursive: true);
      testFile.writeAsStringSync("test for share documents file");
    }
    ShareExtend.share(testFile.path, "file");
  }
}
