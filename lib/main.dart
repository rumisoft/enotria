import 'dart:async';
import 'dart:io';
import 'package:enotria/bloc/bloc_update_password/bloc_opdate_password_bloc.dart';
import 'package:enotria/pages/pagePermission.dart';
import 'package:enotria/pages/pagePermissionGPS.dart';
import 'package:enotria/pages/page_consult.dart';
import 'package:enotria/pages/page_dni.dart';
import 'package:enotria/pages/page_biometria.dart';
import 'package:enotria/pages/page_resumen.dart';
import 'package:enotria/pages/page_login.dart';
import 'package:enotria/pages/page_resumen_finalize.dart';
import 'package:enotria/pages/page_splash.dart';
import 'package:enotria/pages/page_update_password.dart';
import 'package:enotria/pages/recuperar_contrasenia/page_new_password.dart';
import 'package:enotria/pages/recuperar_contrasenia/page_validate_code.dart';
import 'package:enotria/pages/recuperar_contrasenia/page_writeUser.dart';
import 'package:enotria/pages/xpruebaChannel/inicio_prueba.dart';
import 'package:enotria/pages/xpruebaChannel/prubaMethodPage.dart';
import 'package:enotria/util/UtilsColors.dart';
import 'package:enotria/util/userPreferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_root_detection/flutter_root_detection.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'bloc/bloc_login/bloc_login_bloc.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new UserPreferences();
  prefs.initPrefs();
  HttpOverrides.global = new MyHttpOverrides();
  runZonedGuarded(() {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    runApp(MyApp());
  }, (dynamic error, dynamic stack) {});
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

//Para acceder a la url cuando falla la config de algun certificado ssl
class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    initializeDateFormatting();

    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => BlocLoginBloc()),
        BlocProvider(create: (_) => BlocUpdatePasswordBloc()),
      ],
      child: MaterialApp(
        home: PageSplash(),
        theme: ThemeData(
            appBarTheme: AppBarTheme(backgroundColor: UtilsColors.secundary),
            primaryColor: UtilsColors.primary,
            //canvasColor: Colors.black,
            fontFamily: 'Gelion'),
        routes: {
          'Login': (_) => PageLogin(),
          'PermissionLocation': (_) => PermissionLocation(),
          'PermissionGPS': (_) => PermissionGPS(),
          'Resumen': (_) => PageResumen(),
          'Resumen_finalize': (_) => PageResumenFinalize(),
          'Dni': (_) => PageDni(),
          'Update_Password': (_) => UpdatePassword(),
          'Write_User': (_) => WriteUser(),
          'Validate_Code': (_) => ValidateCode(),
          'New_Password': (_) => NewPasswordPage(),
          'Consult': (_) => PageConsult(),
          'biometria': (_) => PageBiometria(),
          'pruebaMethod': (_) => PruebaMethodPage(),
          'inicio': (_) => InicioPage(),
        },
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
