

import 'package:dio/dio.dart';
import 'package:enotria/api/api_utils.dart';
import 'package:enotria/util/userPreferences.dart';

class ApiEstadosSubestados {
  Future<Map<String, dynamic>> getEstadoSubestados(String id) async {
   Map<String, dynamic> _map = new Map<String, dynamic>();
    ApiUtils _api = new ApiUtils();
    UserPreferences _pre = new UserPreferences();
    Response response;

    try {
      Dio dio = await _api.dio(_pre, "application/json");

      response = await dio.post(
        "api/gest/registro/estadosubestados/${id.toString()}",
      );
      print("----->${response.toString()}");

      if (response.statusCode == 200) {
        _map["code"] = response.statusCode;
        _map["data"] = response.data;
        return _map;
      }
      return _map;
    } on DioError catch (e) {
      if (e.response == null) {
        _map["code"] = 0;

        return _map;
      } else if (e.response.statusCode == 401) {
        _map["code"] = e.response.statusCode;
        _map["data"] = "";

        return _map;
      } else {
        _map["code"] = 0;
        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }


}
