import 'package:dio/dio.dart';
import 'package:enotria/api/api_utils.dart';
import 'package:enotria/util/userPreferences.dart';

class ApiDni {
  Future<Map<String, dynamic>> getByDocument(String documentNumber) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    UserPreferences _pre = new UserPreferences();

    ApiUtils _api = new ApiUtils();

    Response _response;

    print(" ENVIANDO PARA BUSQUEDA DNI: ${documentNumber.toString()}");

    try {
      Dio dio = await _api.dio(_pre, "application/json");

      _response = await dio
          .post("api/gest/registro/documento/${documentNumber.toString()}");

      
      if (_response.statusCode == 200) {
        _map["code"] = _response.statusCode;

        _map["data"] = _response.data;

        return _map;
      }
      return _map;
    } on DioError catch (e) {
     
      if (e.response.statusCode == 401) {
       
        _map["code"] = e.response.statusCode;

        _map["data"] = "";

        return _map;
      } else {
       
        _map["code"] = 0;

        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }

  Future<Map<String, dynamic>> getPedido(String order) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    UserPreferences _pre = new UserPreferences();

    ApiUtils _api = new ApiUtils();

    Response _response;

    try {
      Dio dio = await _api.dio(_pre, "application/json");

      _response =
          await dio.post("api/gest/consulta/pedido/${order.toString()}");
     // print("---> RESPPONSE SEARCH: ${_response.data.toString()}");
      if (_response.statusCode == 200) {
        _map["code"] = _response.statusCode;

        _map["data"] = _response.data;

        return _map;
      }
      return _map;
    } on DioError catch (e) {
      if (e.response.statusCode == 401) {
        _map["code"] = e.response.statusCode;

        _map["data"] = "";

        return _map;
      } else {
        _map["code"] = 0;

        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }
}
