import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:enotria/api/api_utils.dart';
import 'package:enotria/util/userPreferences.dart';
import 'package:geolocator/geolocator.dart';

class ApiResumen {
  Future<Map<String, dynamic>> getPedido(String order) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    UserPreferences _pre = new UserPreferences();

    ApiUtils _api = new ApiUtils();

    Response _response;

    try {
      Dio dio = await _api.dio(_pre, "application/json");

      _response =
          await dio.post("api/gest/consulta/pedido/${order.toString()}");

      // print("----> rswponse: ${_response.toString()}");

      if (_response.statusCode == 200) {
        _map["code"] = _response.statusCode;

        _map["data"] = _response.data;

        return _map;
      }
      return _map;
    } on DioError catch (e) {
      if (e.response.statusCode == 401) {
        _map["code"] = e.response.statusCode;

        _map["data"] = "";

        return _map;
      } else {
        _map["code"] = 0;

        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }

  Future<String> sendFile(String id, String base64) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    UserPreferences _pre = new UserPreferences();

    ApiUtils _api = new ApiUtils();

    var formData = FormData.fromMap({
      'requestedOrderId': id,
      'documentStatusType': "ok",
      'drawableB64': base64
    });

    Response _response;

    try {
      Dio dio = await _api.dio(_pre, "application/json");

      _response = await dio.post("/api/gest/registro/cargo", data: formData);

      if (_response.statusCode == 200) {
        print("RESPONSE:::::::= ${_response.toString()}");
        _map["code"] = _response.statusCode;

        _map["data"] = _response.data;

        String name =
            (_map["data"]["fileName"] == "") ? "err" : _map["data"]["fileName"];

        return name;
      }
      return "";
    } on DioError catch (e) {
      if (e.response?.statusCode == 401) {
        _map["code"] = e.response.statusCode;

        _map["data"] = "";

        return "";
      } else {
        _map["code"] = 0;

        _map["data"] = "Conexion fallida";

        return "";
      }
    }
  }

  static String formatISOTime(DateTime date) {
    var duration = date.timeZoneOffset;
    if (duration.isNegative)
      return (date.toIso8601String());
    else
      return (date.toIso8601String());
  }

  static String jsonCodigoUnico(
    String idpedido,
    String latitud,
    String longitud,
    String statusType,
    String subStatusType,
    String date,
    //status biometria
    String statusTypeCodeBio,
    String statusTypeBio,
    String subStatusTypeCodeBio,
    String subStatusTypeBio,
    String deliveryDateBio,
    String cargoPathReniec,
    String fullName,
    String documentType,
    String documentId,
    List<dynamic> jsonCargos,
    List<dynamic> jsonEvidencia,
    Position position,
    String codEstado,
    String codSubEstado,
  ) {
    var d = jsonEncode({
      "codigoUnico": "${idpedido.toString()}",
      "imagesCargo": [
        "${jsonCargos[0].toString()}",
        "${jsonCargos[1].toString()}",
        "${jsonCargos[2].toString()}",
        "${jsonCargos[3].toString()}"
      ],
      "imagesSupport": [
        "${jsonEvidencia[0].toString()}",
        "${jsonEvidencia[1].toString()}",
        "${jsonEvidencia[2].toString()}"
      ],
      "latitud": "${position.latitude}",
      "longitud": "${position.longitude}",
      "deliveryVisits": [
        {
          "statusTypeCode": "${codEstado.toString()}",
          "subStatusTypeCode": "${codSubEstado.toString()}",
          "statusType": "${statusType.toString()}",
          "subStatusType": "${subStatusType.toString()}",
          "deliveryDate": "${formatISOTime(DateTime.now())}",
          "statusTypeCodeBio": statusTypeCodeBio, //"BIO",
          "statusTypeBio": statusTypeBio, //"Biommeria",
          "subStatusTypeCodeBio": subStatusTypeCodeBio, //"NO_HIT"
          "subStatusTypeBio":
              subStatusTypeBio, //"Huella no coincide con Reniec",
          "deliveryDateBio": deliveryDateBio, //"2021-12-18T14:45:11.893419",
          "cargoPathReniec": cargoPathReniec
          // "eno.bucket.biometria/20210802/000027LEV111219_cargo_ok_154511.jpg"
        }
      ],
      "deliveryPerson": {
        "fullName": "${fullName.toString()}",
        "relative": "",
        "documentType": "${documentType.toString()}",
        "documentId": "${documentId.toString()}"
      }
    });

    return d;
  }

  static String jsonPedido(
    String idpedido,
    String latitud,
    String longitud,
    String statusType,
    String subStatusType,
    String date,
    String fullName,
    String documentType,
    String documentId,
    List<dynamic> jsonCargos,
    List<dynamic> jsonEvidencia,
    Position position,
    String codEstado,
    String codSubEstado,
  ) {
    var d = jsonEncode({
      "idpedido": "${idpedido.toString()}",
      "imagesCargo": [
        "${jsonCargos[0].toString()}",
        "${jsonCargos[1].toString()}",
        "${jsonCargos[2].toString()}",
        "${jsonCargos[3].toString()}"
      ],
      "imagesSupport": [
        "${jsonEvidencia[0].toString()}",
        "${jsonEvidencia[1].toString()}",
        "${jsonEvidencia[2].toString()}"
      ],
      "latitud": "${position.latitude}",
      "longitud": "${position.longitude}",
      "deliveryVisits": [
        {
          "statusTypeCode": "${codEstado.toString()}",
          "subStatusTypeCode": "${codSubEstado.toString()}",
          "statusType": "${statusType.toString()}",
          "subStatusType": "${subStatusType.toString()}",
          "deliveryDate": "${formatISOTime(DateTime.now())}"
        }
      ],
      "deliveryPerson": {
        "fullName": "${fullName.toString()}",
        "relative": "",
        "documentType": "${documentType.toString()}",
        "documentId": "${documentId.toString()}"
      }
    });

    return d;
  }

  Future<Map<String, dynamic>> resumenFinalize(
    String idpedido,
    String latitud,
    String longitud,
    String statusType,
    String subStatusType,
    String date,
    //status biometria
    String statusTypeCodeBio,
    String statusTypeBio,
    String subStatusTypeCodeBio,
    String subStatusTypeBio,
    String deliveryDateBio,
    String cargoPathReniec,
    //
    String fullName,
    String documentType,
    String documentId,
    List<dynamic> jsonCargos,
    List<dynamic> jsonEvidencia,
    Position position,
    String codEstado,
    String codSubEstado,
  ) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    Response response;

    UserPreferences _pre = new UserPreferences();

    ApiUtils _api = new ApiUtils();

    Dio dio = await _api.dio(_pre, "application/json");

    // ignore: unused_local_variable
    String d = formatISOTime(DateTime.now());

    var body = "";
    var type = idpedido.substring(0, 3);

    if (type == "PED") {
      body = jsonPedido(
        idpedido,
        latitud,
        longitud,
        statusType,
        subStatusType,
        date,
        fullName,
        documentType,
        documentId,
        jsonCargos,
        jsonEvidencia,
        position,
        codEstado,
        codSubEstado,
      );
    } else {
      body = jsonCodigoUnico(
        idpedido,
        latitud,
        longitud,
        statusType,
        subStatusType,
        date,
        //status biometria
        statusTypeCodeBio,
        statusTypeBio,
        subStatusTypeCodeBio,
        subStatusTypeBio,
        deliveryDateBio,
        cargoPathReniec,
        fullName,
        documentType,
        documentId,
        jsonCargos,
        jsonEvidencia,
        position,
        codEstado,
        codSubEstado,
      );
    }
    print(" jsonEnviando--->: --->${body.toString()}");

    try {
      response = await dio.post("api/gest/registro/entrega", data: body);
      print("PEDIDO ENTREGADO: ${response.toString()}");
      if (response.statusCode == 200) {
        _map["code"] = response.statusCode;
        _map["data"] = response.data;
        return _map;
      }
      return _map;
    } on DioError catch (e) {
      if (e.response.statusCode == 401) {
        _map["code"] = e.response.statusCode;
        _map["data"] = "";

        return _map;
      } else {
        _map["code"] = 0;
        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }

  Future<Map<String, dynamic>> updateImageCodigoUnico({
    String idCodigoUnico,
    List<dynamic> jsonCargos,
    List<dynamic> jsonEvidencia,
  }) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    Response response;

    UserPreferences _pre = new UserPreferences();

    ApiUtils _api = new ApiUtils();

    Dio dio = await _api.dio(_pre, "application/json");

    var body = jsonEncode({
      "codigoUnico": "${idCodigoUnico.toString()}",
      "imagesCargo": [
        "${jsonCargos[0].toString()}",
        "${jsonCargos[1].toString()}",
        "${jsonCargos[2].toString()}",
        "${jsonCargos[3].toString()}"
      ],
      "imagesSupport": [
        "${jsonEvidencia[0].toString()}",
        "${jsonEvidencia[1].toString()}",
        "${jsonEvidencia[2].toString()}"
      ]
    });

    print("...... enviando json actualizar: ${body.toString()}");

    try {
      response = await dio.post("api/gest/registro/updateImageCodigoUnico",
          data: body);
      print("PEDIDO ENTREGADO::::::::::::: ${response.toString()}");
      if (response.statusCode == 200) {
        _map["code"] = response.statusCode;
        _map["data"] = response.data;
        return _map;
      }
      return _map;
    } on DioError catch (e) {
      if (e.response.statusCode == 401) {
        _map["code"] = e.response.statusCode;
        _map["data"] = "";

        return _map;
      } else {
        _map["code"] = 0;
        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }

  Future<Map<String, dynamic>> updateImagePedido({
    String idCodigoUnico,
    List<dynamic> jsonCargos,
    List<dynamic> jsonEvidencia,
  }) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    Response response;

    UserPreferences _pre = new UserPreferences();

    ApiUtils _api = new ApiUtils();

    Dio dio = await _api.dio(_pre, "application/json");

    var body = jsonEncode({
      "idpedido": "${idCodigoUnico.toString()}",
      "imagesCargo": [
        "${jsonCargos[0].toString()}",
        "${jsonCargos[1].toString()}",
        "${jsonCargos[2].toString()}",
        "${jsonCargos[3].toString()}"
      ],
      "imagesSupport": [
        "${jsonEvidencia[0].toString()}",
        "${jsonEvidencia[1].toString()}",
        "${jsonEvidencia[2].toString()}"
      ]
    });

    print("...... enviando json actualizar: ${body.toString()}");

    try {
      response =
          await dio.post("api/gest/registro/updateImagePedido", data: body);
      print("PEDIDO ENTREGADO::::::::::::: ${response.toString()}");
      if (response.statusCode == 200) {
        _map["code"] = response.statusCode;
        _map["data"] = response.data;
        return _map;
      }
      return _map;
    } on DioError catch (e) {
      if (e.response.statusCode == 401) {
        _map["code"] = e.response.statusCode;
        _map["data"] = "";

        return _map;
      } else {
        _map["code"] = 0;
        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }
}
