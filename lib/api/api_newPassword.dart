import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:enotria/api/api_utils.dart';
import 'package:enotria/util/userPreferences.dart';

class ApiNewPassword {
  Future<Map<String, dynamic>> newPassword({
    String oldPasswd,
    String password1,
    String password2,
  }) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();
    ApiUtils _api = new ApiUtils();
    UserPreferences _pre = new UserPreferences();
    Response response;
    var body = jsonEncode({
      "oldPasswd": "${oldPasswd.toString()}",
      "passwd1": "${password1.toString()}",
      "passwd2": "${password2.toString()}"
    });
    print(body);
    try {
      Dio dio = await _api.dio(_pre, "application/json");

      response = await dio.post(
        "api/gest/registro/updatepassword",
        data: body,
      );
      print("----> response: ${response.toString()}");

      if (response.statusCode == 200) {
        _map["code"] = response.statusCode;
        _map["data"] = response.data;
        return _map;
      }
      return _map;
    } on DioError catch (e) {
      if (e.response == null) {
        _map["code"] = 0;

        return _map;
      } else if (e.response.statusCode == 401) {
        _map["code"] = e.response.statusCode;
        _map["data"] = "";

        return _map;
      } else {
        _map["code"] = 0;
        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }
}
