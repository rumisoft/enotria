import 'dart:io';

import 'package:dio/dio.dart';
import 'package:enotria/util/constast.dart';
import 'package:enotria/util/userPreferences.dart';

class ApiUtils {
  Future<Dio> dio(UserPreferences _pre, String contentType) async {
    Map<String, dynamic> headerForm = {
      "x-key-app":
          //"02:15:B2:00:00:00",
        // "74:15:75:78:38:4f",
        tipoApk == "develop" ? "02:15:B2:00:00:00" : _pre.mac,
      "Authorization": _pre.token
    };

    BaseOptions optionsForm = new BaseOptions(
        baseUrl: (tipoApk == "develop")
            ? "http://enotria.evolverapps.com/"
            //"http://192.168.1.150/Entrega.Api.Gest/" 
            : "https://biometria.e-simple.pe/",
        //"http://enotria.evolverapps.com/",
       // : "https://biometriaprod.e-simple.pe/",
        connectTimeout: 9999999000 * 5,
        receiveTimeout: 9999999000 * 5,
        contentType: contentType,
        headers: headerForm);

    Dio dio = new Dio(optionsForm);

    return dio;
  }
}

class ApiUtils2 {
  Future<Dio> dio(String contentType) async {
    Map<String, dynamic> headerForm = {
      HttpHeaders.contentTypeHeader: "application/json",
    };

    BaseOptions optionsForm = new BaseOptions(
        baseUrl:
            "https://localhost:44315/api/", //http://localhost/Entrega.Api.Gest/
        connectTimeout: 9999999000 * 5,
        receiveTimeout: 9999999000 * 5,
        // contentType: contentType,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
        }
        //  headers: headerForm
        );

    Dio dio = new Dio(optionsForm);

    return dio;
  }
}
