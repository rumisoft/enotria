import 'dart:convert';
import 'package:enotria/model/consulta_reniec/consultaBiometriaReniecModel.dart';
import "package:http/http.dart" as http;

import 'package:dio/dio.dart';
import 'package:enotria/api/api_utils.dart';
import 'package:enotria/util/userPreferences.dart';

class ApiConsultaDatosReniec {
  final urlBase = "https://biometria.e-simple.pe";
  UserPreferences pre = UserPreferences();

  //consulta de datos personales
  Future<Map<String, dynamic>> getConsultaDatosPersonalesReniec(
      String dni) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    var resp;
    var decodedData;
    var token = pre.token;

    Map<String, dynamic> body = {
      "dniuser": dni,
    };

    try {
      final url = "$urlBase/api/gest/consulta/consultardatos";
      resp = await http.post(Uri.parse(url),
          headers: {
            "Authorization": token,
            "Content-Type": "application/json",
            "Accept": "application/json"
          },
          body: jsonEncode(body));

      decodedData = json.decode(resp.body);
      print("consulta dni--- $decodedData");

      if (resp.statusCode == 200) {
        //_map["code"] = _response.statusCode;
        _map["data"] = decodedData;
      }

      return _map["data"];
    } catch (e) {
      print(e);
      _map["error"] = decodedData;

      return _map["error"];
    }
  }

  //consulta de mejor huella
  Future<Map<String, dynamic>> getMejorHuellaReniecDio(String dni) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    UserPreferences _pre = new UserPreferences();
    ApiUtils _api = new ApiUtils();
    Response _response;

    try {
      Dio dio = await _api.dio(_pre, "application/json");

      _response = await dio.post(
        "api/gest/consulta/mejorhuella",
        data: jsonEncode({"dni": dni}),
      );

      print("----> response mejor huella: ${_response.toString()}");

      if (_response.statusCode == 200) {
        _map["code"] = _response.statusCode;

        _map["data"] = _response.data;
      }
      return _map;
    } on DioError catch (e) {
      if (e.response.statusCode == 401) {
        _map["code"] = e.response.statusCode;

        _map["data"] = "";

        return _map;
      } else if (e.response.statusCode == 500) {
        _map["code"] = 1;

        _map["data"] = "No se envió";

        return _map;
      } else {
        _map["code"] = 0;

        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }

//consulta de biometria
  Future<Map<String, dynamic>> getBiometriaWsqReniec(
      String dni, huellaDer, huellaIzq) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();
    var token = pre.token;

    var resp;
    var decodedData;

    Map<String, dynamic> body = {
      "wsqright": huellaDer,
      "wsqleft": huellaIzq,
      "dniuser": dni
      //"72676295",
    };

    try {
      final url = "$urlBase/api/gest/consulta/identidadwsq";
      resp = await http.post(Uri.parse(url),
          headers: {
            "Authorization": token,
            "Content-Type": "application/json",
            "Accept": "application/json"
          },
          body: jsonEncode(body));

      decodedData = json.decode(resp.body);
      print(" huella--- $decodedData");

      if (resp.statusCode == 200) {
        //_map["code"] = _response.statusCode;
        _map["data"] = decodedData;
      }

      return _map["data"];
    } catch (e) {
      print(e);
      _map["error"] = decodedData;

      return _map["error"];
    }
  }

//consulta de biometria Wsq
  Future<Map<String, dynamic>> getMejorHuellaReniec(String dni) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    var resp;
    var decodedData;
    var token = pre.token;

    Map<String, dynamic> body = {
      "dni": dni,
    };

    try {
      final url = "$urlBase/api/gest/consulta/mejorhuella",
          resp = await http.post(Uri.parse(url),
              headers: {
                "Authorization": token,
                "Content-Type": "application/json",
                "Accept": "application/json"
              },
              body: jsonEncode(body));

      decodedData = json.decode(resp.body);
      print("mejor huella--- $decodedData");

      if (resp.statusCode == 200) {
        //_map["code"] = _response.statusCode;
        _map["data"] = decodedData;
      }

      return _map["data"];
    } catch (e) {
      print(e);
      return _map["error"] = decodedData;
    }
  }
}
