import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:enotria/api/api_utils.dart';
import 'package:enotria/util/userPreferences.dart';

class ApiLogDataReniec {
  Future<Map<String, dynamic>> enviarDataLogReniec(logData) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();
    UserPreferences _pre = new UserPreferences();
    ApiUtils _api = new ApiUtils();
    Response _response;
    DateTime horaConsulta = DateTime.now();

    try {
      Dio dio = await _api.dio(_pre, "application/json");

      _response = await dio.post(
        "api/gest/registro/registroDataReniec",
        data: jsonEncode({
          "type": logData.type,
          "datetime": horaConsulta.toString(),
          "data": logData.toJson()
        }),
      );

    // print("----> response log ${logData.type}: ${_response.toString()}");

      if (_response.statusCode == 200) {
        _map["code"] = _response.statusCode;

        _map["data"] = _response.data;
        // _response.data["_id"] != "" ? "ok" : "error";
     print(
            "----> response cargo ${_map["data"]["data"]["cargoPathReniec"]}");


      }
      return _map;
    } on DioError catch (e) {
      if (e.response.statusCode == 401) {
        _map["code"] = e.response.statusCode;

        _map["data"] = "";

        return _map;
      } else if (e.response.statusCode == 500) {
        _map["code"] = 1;

        _map["data"] = "No se envió";

        return _map;
      } else {
        _map["code"] = 0;

        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }

  Future<Map<String, dynamic>> firmarPdfReniec(String codPedido, String dni,String pdf64) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    UserPreferences _pre = new UserPreferences();
    ApiUtils _api = new ApiUtils();
    Response _response;

    try {
      Dio dio = await _api.dio(_pre, "application/json");

      _response = await dio.post(
        "api/gest/registro/firmarPdf",
        data: jsonEncode({
          "filename": "CARGO_BIOMETRIA",
          "filesize": 0,
          "dBase64": pdf64,
          "codPedido": codPedido,
           "dni":dni
        }),
      );

      print("----> response envio pdf: ${_response.toString()}");

      if (_response.statusCode == 200) {
        _map["code"] = _response.statusCode;

        _map["data"] = _response.data;
        // _response.data["_id"] != "" ? "ok" : "error";

      }
      return _map;
    } on DioError catch (e) {
      if (e.response.statusCode == 401) {
        _map["code"] = e.response.statusCode;

        _map["data"] = "";

        return _map;
      } else if (e.response.statusCode == 500) {
        _map["code"] = 1;

        _map["data"] = "No se envió";

        return _map;
      } else {
        _map["code"] = 0;

        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }
}
