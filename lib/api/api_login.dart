import 'package:dio/dio.dart';
import 'package:enotria/api/api_utils.dart';
import 'package:enotria/util/userPreferences.dart';

class ApiLogin {
  Future<Map<String, dynamic>> userAuth(String user, String password) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    //instancia que utiliza funcion con el paquete dio
    ApiUtils _api = new ApiUtils();
    UserPreferences _pre = new UserPreferences();
    Response response;
    Map<String, dynamic> body = {
      "username": user,
      "password": password,
    };
    print("__1___>");
    print(" -> $body");
    try {
      Dio dio = await _api.dio(_pre, "application/x-www-form-urlencoded");

      response = await dio.post(
        "api/gest/login/authenticate",
        data: body,
      );
      print("__2___>");
      print("$response");
      print("$dio");
      print("${dio.options.baseUrl} ");
      print("${dio.options.contentType} ");
      print("${dio.options.queryParameters} ");
      print("$response");
      if (response.statusCode == 200) {
        _map["code"] = response.statusCode;
        _map["data"] = response.data;
        return _map;
      }
      return _map;
    } on DioError catch (e) {
      print(" exeption: $e");
      if (e.response == null) {
        _map["code"] = 0;
        return _map;
      } else if (e.response.statusCode == 401) {
        _map["code"] = e.response.statusCode;
        _map["data"] = "";
        return _map;
      } else {
        _map["code"] = 0;
        _map["data"] = "Conexion fallida";
        return _map;
      }
    }
  }
}
