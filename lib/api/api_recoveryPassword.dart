import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:enotria/api/api_utils.dart';
import 'package:enotria/util/userPreferences.dart';

class RecoveryPassword {
  Future<Map<String, dynamic>> validatePassword({String user}) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();
    ApiUtils _api = new ApiUtils();
    UserPreferences _pre = new UserPreferences();
    Response response;
    var body = jsonEncode({
      "username": "${user.toString()}",
      "type": 2,
    });
    print(body);
    try {
      Dio dio = await _api.dio(_pre, "application/json");
      response = await dio.post(
        "api/gest/registro/recoveryPasswordValidateUser",
        data: body,
      );
      print("----> response: ${response.toString()}");

      if (response.statusCode == 200 || response.statusCode == 500) {
        _map["code"] = response.statusCode;
        _map["data"] = response.data;
        return _map;
      }
      return _map;
    } on DioError catch (e) {
      print("--exeption--> response: ${e.message}");
      _map["code"] = 0;
      _map["data"] = e.message;

      return _map;
    }
  }

  Future<Map<String, dynamic>> validateCode({String code, String user}) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();
    ApiUtils _api = new ApiUtils();
    UserPreferences _pre = new UserPreferences();
    Response response;

    try {
      Dio dio = await _api.dio(_pre, "application/json");
      response = await dio
          .post("api/gest/registro/recoveryPasswordValidateMsg/$user/$code");
      print("----> response: ${response.toString()}");

      if (response.statusCode == 200 || response.statusCode == 500) {
        _map["code"] = response.statusCode;
        _map["data"] = response.data;
        return _map;
      }
      return _map;
    } on DioError catch (e) {
      print("--exeption--> response: ${e.message}");
      _map["code"] = 0;
      _map["data"] = e.message;

      return _map;
    }
  }

  Future<Map<String, dynamic>> newPassword({
    String hash,
    String pass1,
    String pass2,
  }) async {
    Map<String, dynamic> _map = new Map<String, dynamic>();
    ApiUtils _api = new ApiUtils();
    UserPreferences _pre = new UserPreferences();
    Response response;

    var body = jsonEncode(
        {"passwd1": "${pass1.toString()}", "passwd2": "${pass2.toString()}"});
    print("JSON:---> ${body.toString()}");

    try {
      Dio dio = await _api.dio(_pre, "application/json");
      response = await dio.post(
        "api/gest/registro/recoveryNewPassword/${hash.toString()}",
        data: body,
      );
      print("----> response: ${response.toString()}");

      if (response.statusCode == 200 || response.statusCode == 500) {
        _map["code"] = response.statusCode;
        _map["data"] = response.data;
        return _map;
      }
      return _map;
    } on DioError catch (e) {
      print("--exeption--> response: ${e.message}");
      _map["code"] = 0;
      _map["data"] = e.message;

      return _map;
    }
  }
}
