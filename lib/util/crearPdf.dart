import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:enotria/model/consulta_reniec/consultaDatosPersonalesReniecModel.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

class CreatePDf {
  pw.Document pdf;
  PdfImage imagen;
  var pdfBase64;

  Future<String> generarPdf1(
      String resultado,
      ConsultaDatosPersonalesReniecModel consultaDatosPersonalesModel,
      horaFecha) async {
    pdf = pw.Document();

    final ByteData bytes = await rootBundle.load('assets/png/reniec_logo.png');
    final Uint8List byteList = bytes.buffer.asUint8List();

    final ByteData bytes2 = await rootBundle.load('assets/png/logo.png');
    final Uint8List byteList2 = bytes2.buffer.asUint8List();

    // imagen = PdfImage.file(
    //   pdf.document,
    //   bytes: (await rootBundle.load('assets/png/logo.png')).buffer.asUint8List(),
    // );

    pdf.addPage(
      pw.MultiPage(
        pageFormat: PdfPageFormat.a4,
        margin: pw.EdgeInsets.all(40),
        build: (context) => [
          pw.Padding(
            padding: pw.EdgeInsets.symmetric(horizontal: 4),
            child: pw.Row(
              mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
              children: [
                pw.Image(
                  pw.MemoryImage(
                    byteList,
                  ),
                  height: 90,
                  width: 90,
                  //fit: pw.BoxFit.contain
                ),
                pw.Image(
                  pw.MemoryImage(
                    byteList2,
                  ),
                  height: 80,
                  width: 80,
                  //fit: pw.BoxFit.contain
                ),
              ],
            ),
          ),
          pw.SizedBox(
            height: 10,
          ),
          pw.Padding(
            padding: pw.EdgeInsets.symmetric(vertical: 10),
            child: pw.Center(
              child: pw.Text(
                'ENOTRIA',
                style: pw.TextStyle(
                  fontSize: 10,
                  color: PdfColors.black,
                  fontWeight: pw.FontWeight.bold,
                ),
                textAlign: pw.TextAlign.center,
              ),
            ),
          ),

          pw.Padding(
            padding: pw.EdgeInsets.symmetric(vertical: 2),
            child: pw.Center(
              child: pw.Text(
                'CARGO DE CONSULTA DE VERIFICACIÓN BIOMÉTRICA CON RENIEC',
                style: pw.TextStyle(
                  fontSize: 10,
                  color: PdfColors.black,
                  fontWeight: pw.FontWeight.bold,
                ),
                textAlign: pw.TextAlign.center,
              ),
            ),
          ),
          pw.Divider(thickness: 0.7),
          pw.SizedBox(height: 15),

          pw.Padding(
            padding: pw.EdgeInsets.symmetric(vertical: 10, horizontal: 40),
            child: pw.Container(
              //color: PdfColors.amber,
              child: pw.Column(
                  crossAxisAlignment: pw.CrossAxisAlignment.start,
                  children: [
                    pw.Text(
                      'INFORMACIÓN DE CONSULTA',
                      style: pw.TextStyle(
                        fontSize: 10,
                        color: PdfColors.black,
                        fontWeight: pw.FontWeight.bold,
                      ),
                      // textAlign: pw.TextAlign.center,
                    ),
                    pw.SizedBox(height: 30),
                    pw.Text(
                      'Fecha de transacción: $horaFecha',
                      style: pw.TextStyle(
                        fontSize: 9.5,
                        color: PdfColors.black,
                      ),
                    ),
                    pw.SizedBox(height: 10),
                    pw.Text(
                      'Entidad: Enotria',
                      style: pw.TextStyle(
                        fontSize: 9.5,
                        color: PdfColors.black,
                      ),
                      textAlign: pw.TextAlign.right,
                    ),
                    pw.SizedBox(height: 10),
                    pw.Text(
                      'Resultado: $resultado',
                      style: pw.TextStyle(
                        fontSize: 9.5,
                        color: PdfColors.black,
                      ),
                      textAlign: pw.TextAlign.right,
                    ),
                    pw.SizedBox(height: 35),
                    pw.Text(
                      'INFORMACIÓN DE PERSONA',
                      style: pw.TextStyle(
                        fontSize: 10,
                        color: PdfColors.black,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                    pw.SizedBox(height: 30),
                    pw.Text(
                      'DNI: ${consultaDatosPersonalesModel.dni}',
                      style: pw.TextStyle(
                        fontSize: 9.5,
                        color: PdfColors.black,
                      ),
                      textAlign: pw.TextAlign.left,
                    ),
                    pw.SizedBox(height: 10),
                    pw.Text(
                      'Apellido Paterno: ${consultaDatosPersonalesModel.primerApellido}',
                      style: pw.TextStyle(
                        fontSize: 9.5,
                        color: PdfColors.black,
                      ),
                      textAlign: pw.TextAlign.left,
                    ),
                    pw.SizedBox(height: 10),
                    pw.Text(
                      'Apellido Materno: ${consultaDatosPersonalesModel.segundoApellido}',
                      style: pw.TextStyle(
                        fontSize: 9.5,
                        color: PdfColors.black,
                      ),
                      textAlign: pw.TextAlign.left,
                    ),
                    pw.SizedBox(height: 10),
                    pw.Text(
                      'Nombres: ${consultaDatosPersonalesModel.prenombres} ',
                      style: pw.TextStyle(
                        fontSize: 9.5,
                        color: PdfColors.black,
                      ),
                      textAlign: pw.TextAlign.left,
                    ),
                    pw.SizedBox(height: 10),
                    pw.Text(
                      'Género: ${consultaDatosPersonalesModel.genero}',
                      style: pw.TextStyle(
                        fontSize: 9.5,
                        color: PdfColors.black,
                      ),
                      textAlign: pw.TextAlign.left,
                    ),
                    pw.SizedBox(height: 10),
                    pw.Text(
                      'Fecha de Nacimiento: ${consultaDatosPersonalesModel.fechaNacimiento}',
                      style: pw.TextStyle(
                        fontSize: 9.5,
                        color: PdfColors.black,
                      ),
                      textAlign: pw.TextAlign.left,
                    ),
                    pw.SizedBox(height: 10),
                    pw.Text(
                      'Estado Civil: ${consultaDatosPersonalesModel.estadoCivil}',
                      style: pw.TextStyle(
                        fontSize: 9.5,
                        color: PdfColors.black,
                      ),
                      textAlign: pw.TextAlign.left,
                    ),
                    pw.SizedBox(height: 10),
                    pw.Text(
                      'Restricción: ${consultaDatosPersonalesModel.restriccion}',
                      style: pw.TextStyle(
                        fontSize: 9.5,
                        color: PdfColors.black,
                      ),
                      textAlign: pw.TextAlign.left,
                    ),
                  ]),
            ),
          ),

          //footer
          pw.SizedBox(
            height: 190,
          ),
          pw.Column(mainAxisAlignment: pw.MainAxisAlignment.end, children: [
            pw.Divider(thickness: 0.7),
            pw.Padding(
              padding: pw.EdgeInsets.symmetric(horizontal: 40),
              child: pw.Text(
                "La presente consulta de datos fue realizada por la empresa ENOTRIA a través del Convenio suscrito con el Registro Nacional de Identificación y Estado Civil - RENIEC.",
                style: pw.TextStyle(
                  fontSize: 8.5,
                ),
                textAlign: pw.TextAlign.justify,
              ),
            )
          ]),
        ],
      ),
    );
    await savePdf();
    //print(pdfBase64);
    return pdfBase64;
  }

  Future<void> savePdf() async {
    final String dir = (await getApplicationDocumentsDirectory()).path;
    final String path = '$dir/cargobiometria.pdf';
    final File file = File(path);
    await file.writeAsBytes(await pdf.save());

    pdf = pw.Document();
    //print("ppp ${pdf.toString()}");

    pdfBase64 = await convertToBytes(file);
    print("se creo pdf");
  }
}

Future<String> convertToBytes(File file) async {
  List<int> pdfBytes = await file.readAsBytes();
  final pdfFile = base64Encode(pdfBytes);
  //print(pdfFile);
  return pdfFile;
}
