import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  static final UserPreferences _instancia = new UserPreferences._internal();

  factory UserPreferences() {
    return _instancia;
  }

  UserPreferences._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

 
//-->
  String get user {
    return _prefs.getString('user') ?? '';
  }

  set user(String value) {
    _prefs.setString('user', value);
  }

//-->
  String get token {
    return _prefs.getString('token') ?? '';
  }

  set token(String value) {
    _prefs.setString('token', value);
  }

//-->
  String get lastName {
    return _prefs.getString('lastName') ?? '';
  }

  set lastName(String value) {
    _prefs.setString('lastName', value);
  }

//-->
  String get firstName {
    return _prefs.getString('firstName') ?? '';
  }

  set firstName(String value) {
    _prefs.setString('firstName', value);
  }

//-->
  int get userType {
    return _prefs.getInt('userType') ?? 0;
  }

  set userType(value) {
    _prefs.setInt('userType', value);
  }

//-->
  String get courierCode {
    return _prefs.getString('courierCode') ?? '';
  }

  set courierCode(value) {
    _prefs.setString('courierCode', value??'-');
  }

//-->
  int get needsPasswdChange {
    return _prefs.getInt('needsPasswdChange') ?? 0;
  }

  set needsPasswdChange(value) {
    _prefs.setInt('needsPasswdChange', value);
  }

  //-->
  String get downtime {
    return _prefs.getString('downtime') ?? "";
  }

  set downtime(value) {
    _prefs.setString('downtime', value);
  }

  //-->
  String get consultationTime {
    return _prefs.getString('consultationTime') ?? "";
  }

  set consultationTime(value) {
    _prefs.setString('consultationTime', value);
  }
  //-->
  String get mac {
    return _prefs.getString('consultationTime') ?? "";
  }

  set mac(value) {
    _prefs.setString('consultationTime', value);
  }
  //-->
   String get app {
    return _prefs.getString('app') ?? "";
  }

  set app(value) {
    _prefs.setString('app', value);
  }

  //-->
  int get timeOuth {
    return _prefs.getInt('timeOuth') ?? 0;
  }

  set timeOuth(value) {
    _prefs.setString('timeOuth', value);
  }

}
