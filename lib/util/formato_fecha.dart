import 'package:intl/intl.dart';

bool obtenerHoraParaBloqueo(String date) {
  if (date != '0' && date != null) {
    var horaregistro = DateTime.parse(date);
    //para encontrar los minutos (10) después de la hora actual
    DateTime diezmindesdeahora =
        horaregistro.add(Duration(minutes: int.parse("dd")));
    // print(diezmindesdeahora);

    //hora actual
    DateTime horaActual = DateTime.now();

    bool comprobarhora = diezmindesdeahora.isBefore(horaActual);

    // if (!comprobarhora) {
    //   print("todavia puede guardar");
    // } else {
    //   print("error!! se pasó de 10 minutos");
    // }

    //retorna false si todavia se puede guardar
    // print('result $comprobarhora');
    return comprobarhora;
  }
  return false;
}

obtenerFechaHora( date) {
  if (date != '0') {
    //var fecha = DateTime.parse(date);

    final DateFormat fech = new DateFormat('dd/MM/yyyy HH:mm:ss', 'es');

    return fech.format(date);
  } else {
    return 0;
  }
}

obtenerFechaHoracorta(String date) {
  if (date != '0' && date != null) {
    var fecha = DateTime.parse(date);

    final DateFormat fech = new DateFormat('yyyy-MM-dd HH:mm:ss', 'es');

    return fech.format(fecha);
  } else {
    return '0';
  }
}

obtenerFechaHoraFoto(String date) {
  if (date != '0' && date != null) {
    var fecha = DateTime.parse(date);

    final DateFormat fech = new DateFormat('yyyyMMddHH', 'es');

    return fech.format(fecha);
  } else {
    return '0';
  }
}

obtenerFechaHoraDate(DateTime fecha) {
  final DateFormat fech = new DateFormat('yyyy-MM-dd-HHmmss', 'es');

  return fech.format(fecha);
}
