package com.enotriasa.enotria;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import android.widget.Toast;
import androidx.annotation.NonNull;
import com.futronictech.FPScan;
import com.futronictech.Scanner;
import com.futronictech.UsbDeviceDataExchangeImpl;
import com.gemalto.wsq.WSQEncoder;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {

    /**
     * Called when the activity is first created.
     */
    private static Button mButtonScan;
    private static Button mButtonStop;
    private Button mButtonSave;

    public static boolean mStop = false;
    public static boolean mFrame = true;
    public static boolean mInvertImage = false;

    public static final int MESSAGE_SHOW_MSG = 1;
    public static final int MESSAGE_SHOW_SCANNER_INFO = 2;
    public static final int MESSAGE_SHOW_IMAGE = 3;
    public static final int MESSAGE_ERROR = 4;
    public static final int MESSAGE_TRACE = 5;

    public static byte[] mImageFP = null;
    public static Object mSyncObj = new Object();

    public static int mImageWidth = 0;
    public static int mImageHeight = 0;
    private static int[] mPixels = null;
    private static Bitmap mBitmapFP = null;
    private static Canvas mCanvas = null;
    private static Paint mPaint = null;

    private FPScan mFPScan = null;
    private static byte[] mBitmapFP_b;
    private static byte[] mBitmapFP_c;
    private static byte[] wsqData;
    MethodChannel.Result resultado;
    boolean methodCheckFlag = true;
    boolean checkFlagDenie = true;
    boolean checkNoOpen = true;

    public static boolean mUsbHostMode = false;

    // Intent request codes
    private static final int REQUEST_FILE_FORMAT = 1;
    private UsbDeviceDataExchangeImpl usb_host_ctx = null;
    private File SyncDir = null;

    Timer timer = new Timer();
    Timer timer1 = new Timer();
    int contador = 0;
    String method="";

    String CHANNEL = "com.enotriasa/biometria_platform_channel";
    MethodChannel methodChannel2;
    private static Bitmap newBitmapFP = null;

    // inicializa los parametros de la huella
    public static void InitFingerPictureParameters(int wight, int height) {
        mImageWidth = wight;
        mImageHeight = height;
        // raw bytes[] sin procesar
        mImageFP = new byte[MainActivity.mImageWidth * MainActivity.mImageHeight];
        mPixels = new int[MainActivity.mImageWidth * MainActivity.mImageHeight];
        // Mapa de bits
        mBitmapFP = Bitmap.createBitmap(512, 512, Bitmap.Config.RGB_565);
        mCanvas = new Canvas(mBitmapFP);
        mPaint = new Paint();

        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);

       /* ColorMatrixColorFilter f = new ColorMatrixColorFilter(new float[]{
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
                1, 0, 0, 0, 0
        });*/
        mPaint.setColorFilter(f);
    }

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);

        BinaryMessenger message2 = flutterEngine.getDartExecutor().getBinaryMessenger();
        // definir una instancia de method channel
        methodChannel2 = new MethodChannel(message2, CHANNEL);
        methodChannel2.setMethodCallHandler((MethodCall call, MethodChannel.Result result) -> {
            //llama a la clase que modifica el method result
            resultado = new MethodResultWrapper(result);

           //final Map<String, Object> arguments = call.arguments();
            if (call.method.equals("abrirScaner")) {
                // iniciar actividad del huellero
               iniciarScan();

            } else if (call.method.equals("cerrarScaner")){
                detenerScan();
                System.out.println("deteniendo scannn.....");
                if (mImageFP != null)
                    resultado.success(GetWsq());
                     //methodChannel2.invokeMethod("methodCallback", GetWsq());
            }

            else if (call.method.equals("version")) {
                String version = getAndroidVersion();
                if (version != "11") {
                    result.success(version);
                } else {
                    result.notImplemented();
                }
            }
        });
    }



    //segundo metodo del channel: retorna la version de android
    String getAndroidVersion() {
        int sdkVersion = Build.VERSION.SDK_INT;
        String release = Build.VERSION.RELEASE;
        // return "Android version: " + sdkVersion + " (" + release + ")";
        return release;
    }

    void detenerScan() {
        mStop = true;
        if (mFPScan != null) {
            mFPScan.stop();
            mFPScan = null;

        }
        // cierra completamente el usb_host_ctx
        // usb_host_ctx.CloseDevice();
         /*usb_host_ctx.Destroy();
         usb_host_ctx = null;*/
        // return false;
    }

    void iniciarScan() {

        contador++;
        System.out.println("contador= " + contador);
        //flags para que no llame a las variables del main Activity
        methodCheckFlag = true;
        checkFlagDenie=true;
        checkNoOpen = true;

        mFrame = true;
        //mInvertImage=true;
        mUsbHostMode = true;
        usb_host_ctx = new UsbDeviceDataExchangeImpl(this, mHandler);
        SyncDir = this.getExternalFilesDir(null);

        // si encuentra un dispositivo conectado
        if (mFPScan != null) {
            mStop = true;
            mFPScan.stop();

        }
        mStop = false;
        if (mUsbHostMode) {
            usb_host_ctx.CloseDevice();
            if (usb_host_ctx.OpenDevice(0, true)) { //si existe una activy abierta
                //method="allowDevice";
                methodChannel2.invokeMethod("allowDevice", "startScan2");
                System.out.println("antes de startScan2");
               StartScan();

              /*  timer1.schedule(new TimerTask() {
                    public void run() {
                        System.out.println("detener scanerqqqq...");
                        // detener scanner
                        detenerScan();
                        if (mImageFP != null) {
                            // enviar bytes[] a flutter
                           // resultado.success(GetWsq());
                            methodChannel2.invokeMethod("methodCallback", GetWsq());
                        } else {
                            System.out.println("no se implemntó---");
                            resultado.notImplemented();
                        }
                        //timer.cancel();
                    }
                }, 5000);*/

            } else {

                if (!usb_host_ctx.IsPendingOpen()) {
                    System.out.println("No se puede iniciar la operación de escaneo");
                    Toast.makeText(this, "No se puede iniciar la operación de escaneo", Toast.LENGTH_SHORT)
                            .show();
                   /* method="noDeviceFound";*/
                    methodChannel2.invokeMethod("noDeviceFound", "No se puede iniciar la operación de escaneo");


                }
            }
       }
    }



    private boolean StartScan() {
        System.out.println("ingreso a starscan....");
        mFPScan = new FPScan(usb_host_ctx, SyncDir, mHandler);
        mFPScan.start();
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mStop = true;
        if (mFPScan != null) {
            mFPScan.stop();
            mFPScan = null;
        }
        usb_host_ctx.CloseDevice();
        usb_host_ctx.Destroy();
        usb_host_ctx = null;
    }

    private byte[] GetWsq() {

        Scanner devScan = new Scanner();
        boolean bRet;
        if (mUsbHostMode)
            // verifica que haya un dispositivo disponible
            bRet = devScan.OpenDeviceOnInterfaceUsbHost(usb_host_ctx);
        else
            bRet = devScan.OpenDevice();
        if (!bRet) {
            System.out.println("error de algo");
            return null;
        }
        return ShowBitmap();
    }
    private static byte[] ShowBitmap() {
        for (int i = 0; i < mImageWidth * mImageHeight; i++) {
            mPixels[i] = Color.rgb(mImageFP[i], mImageFP[i], mImageFP[i]);
        }

        mCanvas.drawBitmap(mPixels, 0, mImageWidth, 0, 0, mImageWidth, mImageHeight, false, mPaint);

        // convertir a bytes de png
        /*
         * ByteArrayOutputStream stream = new ByteArrayOutputStream();
         * mBitmapFP.compress(Bitmap.CompressFormat.PNG, 50, stream);
         * mBitmapFP_b = stream.toByteArray();
         */

        // convertir a bitmap
        // Bitmap bmp = BitmapFactory.decodeByteArray(mBitmapFP_b, 0,
        // mBitmapFP_b.length);

        // New bitmap con el alto y ancho requeridos, may not return a null object
        Bitmap mutableBitmapImage = Bitmap.createScaledBitmap(mBitmapFP, 512, 512, true);

        //definir bir rate
        float wsqbitRate = WSQEncoder.BITRATE_15_TO_1;
        WSQEncoder instanceWsq = new WSQEncoder(mBitmapFP).setBitrate(wsqbitRate);
        wsqData = instanceWsq.encode();
        // new WSQEncoder(mutableBitmapImage).encode();

        synchronized (mSyncObj) {
            mSyncObj.notifyAll();
        }
        return wsqData;
    }

    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_SHOW_MSG:
                    //String showMsg = (String) msg.obj;
                    //System.out.println("Message show...");
                    break;
                case MESSAGE_SHOW_SCANNER_INFO:
                    String showInfo = (String) msg.obj;
                    System.out.println("Message show scanner info...");
                    break;
                case MESSAGE_SHOW_IMAGE:
                    // ShowBitmap();
                    break;
                case MESSAGE_ERROR:
                    mFPScan = null;
                    break;
                case UsbDeviceDataExchangeImpl.MESSAGE_ALLOW_DEVICE:
                    //cuando se otroga permisos al huellero
                    if (usb_host_ctx.ValidateContext()) {
                        //method="allowDeviceone";
                        if (methodCheckFlag){
                            methodCheckFlag=false;
                            //Toast.makeText(MainActivity.this, "Vuelva a presionar el botón de Biometria", Toast.LENGTH_SHORT).show();
                            methodChannel2.invokeMethod("allowDeviceOne", "startScan1");
                            StartScan();
                        }
                    }else{
                        if (checkNoOpen){
                            checkNoOpen=false;
                        methodChannel2.invokeMethod("NoOpen", "Can't open scanner device");
                       // Toast.makeText(MainActivity.this, "Can't open scanner device", Toast.LENGTH_SHORT).show();
                    }
                    }
                    break;
                case UsbDeviceDataExchangeImpl.MESSAGE_DENY_DEVICE:
                    System.out.println("usuario denegó el permiso");
                    if (checkFlagDenie){
                        checkFlagDenie=false;
                        //Toast.makeText(MainActivity.this, "Usuario denegó el permiso", Toast.LENGTH_SHORT).show();
                        methodChannel2.invokeMethod("denyDevice", "usuario denegó el permiso");
                    }

                    break;
            }
        }
    };

}

