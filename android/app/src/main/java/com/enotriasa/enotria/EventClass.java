package com.enotriasa.enotria;

import android.app.Activity;

import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.EventChannel;

public class EventClass implements EventChannel.StreamHandler{

    private EventChannel.EventSink events;

    public EventClass(Activity activity, FlutterEngine flutterEngine){
        BinaryMessenger message = flutterEngine.getDartExecutor().getBinaryMessenger();
        EventChannel eventChannel= new EventChannel(message,"com.enotriasa/bio_event_channel");
        eventChannel.setStreamHandler(this);

    }

    @Override
    public void onListen(Object arguments, EventChannel.EventSink events) {
        this.events = events;
        events.success("argumentooos");
    }

    @Override
    public void onCancel(Object arguments) {

    }
}
