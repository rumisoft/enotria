package com.enotriasa.enotria;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.util.Log;

import java.util.HashMap;
import java.util.Iterator;

import io.flutter.plugin.common.MethodChannel;

public class PermissionUsb {
    private UsbManager mDevManager;
   private PermissionUsb.FTR_USB_DEVICE_INTERNAL usb_ctx = null;
    private Context context = null;
    private boolean pending_open = false;
    static final String ACTION_USB_PERMISSION = "com.futronictech.FtrScanDemoActivity.USB_PERMISSION";
    private PendingIntent mPermissionIntent = null;
    MethodChannel.Result resultadow;

    public final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("com.futronictech.FtrScanDemoActivity.USB_PERMISSION".equals(action)) {
                synchronized(PermissionUsb.this.mPermissionIntent) {
                    UsbDevice device = (UsbDevice)intent.getParcelableExtra("device");
                    if (intent.getBooleanExtra("permission", false)) {
                        if (device != null) {
                            System.out.println("usb permiso aceptado = " );

                           // System.out.println(mDevManager.EXTRA_PERMISSION_GRANTED);
                            resultadow.success("sii");
                            //PermissionUsb.this.usb_ctx = PermissionUsb.this.OpenDevice(device);
                        }

                        //PermissionUsb.this.handler.obtainMessage(255).sendToTarget();
                    } else {
                        System.out.println("usb permiso denegado = " );
                        resultadow.success("noo");

                        // PermissionUsb.this.handler.obtainMessage(256).sendToTarget();
                    }
                }
            }

        }
    };

    public PermissionUsb(Context ctx,MethodChannel.Result resultado ) {
        this.context = ctx;
        //this.handler = trg_handler;
        this.mDevManager = (UsbManager) ctx.getSystemService(Context.USB_SERVICE);
        this.mPermissionIntent = PendingIntent.getBroadcast(ctx, 0, new Intent("com.futronictech.FtrScanDemoActivity.USB_PERMISSION"), 0);
        IntentFilter filter = new IntentFilter("com.futronictech.FtrScanDemoActivity.USB_PERMISSION");
        this.context.registerReceiver(this.mUsbReceiver, filter);
       this.resultadow=resultado;
    }

    public boolean OpenDevice(int instance, boolean is_activity_thread) {
        synchronized(this) {
            if (this.usb_ctx == null) {
                this.pending_open = false;
                HashMap<String, UsbDevice> usb_devs = this.mDevManager.getDeviceList();
                Iterator<UsbDevice> deviceIterator = usb_devs.values().iterator();
                int index = 0;

                while(deviceIterator.hasNext()) {
                    UsbDevice device = (UsbDevice)deviceIterator.next();
                   if (IsFutronicDevice(device.getVendorId(), device.getProductId())) {
                        if (index < instance) {
                            ++index;
                        } else {
                            if (!this.mDevManager.hasPermission(device)) {
                                this.mDevManager.requestPermission(device, this.mPermissionIntent);
                                if (is_activity_thread) {
                                    this.pending_open = true;
                                    return false;
                                }

                                this.pending_open = false;
                                synchronized(this.mPermissionIntent) {
                                    try {
                                        this.mPermissionIntent.wait();
                                    } catch (InterruptedException var12) {
                                        var12.printStackTrace();
                                    }
                                }
                            }

                            if (this.mDevManager.hasPermission(device)) {
                                this.usb_ctx = this.OpenDevice(device);
                            } else {
                                Log.e("FUTRONICFTR_J", "device not allow");
                            }
                        }
                    }
                }
            }

            return this.usb_ctx != null;
        }
    }
    private PermissionUsb.FTR_USB_DEVICE_INTERNAL OpenDevice(UsbDevice device) {
        PermissionUsb.FTR_USB_DEVICE_INTERNAL res = null;
        UsbInterface intf = device.getInterface(0);
        if (intf != null) {
            UsbEndpoint readpoint = null;
            UsbEndpoint writepoint = null;

            for(int i = 0; i < intf.getEndpointCount(); ++i) {
                if (intf.getEndpoint(i).getDirection() == 0) {
                    writepoint = intf.getEndpoint(i);
                }

                if (intf.getEndpoint(i).getDirection() == 128) {
                    readpoint = intf.getEndpoint(i);
                }
            }

            if (readpoint != null && writepoint != null) {
                UsbDeviceConnection connection = this.mDevManager.openDevice(device);
                if (connection != null) {
                    res = new PermissionUsb.FTR_USB_DEVICE_INTERNAL(device, intf, readpoint, writepoint, connection);
                } else {
                    Log.e("FUTRONICFTR_J", "open device failed: " + device);
                }
            } else {
                Log.e("FUTRONICFTR_J", "End points not found in device: " + device);
            }
        } else {
            Log.e("FUTRONICFTR_J", "Get interface failed failed in device: " + device);
        }

        return res;
    }

    public static boolean IsFutronicDevice(int idVendor, int idProduct) {
        boolean res = false;
        if (idVendor == 2100 && idProduct == 32 || idVendor == 2392 && idProduct == 775 || idVendor == 5265 && (idProduct == 32 || idProduct == 37 || idProduct == 136 || idProduct == 144 || idProduct == 80 || idProduct == 96 || idProduct == 152 || idProduct == 32920 || idProduct == 39008) || idVendor == 8122 && (idProduct == 19 || idProduct == 18 || idProduct == 39)) {
            res = true;
        }

        return res;
    }

    public class FTR_USB_DEVICE_INTERNAL {
        public UsbDevice mDev_;
        public UsbInterface mIntf_;
        public UsbEndpoint mReadPoint_;
        public UsbEndpoint mWritePoint_;
        public UsbDeviceConnection mDevConnetion_;
        public boolean mHandleClaimed_;

        public FTR_USB_DEVICE_INTERNAL(UsbDevice mDev, UsbInterface mIntf, UsbEndpoint mReadPoint, UsbEndpoint mWritePoint, UsbDeviceConnection mDevConnetion) {
            this.mDev_ = mDev;
            this.mIntf_ = mIntf;
            this.mReadPoint_ = mReadPoint;
            this.mWritePoint_ = mWritePoint;
            this.mDevConnetion_ = mDevConnetion;
            this.mHandleClaimed_ = false;
        }
    }

}
