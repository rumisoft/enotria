package com.enotriasa.enotria;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import io.flutter.Log;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.plugins.shim.ShimPluginRegistry;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;

public class HuellerBiometrico implements MethodChannel.MethodCallHandler, EventChannel.StreamHandler {
    private static final String CHANNEL = "com.enotriasa/biometria_platform_channel";
    private Activity activity;
    private MethodChannel.Result flutterResult;
    final int REQUEST_CODE = 1200;
    private UsbManager mDevManager;
    static final String ACTION_USB_PERMISSION = "com.futronictech.FtrScanDemoActivity.USB_PERMISSION";
    private PermissionUsb permissionUsb = null;


    public HuellerBiometrico(Activity activity, FlutterEngine flutterEngine) {
        this.activity = activity;
        BinaryMessenger message = flutterEngine.getDartExecutor().getBinaryMessenger();
        // definir una instancia de method channel
        MethodChannel methodChannel = new MethodChannel(message, CHANNEL);
        methodChannel.setMethodCallHandler(this);

        // ShimPluginRegistry registry = new ShimPluginRegistry(flutterEngine);

      //  PluginRegistry.Registrar registrar = registry.registrarFor(CHANNEL);
       /* registrar.addRequestPermissionsResultListener((requestCode, permissions, grantResults) -> {
            Log.i("LALA", "REQUEST_CODE " + requestCode);
            if (requestCode == REQUEST_CODE) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.flutterResult.success("granted");
                } else {
                    this.flutterResult.success("denied");
                }
                this.flutterResult = null;
            }
            return false;
        });*/
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
        switch (call.method) {
           /* case "check":
                this.check(result);
                break;*/
            case "request":
                this.request(result);
                break;

           /* case "start":
                this.start();
                result.success(null);
                break;
            case "stop":
                this.stop();
                result.success(null);
                break;*/
            default:
                result.notImplemented();
        }
    }

  /*  private void check(MethodChannel.Result result) {
        int status = ContextCompat.checkSelfPermission(this.activity, ACTION_USB_PERMISSION);
        if (status == PackageManager.PERMISSION_GRANTED) {
            result.success("granted");
        } else {
            result.success("denied");
        }
    }*/

    private void request(MethodChannel.Result result) {
       /* if (this.flutterResult != null) {
            result.error("PENDING_TASK", "You have a pending task", null);
            return;
        }*/


       /* this.flutterResult = result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.activity.requestPermissions(new String[]{
                  ACTION_USB_PERMISSION
            }, REQUEST_CODE);*/
        permissionUsb = new PermissionUsb(this.activity, result);
        permissionUsb.OpenDevice(0, true);
           // this.flutterResult = null;

        /*} else {
            this.flutterResult.success("granted");
            this.flutterResult = null;
        }*/
        // Manifest.permission.ACCESS_FINE_LOCATION
    }

    @Override
    public void onListen(Object arguments, EventChannel.EventSink events) {

    }

    @Override
    public void onCancel(Object arguments) {

    }
}
